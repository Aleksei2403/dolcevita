//
//  CSPAlertEventViewController.swift
//  32dayz Family
//
//  Created by mac on 24.08.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

protocol  CSPAlertEventViewControllerDelegate {
    func cspAlertEventViewControllerDelegateText( with text: String)
    func cspAlertEventViewControllerDelegateTime(with time: Int)
}

class CSPAlertEventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AdManagerBannerDelegate {
   
    
    
    @IBOutlet weak var tableView: UITableView!

    var delegate:CSPAlertEventViewControllerDelegate!
    
    private var time = ["At time of event", "5 minutes before","10 minutes before","15 minutes before","30 minutes before","1 hour before","2 hour before","1 day before","2 days before","1 weak before",]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        tableView.delegate = self
        tableView.dataSource = self
    }
    

    @IBAction func noneButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelClickedButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return time.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = time[indexPath.row]
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(time[indexPath.row])
        if time[indexPath.row] == "At time of event" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 1)
        } else if time[indexPath.row] == "5 minutes before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 300)
        }
        else if time[indexPath.row] == "10 minutes before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 600)
        }
        else if time[indexPath.row] == "15 minutes before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 900)
        }
        else if time[indexPath.row] == "30 minutes before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 1800)
        }
        else if time[indexPath.row] == "1 hour before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 3600)
        }
        else if time[indexPath.row] == "2 hour before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 7200)
        }
        else if time[indexPath.row] == "1 day before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 86400)
        }
        else if time[indexPath.row] == "2 days before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 172800)
        }
        else if time[indexPath.row] == "1 weak before" {
            delegate.cspAlertEventViewControllerDelegateTime(with: 604800)
        }
        delegate.cspAlertEventViewControllerDelegateText(with: time[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
}

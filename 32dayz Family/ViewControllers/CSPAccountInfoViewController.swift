//
//  CSPAccountInfoViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPAccountInfoViewController: UIViewController, AdManagerBannerDelegate {
    
    let model = CSPAccountInfoModel()

    @IBOutlet weak var tableView: UITableView!
    private let imagePicker = UIImagePickerController()
    fileprivate var pickedImage: UIImage?
    
    @IBOutlet weak var constraintBottomTableView: NSLayoutConstraint!
    
    lazy var header: CSPAccountInfoHeaderView = {
        let v = CSPAccountInfoHeaderView.instanceFromNib()
        v.onAddPhotoClick = { [weak self] in
            let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let chooseFromLib = UIAlertAction(title: "Choose from Library", style: .default) { _ in
                self?.showImagePicker(withType: .photoLibrary)
            }
            
            let takePhoto = UIAlertAction(title: "Take a Photo", style: .default) { _ in
                self?.showImagePicker(withType: .camera)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
            
            optionMenu.addAction(chooseFromLib)
            optionMenu.addAction(takePhoto)
            optionMenu.addAction(cancelAction)
            
            self?.present(optionMenu, animated: true, completion: nil)
        }
        return v
    }()
    
    lazy var footer: CSPAccountInfoFooterView = {
        let v = CSPAccountInfoFooterView.instanceFromNib()
        v.onLogOutClick = { [weak self] in
            CSPSessionManager.signOut()
            self?.navigationController?.dismiss(animated: false) {
                (UIApplication.shared.keyWindow?.rootViewController as? CSPNavigationController)?.popToRootViewController(animated: true)
            }
        }
        return v
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableHeaderView = header
        tableView.tableFooterView = footer
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let headerHeight: CGFloat = 225
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: headerHeight)
        tableView.tableFooterView?.frame = CGRect(x: 0, y: headerHeight + 180, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 64 - headerHeight - 180 - 70)
    }
    
    //MARK: - Keyboard
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardEndRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintBottomTableView.constant = keyboardEndRect.height
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintBottomTableView.constant = 0
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: -
    
    private func showImagePicker(withType type: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(type) {
            imagePicker.allowsEditing = true
            imagePicker.sourceType = type
            imagePicker.delegate = self
            present(imagePicker, animated: true, completion: nil)
        }
    }

    // MARK: - Actions
    
    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        guard let fn = (tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CSPAccountInfoCell)?.textField.text,
            let ln = (tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? CSPAccountInfoCell)?.textField.text else {
                return
        }
        let ai = UIActivityIndicatorView(style: .gray)
        ai.color = UIColor(0x136781)
        ai.hidesWhenStopped = true
        ai.startAnimating()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: ai)
        if pickedImage != nil {
            CSPAPIManager.shared.updatePhoto(pickedImage!) { [weak self] in
                CSPAPIManager.shared.updateName(fn, ln) {
                    ai.stopAnimating()
                    self?.navigationItem.rightBarButtonItem = sender
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            CSPAPIManager.shared.updateName(fn, ln) { [weak self] in
                ai.stopAnimating()
                self?.navigationItem.rightBarButtonItem = sender
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}

extension CSPAccountInfoViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.cellsCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return model.cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspAccountInfoCell_id.identifier, for: indexPath) as! CSPAccountInfoCell
        let type = model.getCellType(atIndexPath: indexPath)
        cell.config(forType: type)
        if type == .Phone {
            cell.onEditPhoneClick = { [weak self] in
                if let vc = R.storyboard.authorization.cspEnterPhoneViewController_id() {
                    vc.mode = .Settings
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        return cell
    }
    
}

extension CSPAccountInfoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        header.config(withPickedImage: pickedImage)
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

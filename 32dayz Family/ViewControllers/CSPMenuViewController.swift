//
//  CSPMenuViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPMenuViewController: UIViewController, AdManagerBannerDelegate {
    
    let model = CSPMenuModel()
    
    var loadFlag: Bool = false
    var tasks = [CSPTaskModel]()
    let modelTask = CSPTasksModel()
    @IBOutlet weak var menuHeaderView: UIView!
    @IBOutlet weak var menuHeaderButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadMenu()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadMenu), name: NN.Update, object: nil)
    }
    
    fileprivate func updateCircleHeader(_ circle: CSPCircleModel) {
        menuHeaderButton.setTitle(circle.name, for: .normal)
    }
    
    @objc fileprivate func reloadMenu() {
        menuHeaderView.isHidden = model.circles.count == 1
        if let currentCircle = CSPSessionManager.currentCircle {
            updateCircleHeader(currentCircle)
        }
        tableView.reloadData()
    }
    
    // MARK: - Actions
    
    lazy var popup: CSPCircleSelectionPopup = {
        let view = CSPCircleSelectionPopup.instanceFromNib()
        view.onCircleSelection = { [weak self] circle in
            CSPSessionManager.setCurrentCircle(circle)
            NotificationCenter.default.post(name: NN.Update, object: nil)
        }
        return view
    }()
    
    @IBAction func clickedMenuHeaderButton(_ sender: UIButton) {
        popup.circles = model.circles
        popup.currentCircle = CSPSessionManager.currentCircle
        UIApplication.shared.keyWindow?.addSubview(popup)
    }
    
    @IBAction func clickedSettingsButton(_ sender: UIButton) {
        
    }

    @IBAction func clickedAddListButton(_ sender: UIButton) {
        if CSPSessionManager.isSubscribed || CSPSessionManager.currentCircle?.listsArray.count ?? 0 < 3 {
            if let vc = R.storyboard.main.cspNewListViewController_id() {
                vc.onListCreation = { [weak self] in
                    self?.reloadMenu()
                }
                let nc = CSPNavigationController(rootViewController: vc)
                //navigationController?.pushViewController(nc, animated: true)
               present(nc, animated: true, completion: nil)
            }
        } else {
            if let vc = R.storyboard.settings.cspGoldViewController_id() {
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
}

extension CSPMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.rowsCount
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let menuItem = model.menuItem(atIndexPath: indexPath)
        if menuItem is CSPMenuItem {
            switch (menuItem as! CSPMenuItem).type {
            case .Profile:
                return model.circles.count == 1 ? 180 : 195
            case .Journal, .Events, .Today, .Next7Days, .Someday, .Tasks:
                return 60
            default:
                return .leastNormalMagnitude
            }
        } else if menuItem is CSPMenuSmallItem {
            return 50
        } else {
            return .leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let menuItem = model.menuItem(atIndexPath: indexPath)
        if menuItem is CSPMenuItem {
            switch (menuItem as! CSPMenuItem).type {
            case .Profile:
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspMenuProfileCell_id.identifier, for: indexPath) as! CSPMenuProfileCell
                cell.config(topConstraintConstant: model.circles.count == 1 ? 45 : 60)
                cell.onSettingsClick = { [weak self] in
                    if let vc = R.storyboard.settings.cspSettingsViewController_id() {
                        let nc = CSPNavigationController(rootViewController: vc)
                        self?.present(nc, animated: true, completion: nil)
                    }
                }
                return cell
            case .Journal,.Events, .Today, .Next7Days, .Someday, .Tasks:
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspMenuCell_id.identifier, for: indexPath) as! CSPMenuCell
                cell.config(forMenuItem: menuItem as! CSPMenuItem)
                
                return cell
            default:
                return UITableViewCell()
            }
        } else if menuItem is CSPMenuSmallItem {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspMenuSmallCell_id.identifier, for: indexPath) as! CSPMenuSmallCell
            cell.config(forMenuSmallItem: menuItem as! CSPMenuSmallItem)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        
        
        guard let drawerController = navigationController?.viewControllers.last as? KYDrawerController else { return }
        let menuItem = model.menuItem(atIndexPath: indexPath)
        if menuItem is CSPMenuItem {
            switch (menuItem as! CSPMenuItem).type {
            case .Journal:
                if let vc = R.storyboard.main.cspJournalViewController_id() {
                    let nc = CSPNavigationController(rootViewController: vc)
                    nc.isNavigationBarHidden = true
                    drawerController.mainViewController = nc
                }
                drawerController.setDrawerState(.closed, animated: true)
                break
            
            case .Today:
                if let vc = R.storyboard.main.cspTasksViewController_id() {
                    vc.model.mode = .Today
                    vc.model.allTasks = model.getTodayTasks()
                    let nc = CSPNavigationController(rootViewController: vc)
                    nc.isNavigationBarHidden = true
                    drawerController.mainViewController = nc
                }
                drawerController.setDrawerState(.closed, animated: true)
                break
            case .Events :
                if let vc = R.storyboard.main.cspCalendarViewController_id() {
                navigationController?.pushViewController(vc, animated: true)
                }
                break
            case .Next7Days:
                if let vc = R.storyboard.main.cspTasksViewController_id() {
                    vc.model.mode = .Next7Days
                    vc.model.allTasks = model.getNext7DaysTasks()
                    let nc = CSPNavigationController(rootViewController: vc)
                    nc.isNavigationBarHidden = true
                    drawerController.mainViewController = nc
                }
                drawerController.setDrawerState(.closed, animated: true)
                break
            case .Someday:
                if let vc = R.storyboard.main.cspTasksViewController_id() {
                    vc.model.mode = .Someday
                    vc.model.allTasks = model.getSomedayTasks()
                    let nc = CSPNavigationController(rootViewController: vc)
                    nc.isNavigationBarHidden = true
                    drawerController.mainViewController = nc
                }
                drawerController.setDrawerState(.closed, animated: true)
                break
            case .Tasks:
                break
            default:
                break
            }
        } else if menuItem is CSPMenuSmallItem {
            switch (menuItem as! CSPMenuSmallItem).type {
            case .Inbox, .CustomList:
                if let list = model.getList(withListUID: (menuItem as! CSPMenuSmallItem).listUID) {
                    if let vc = R.storyboard.main.cspTasksViewController_id() {
                        vc.model.mode = (menuItem as! CSPMenuSmallItem).type
                        vc.model.list = list
                        vc.model.allTasks = model.getTasks(forList: list)
                        tasks = model.getTasks(forList: list)
                        let nc = CSPNavigationController(rootViewController: vc)
                        nc.isNavigationBarHidden = true
                        drawerController.mainViewController = nc
                    }
                }
                drawerController.setDrawerState(.closed, animated: true)
                break
            default:
                break
            }
        }
    }
    
}

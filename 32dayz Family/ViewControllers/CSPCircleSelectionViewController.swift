//
//  CSPCircleSelectionViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/19/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCircleSelectionViewController: UIViewController, AdManagerBannerDelegate {
    
    let model = CSPCircleSelectionModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        navigationController?.isNavigationBarHidden = false
        navigationItem.hidesBackButton = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Actions

    @IBAction func clickedSwitchAccount(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
}

extension CSPCircleSelectionViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return model.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return model.headerSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return model.footerSize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspCircleCell_id.identifier, for: indexPath) as! CSPCircleCell
        cell.config(forCircle: model.getCircle(atIndexPath: indexPath))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let circle = model.getCircle(atIndexPath: indexPath)
        CSPSessionManager.setCurrentCircle(circle)
        guard let mainViewController = R.storyboard.main.cspJournalViewController_id(),
            let menuViewController = R.storyboard.main.cspMenuViewController_id() else {
                CSPAPIManager.shared.signOut()
                return
        }
        let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300)
        let nc = CSPNavigationController(rootViewController: mainViewController)
        nc.isNavigationBarHidden = true
        drawerController.mainViewController = nc
        drawerController.drawerViewController = menuViewController
        navigationController?.pushViewController(drawerController, animated: true)
    }
    
}

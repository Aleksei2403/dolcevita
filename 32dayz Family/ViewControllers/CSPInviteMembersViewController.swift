//
//  CSPInviteMembersViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Contacts
import MessageUI
import Firebase
import SystemConfiguration
import Alamofire
import AlamofireObjectMapper

enum CSPInviteMembersViewControllerMode {
    case Authorization
    case Settings
    case Journal
}

class CSPInviteMembersViewController: UIViewController, AdManagerBannerDelegate {
    static let shared = CSPInviteMembersViewController()
    let model = CSPInviteMembersModel()
    var onMembersChange: (([CSPUserModel]) -> ())?
    var mode: CSPInviteMembersViewControllerMode = .Authorization
    var createModel = CSPCreateAccountModel()
    var onUserInvite: ((CSPUserModel) -> ())?
    var circle = CSPCircleModel()
    var onUserInvited: (([CSPUserModel]) -> ())?
   
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.sectionIndexBackgroundColor = .white
        tableView.sectionIndexTrackingBackgroundColor = .white
        tableView.sectionIndexColor = UIColor(0x256bd5)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = R.font.muliLight(size: 15)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.hidesBackButton = true
        if mode == .Authorization || mode == .Journal {
            navigationController?.isNavigationBarHidden = false
        }
        
        if CNContactStore.authorizationStatus(for: .contacts) == .notDetermined {
            CNContactStore().requestAccess(for: .contacts) { [weak self] granted, error in
                if granted {
                    self?.reloadContacts()
                }
            }
        } else if CNContactStore.authorizationStatus(for: .contacts) == .authorized {
            reloadContacts()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationItem.hidesBackButton = false
        if mode == .Authorization || mode == .Journal {
            navigationController?.isNavigationBarHidden = true
        }
        
        
        
    }
    
    fileprivate func reloadContacts() {
        model.loadContacts()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Actions
    
    @IBAction func clickedLaterButton(_ sender: UIBarButtonItem) {
        if mode == .Authorization {
            model.getCircles { [weak self] circles in
                if circles.count > 0 {
                    guard let mainViewController = R.storyboard.main.cspJournalViewController_id(),
                          let menuViewController = R.storyboard.main.cspMenuViewController_id() else {
                        CSPAPIManager.shared.signOut()
                        return
                    }
                    let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300)
                    let nc = CSPNavigationController(rootViewController: mainViewController)
                    nc.isNavigationBarHidden = true
                    drawerController.mainViewController = nc
                    drawerController.drawerViewController = menuViewController
                    self?.navigationController?.pushViewController(drawerController, animated: true)
                }
            }
        } else if mode == .Settings || mode == .Journal {
            navigationController?.popViewController(animated: true)
            
        }
    }
 
    fileprivate func userWasInvited(user: CSPUserModel?, error: Error?) {
        
        if user != nil {
            onUserInvite?(user!)
        }
        // MARK: - Analytics
        
        let sourceParameter = Parameter(.source, mode == .Authorization ? .onboarding : mode == .Journal ? .journal : .settings)
        AnalyticsManager.logEvent(type: .MemberInvited, parameters: ["member_invited" : sourceParameter])
        
        // MARK: -
    }
    
}

extension CSPInviteMembersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.sectionsCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.rowsCount(forSection: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = CSPAlphabetHeaderView.instanceFromNib()
        header.config(forLetter: model.letter(forSection: section))
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return model.letters.map { String($0) }
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspMemberCell_id.identifier, for: indexPath) as! CSPMemberCell
        
        let contact = model.contact(atIndexPath: indexPath)
        let phones = contact.phoneNumbers.map { String($0.value.stringValue.filter { "+01234567890".contains($0) }) }
        cell.config(forContact: contact, isInvited: model.isUserExistAndAlreadyInvited(phones: phones))
        cell.onInviteClick = { [weak self] in
            
            self?.model.getInvites { invites in

                if CSPSessionManager.isSubscribed || invites.count < 1 {
                    let actionSheet = UIAlertController(title: "Pick a Number", message: nil, preferredStyle: .actionSheet)

                    cell.inviteButton.showLoading()
                    self?.model.getUser(phones) { foundUser in
                        cell.inviteButton.hideLoading(success: false) {}
                        if let foundUser = foundUser, let circleUID = self?.model.circleUID {
                            self!.model.invite(user: foundUser, toCircle: circleUID, self!.userWasInvited)
                            cell.config(forContact: contact, isInvited: true)
                        }
                        else  {
                            phones.forEach { phone in
                                
                                let action = UIAlertAction(title: phone, style: .default) { _ in
                                    let circleUID = self?.model.circleUID
                                    
                                    CSPAPIManager.shared.createInvitedAccount(account: (firstName: contact.givenName, lastName: contact.familyName, phone: phone, id: contact.identifier, circleUid: String(circleUID ?? ""))) { (account) in
                                    }
                                    self?.model.invite(phone: phone, toCircle: circleUID ?? "", self!.userWasInvited)
                                  
                                    if MFMessageComposeViewController.canSendText() {
                                        
                                        let vc = MFMessageComposeViewController()
                                        vc.body = inviteFriendsText
                                        vc.recipients = [phone]
                                        vc.messageComposeDelegate = self
                                        self?.present(vc, animated: true, completion: nil)
                                    }
                                }
                                actionSheet.addAction(action)
                            }
                            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
                            actionSheet.addAction(cancel)
                            self?.present(actionSheet, animated: true, completion: nil)
                        }
                    }
                } else {
                    if let vc = R.storyboard.settings.cspGoldViewController_id() {
                        self?.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
        
        return cell
    }
    
}

extension CSPInviteMembersViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            model.resetSearch()
        } else {
            model.search(forString: searchText)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}

extension CSPInviteMembersViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case .sent:
            guard let phone = controller.recipients?.first,
                  let circleUID = model.circleUID else { return }
            model.invite(phone: phone, toCircle: circleUID, userWasInvited)
            break
        default:
            break
        }
        dismiss(animated: true, completion: nil)
    }
    
}

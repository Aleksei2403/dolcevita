//
//  CSPCreateCircleViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

enum CSPCreateCircleMode {
    case Authorization
    case Settings
}

class CSPCreateCircleViewController: UIViewController, AdManagerBannerDelegate {

    @IBOutlet var presenter: CSPCreateCirclePresenter!
    let model = CSPCreateCircleModel()
    var mode: CSPCreateCircleMode = .Authorization
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var familyCircleNameTextField: CSPTextField!
    @IBOutlet weak var createAndContinueButton: CSPButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setupConstraints()
        if mode == .Settings {
            backButton.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        familyCircleNameTextField.becomeFirstResponder()
        if mode == .Settings {
            navigationController?.isNavigationBarHidden = true
        }
    }

    // MARK: - Actions
    
    @IBAction func clickedBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        if mode == .Settings {
            navigationController?.isNavigationBarHidden = false
        }
    }
    
    @IBAction func clickedCreateAndContinueButton(_ sender: UIButton) {
        view.endEditing(true)
        model.circleName = familyCircleNameTextField.text ?? ""
        if model.circleName != "" {
            createAndContinueButton.showLoading()
            model.createCircle { [weak self] success in
                if success {
                    self?.createAndContinueButton.hideLoading(success: true) {
                        if self?.mode == .Authorization {
                            if let vc = R.storyboard.account.cspInviteViewController_id() {
                                self?.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            // MARK: - Analytics
                           
                            AnalyticsManager.logEvent(type: .OnboardingCircleCreated)
                            
                            // MARK: -
                            
                        } else {
                            self?.navigationController?.popViewController(animated: true)
                            self?.navigationController?.isNavigationBarHidden = false
                            NotificationCenter.default.post(name: NN.Update, object: nil)
                        }
                    }
                } else {
                    self?.createAndContinueButton.hideLoading(success: false) {
                        self?.familyCircleNameTextField.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
}

//
//  CSPTaskViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/31/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPTaskViewController: UIViewController, AdManagerBannerDelegate {

    
    var mode: MenuItemType?
    var list: CSPListModel?
    var task: CSPTaskModel!

    static let placeholder = "Your task details..."
    var onTaskCreation: ((CSPTaskModel?) -> ())?
    
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var taskEditorXibView: CSPXibView!
    var taskEditorView: CSPTaskEditorView? {
        return taskEditorXibView.contentView as? CSPTaskEditorView
    }
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var constraintHeightTextView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightDeleteButton: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomScrollView: NSLayoutConstraint!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        if task == nil {
            task = CSPDBManager.shared.createNewTask()
            if let mode = mode {
                switch mode {
                case .Today:
                    task.date = NSDate.today()
                    break
                case .Next7Days:
                    task.date = NSDate.nextWeekMonday()
                    break
                default:
                    break
                }
            }
            task.list = list
        }
        
        taskEditorView?.setup(withTask: task)
        
        taskEditorView?.onAddMembersClick = { [weak self] in
            self?.view.endEditing(true)
            if CSPSessionManager.isSubscribed {
                if let vc = R.storyboard.main.cspAddMembersViewController_id() {
                    vc.selectedUsers = self?.task.members?.array as? [CSPUserModel] ?? []
                    vc.onMembersSelection = { users in
                        (self?.task.members?.array as? [CSPUserModel])?.forEach { self?.task.removeFromMembers($0) }
                        users.forEach { self?.task.addToMembers($0) }
                        if let task = self?.task {
                            self?.taskEditorView?.setup(withTask: task)
                        }
                        
                        // MARK: - Analytics
                        
                        AnalyticsManager.logEvent(type: .TaskMemberAdded)
                        // MARK: -
                        
                    }
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                if let vc = R.storyboard.settings.cspGoldViewController_id() {
                    self?.present(vc, animated: true, completion: nil)
                }
            }
        }
        
        taskEditorView?.onAddDueDateClick = { [weak self] in
            self?.view.endEditing(true)
            self?.taskEditorView?.datePickerView.show()
        }
        
        taskEditorView?.onChooseListClick = { [weak self] in
            self?.view.endEditing(true)
            if let vc = R.storyboard.main.cspSelectListViewController_id() {
                vc.model.selectedList = self?.task.list
                vc.onListSelection = { list in
                    self?.task.list = list
                    if let task = self?.task {
                        self?.taskEditorView?.setup(withTask: task)
                    }

                    // MARK: - Analytics

                    AnalyticsManager.logEvent(type: .TaskListAssigned)
                    // MARK: -

                }
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        taskEditorView?.onChoosePriorityClick = { [weak self] in
            let prioritys = ["None", "Low","Normal","High","Highest"]
            let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            for prior in prioritys {
                let action = UIAlertAction(title: prior, style: .default) { [weak self] action in
                    self?.task.priority = action.title
                    if let task = self?.task {
                        self?.taskEditorView?.setup(withTask: task)
                    }
                }
                //                action.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                actionSheetAlertController.addAction(action)
            }
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in
                
            }
            actionSheetAlertController.addAction(cancelActionButton)
            
            self?.present(actionSheetAlertController, animated: true, completion: nil)
            
        }
        
        
        
        setup(withTask: task)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        contentTextView.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    //MARK: - Keyboard
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardEndRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintBottomScrollView.constant = keyboardEndRect.height
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintBottomScrollView.constant = 0
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: -
    
    private func setup(withTask task: CSPTaskModel) {
        title = task.uid == nil ? "New Task" : "Edit Task"
        if let content = task.content {
            contentTextView.text = content
            contentTextView.textColor = UIColor(0x4c4c4c)
        } else {
            contentTextView.text = CSPTaskViewController.placeholder
            contentTextView.textColor = UIColor(0xd2d5db)
        }
        adjustTextViewHeight()
        if task.author?.uid != CSPSessionManager.currentUser?.uid {
            constraintHeightDeleteButton.constant = 0
            deleteButton.isHidden = true
        }
    }
    
    fileprivate func adjustTextViewHeight() {
        let size = contentTextView.sizeThatFits(CGSize(width: contentTextView.frame.width, height: .greatestFiniteMagnitude))
        constraintHeightTextView.constant = size.height
    }

    // MARK: - Actions
    
    @IBAction func clickedCancelButton(_ sender: UIBarButtonItem?) {
        if task.uid != nil {
            navigationController?.dismiss(animated: true, completion: nil)

        } else  {
            
            navigationController?.dismiss(animated: true, completion: nil)

        }
        
        
    }
    
    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        guard task.list != nil else {
            presentAlertController(text: "Please, select list")
            return
        }
        
        guard task.priority != nil else {
            presentAlertController(text: "Please, select priority")
            return
        }
        
        if let date = task.date {
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "E, dd MMMM yyyy, HH:mm"
            self.task.start = dateFormatter1.string(from: date as Date)
        }
        
        if task.content != nil && task.content != "" {
            let ai = UIActivityIndicatorView(style: .gray)
            ai.color = UIColor(0x136781)
            ai.hidesWhenStopped = true
            ai.startAnimating()
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: ai)
            CSPAPIManager.shared.saveTask(task) { [weak self] error in
                if error == nil {
                    self?.clickedCancelButton(sender)
                    if self?.task.uid == nil {
                        self?.onTaskCreation?(self?.task)
                    }
                    NotificationCenter.default.post(name: NN.Update, object: nil)
                } else {
                    //pm alert error
                }
                ai.stopAnimating()
                self?.navigationItem.rightBarButtonItem = sender
            }
        } else {
            presentAlertController(text: "Please, type details of task")
        }
    }
    
    @IBAction func clickedDeleteButton(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this task? This can not be undone.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
            CSPAPIManager.shared.deleteTask(self.task) { [weak self] error in
                if error == nil {
                    self?.clickedCancelButton(nil)
                    NotificationCenter.default.post(name: NN.Update, object: nil)
                    
                    // MARK: - Analytics
                    
                    AnalyticsManager.logEvent(type: .TaskDeleted)
                    // MARK: -
                    
                } else {
                    //pm show alert error
                }
            }
        }
        alert.addAction(cancel)
        alert.addAction(delete)
        present(alert, animated: true, completion: nil)
    }
    
}

extension CSPTaskViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == CSPTaskViewController.placeholder {
            textView.text = ""
            textView.textColor = UIColor(0x4c4c4c)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            contentTextView.text = CSPTaskViewController.placeholder
            contentTextView.textColor = UIColor(0xd2d5db)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        task.content = textView.text
        adjustTextViewHeight()
    }
    
}


//
//  CSPGoldViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/29/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPGoldViewController: UIViewController {

    @IBOutlet var labels: [UILabel]!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labels.forEach { $0.font = R.font.muliLight(size: DDV(14, 14, 18, 18)) }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Actions
    
    @IBAction func clickedBackButton(_ sender: UIButton) {
        if let nc = navigationController {
            nc.popViewController(animated: true)
            nc.isNavigationBarHidden = false
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func clickedTryGoldButton(_ sender: UIButton) {
        if let vc = R.storyboard.settings.cspGoldSubscriptionViewController_id() {
            present(vc, animated: true, completion: nil)
        }
    }

}

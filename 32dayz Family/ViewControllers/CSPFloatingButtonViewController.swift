//
//  CSPFloatingButtonViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPFloatingButtonViewController: UIViewController {

    @IBOutlet weak var floatingButton: UIButton!
    
    @IBOutlet weak var constraintTopFloatingButton: NSLayoutConstraint!
    
    let maxConstraintValue: CGFloat = 70
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(moveFloatingButton), name: NN.DrawerMove, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideFloatingButton), name: NN.DrawerOpen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showFloatingButton), name: NN.DrawerClose, object: nil)
    }
    
    @objc private func moveFloatingButton(_ notification: Notification) {
        if let percentage = notification.object as? CGFloat {
            constraintTopFloatingButton.constant = maxConstraintValue * (1 - percentage)
        }
    }
    
    @objc private func hideFloatingButton(_ notification: Notification) {
        if let duration = notification.object as? TimeInterval {
            constraintTopFloatingButton.constant = 0
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc private func showFloatingButton(_ notification: Notification) {
        if let duration = notification.object as? TimeInterval {
            constraintTopFloatingButton.constant = maxConstraintValue
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
            }
        }
    }

}

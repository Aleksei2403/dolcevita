//
//  CSPViewTaskViewController.swift
//  32dayz Family
//
//  Created by mac on 30.06.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import CoreData
import Nuke


class CSPViewEventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CSPEventViewControllerDelegate, AdManagerBannerDelegate {
    
    @IBOutlet weak var nameLocation: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backButtonClicked: UIButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightPostImagePlaceholderView: NSLayoutConstraint!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var initialsAuthorCommentLabel: UILabel!
    @IBOutlet weak var initialsAuthorView: UIView!
    @IBOutlet weak var textCommentView: UITextView!
    @IBOutlet weak var buttonAddCommentClicked: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var textReminderLabel: UILabel!
    @IBOutlet weak var reminderLabel: UILabel!
    @IBOutlet weak var endsDateLabel: UILabel!
    @IBOutlet weak var startsDateLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var whosGoingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var taskContentLabel: UILabel!
    @IBOutlet weak var taskDateLabel: UILabel!
    @IBOutlet weak var taskStartsDateLabel: UILabel!
    @IBOutlet weak var taskEndsDateLabel: UILabel!
    @IBOutlet weak var eventReminderLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var event: CSPEventModel!
    var uidComment : String?
    var events = [CSPEventModel]()
    var authorComment = CSPSessionManager.currentUser
    var getComments = [CSPCommentModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        initialsAuthorView.layer.cornerRadius = 23
        authorImageView.layer.cornerRadius = 23
        textCommentView.layer.borderWidth = 1
        textCommentView.layer.borderColor = UIColor.blackk.cgColor
        textCommentView.layer.cornerRadius = 6
        let xib = UINib(nibName: "CSPCommentsViewCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "CSPCommentsViewCell_id")
        commentsLabel.font = UIFont(name: R.font.latoRegular.fontName, size: 11)
        buttonAddCommentClicked.setTitle("Add Comment", for: .normal)
        buttonAddCommentClicked.titleLabel?.font =  UIFont(name: R.font.latoRegular.fontName, size: 14)
        buttonAddCommentClicked.layer.cornerRadius = 6
        buttonAddCommentClicked.setTitleColor(.white, for: .normal)
        buttonAddCommentClicked.backgroundColor = UIColor.turquoiseBlue
        buttonAddCommentClicked.addTarget(self, action: #selector(buttonFooterClicked(_:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        hideKeyboardWhenTappedAround()
        registerNotifications()
        initialsAuthorCommentLabel.text = authorComment?.initials
        
        if let photo = authorComment?.photoURL {
            Nuke.loadImage(with: URL(string: photo)!, into: authorImageView)
        }
        
        if let event = self.event {
            if event.reminder == true {
                eventReminderLabel.text = "Yes"
            } else {
                eventReminderLabel.text = "No"
            }
        }
        
        if let event = self.event {
            if event.allDay == true {
                startsDateLabel.isHidden = true
                endsDateLabel.isHidden = true
                taskStartsDateLabel.isHidden = true
                taskEndsDateLabel.isHidden = true
                
            }
        }
        
        if let start = self.event.startTime {
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat =  "HH:mm"
            let newStartTime = dateFormatter1.string(from: start as Date)
            taskStartsDateLabel.text = "\(newStartTime)"
        }
        
        if let end = self.event.endsTime {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "dd MMMM yyyy "
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat =  "HH:mm"
            let endTime = dateFormatter.string(from: end as Date)
            if let start = self.event.startTime {
                if dateFormatter.string(from: start as Date) == endTime {
                    let newEndsTime  = dateFormatter1.string(from: end as Date)
                    taskEndsDateLabel.text = "\(newEndsTime)"
                    
                } else if dateFormatter.string(from: start as Date) != endTime{
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.dateFormat =  "MMM d, HH:mm "
                    let endDate = dateFormatter2.string(from: end as Date)
                    taskEndsDateLabel.text = "\(endDate)"
                }
            }
        }
        
        if let date = self.event.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "EEEE,  MMM d "
            let newDate = dateFormatter.string(from: date as Date )
            
            taskDateLabel.text = "\(newDate)"
        }
        if let event = self.event {
            uidComment = event.uid
            fetchData(uid: uidComment ?? "")
            taskContentLabel.text = "\(event.content ?? "")"
            nameLocation.text = "\(event.location ?? "")"
        } else {
            taskContentLabel.text = ""
        }
        
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        collectionView.register(R.nib.cspTaskMemberCell)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newValue = change?[.newKey] {
                let newSize = newValue as! CGSize
                self.tableViewHeight.constant = newSize.height
            }
        }
    }
    
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: Foundation.Notification) {
        
        let userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView!.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        let loc = self.textCommentView?.convert(textCommentView!.bounds, to: self.view)
        
        if keyboardFrame.origin.y <  loc!.origin.y {
            self.scrollView?.contentOffset = CGPoint.init(x: (self.scrollView?.contentOffset.x)!, y: loc!.origin.y)
        }
        
        if self.scrollView?.contentInset.bottom == 0 {
            self.scrollView?.contentInset = contentInset
        }
        
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        scrollView.contentOffset = .zero
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func cspEventViewControllerDelegate(with event: CSPEventModel) {
        self.event = event
    }
    
    func setFontAndSizeLabels() {
        reminderLabel.font = R.font.latoRegular(size: 12)
        endsDateLabel.font = R.font.latoRegular(size: 12)
        startsDateLabel.font = R.font.latoRegular(size: 12)
        dateLabel.font = R.font.latoRegular(size: 12)
        whosGoingLabel.font = R.font.latoRegular(size: 12)
    }
    
    func fetchData(uid: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
            let filter = uidComment
            let predicate = NSPredicate(format: "uid = %@", filter ?? "")
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CSPCommentModel")
            fetchRequest.predicate = predicate
            getComments = try managedContext.fetch(fetchRequest) as! [CSPCommentModel]
        } catch let error as NSError {
            print(error)
        }
        
    }
    
    @IBAction func editTaskButtonClicked(_ sender: Any) {
        
        if let vc = R.storyboard.main.cspEventViewController_id(){
            vc.event = event
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func deleteTaskButtonClicked(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this event? This can not be undone.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
            CSPAPIManager.shared.deleteEvent(self.event) { [weak self] error in
                if error == nil {
                    CSPAPIManager.shared.getCircles { (error) in
                        self?.backButtonClicked((Any).self)
                        NotificationCenter.default.post(name: NN.Update, object: nil)
                        // MARK: - Analytics
                        
                        AnalyticsManager.logEvent(type: .EventDeleted)
                    }
                    
                } else {
                    //pm show alert error
                }
            }
        }
        alert.addAction(cancel)
        alert.addAction(delete)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    func saveComment(content: String, uid: String, postId: String, author: String,authorImage: String  ){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        if let entity = NSEntityDescription.entity(forEntityName: "CSPCommentModel", in: managedContext){
            let comment = NSManagedObject(entity: entity, insertInto: managedContext)
            comment.setValue(content, forKey: "content")
            comment.setValue(uid, forKey: "uid")
            comment.setValue(postId, forKey: "postId")
            comment.setValue(author, forKey: "author")
            comment.setValue(authorImage, forKey: "authorImage")
            do {
                try managedContext.save()
                getComments.append(comment as! CSPCommentModel)
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func delete(comment: NSManagedObject) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(comment)
        do {
            try managedContext.save()
        } catch {
            print(error)
        }
    }
    
    
    @objc func buttonFooterClicked(_ sender: Any) {
        CSPAPIManager.shared.saveComment(content: self.textCommentView.text, uid: uidComment ?? "", authorImage: "") {  error in
        }
        self.saveComment(content: self.textCommentView.text, uid: uidComment ?? "", postId: uidComment ?? "", author: authorComment?.fullName ?? "", authorImage: authorComment?.photoURL ?? "")
        self.tableView.reloadData()
        self.textCommentView.text = ""
        self.removeObservers()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        commentsLabel.text = "COMMENTS(\(getComments.count))"
        return getComments.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let comment = getComments[sourceIndexPath.item]
        getComments.remove(at: sourceIndexPath.item)
        getComments.insert(comment, at: destinationIndexPath.item)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let comments = getComments[indexPath.row]
            getComments.remove(at: indexPath.item)
            self.delete(comment: comments)
            DispatchQueue.main.async {
                tableView.reloadData ()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CSPCommentsViewCell_id", for: indexPath) as? CSPCommentsViewCell {
            let comment = getComments[indexPath.row]
            if let date = event.date {
                let dateLabel = "Posted \(date.stringRepresentation())"
                cell.dateCommentLabel.text = dateLabel
            }
            
            cell.authorCommentLabel.text = comment.value(forKey: "author") as? String
            cell.textCommentLabel.text = comment.value(forKey: "content") as? String
            cell.userInitialsLabel.text = comment.value(forKey: "author") as? String
            Nuke.loadImage(with: URL(string: comment.value(forKey: "authorImage") as? String ?? "")!, into: cell.userPhotoImageView)
            
            
            return cell
        }
        return UITableViewCell()
    }
    
    func cspTaskViewControllerDelegate(with taskText: String) {
        taskContentLabel.text = taskText
    }
    
    fileprivate var taskMembersCount: Int {
        get {
            return event?.members?.count ?? 0
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return taskMembersCount > 3 ? 3 : taskMembersCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspTaskMemberCell_id.identifier, for: indexPath) as! CSPTaskMemberCell
        cell.transform = CGAffineTransform(scaleX: -1, y: 1)
        if indexPath.item == 2 && taskMembersCount > 3 {
            cell.config(forUsers: taskMembersCount - 2)
        } else {
            cell.config(forUser: event!.membersArray[indexPath.item])
        }
        return cell
    }
    
    
    
    
}

   

extension CSPViewEventViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}

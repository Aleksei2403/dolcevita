//
//  CSPPhoneFailureViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPPhoneFailureViewController: UIViewController, AdManagerBannerDelegate {

    @IBOutlet var presenter: CSPPhoneFailurePresenter!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setupConstraints()
    }

    // MARK: - Cancel
    
    @IBAction func clickedCancelButton(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func clickedTryADifferentNumberButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

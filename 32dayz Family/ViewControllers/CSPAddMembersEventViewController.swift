//
//  CSPAddMembersEventViewController.swift
//  32dayz Family
//
//  Created by mac on 27.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPAddMembersEventViewController: UIViewController, AdManagerBannerDelegate {

    
    var users: [CSPUserModel] {
        get {
            return CSPSessionManager.currentCircle?.members?.array as? [CSPUserModel] ?? []
        }
    }
    var selectedUsers: [CSPUserModel] = []
    var onMembersSelection: (([CSPUserModel]) -> ())?

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
   
    @IBAction func clickedCancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSave(_ sender: Any) {
        onMembersSelection?(selectedUsers)
        
        navigationController?.popViewController(animated: true)
    }
  
}

extension CSPAddMembersEventViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspAddMemberCell_id.identifier, for: indexPath) as! CSPAddMemberCell
        let user = users[indexPath.row]
        cell.config(forUser: user, isSelected: selectedUsers.contains(user))
        cell.onSelectUser = { [weak self] user in
            self?.selectedUsers.append(user)
            tableView.reloadData()
        }
        cell.onDeselectUser = { [weak self] user in
            guard let index = self?.selectedUsers.firstIndex(of: user) else { return }
            self?.selectedUsers.remove(at: index)
            tableView.reloadData()
        }
        return cell
    }

}

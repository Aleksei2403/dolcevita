//
//  CSPSpinnerViewController.swift
//  FamilyOrganizer
//
//  Created by User on 27.10.16.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import UIKit

class CSPSpinnerViewController: UIViewController {
    
    @IBOutlet weak var spinnerImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CSPSpinnerViewController.animateSpiner),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateSpiner()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeSpinnerAnimation()
    }
    
    public func dismissSelf() {
        dismiss(animated: false, completion: nil)
    }
    
    @objc private func animateSpiner() {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.fromValue = 0
        animation.toValue = 2 * Double.pi
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = .infinity
        spinnerImageView.layer.add(animation, forKey: "spin")
    }
    
    private func removeSpinnerAnimation() {
        spinnerImageView.layer.removeAllAnimations()
    }
    
}

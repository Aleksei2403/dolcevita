//
//  CSPMonthViewController.swift
//  32dayz Family
//
//  Created by mac on 8.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import JTAppleCalendar
import CoreData

protocol CSPMonthViewControllerDelegate {
    func cspPMonthViewControllerDelegate(with month : String)
    func cspMonthViewControllerPassDateDelegate(with date : String)
    func cspMonthViewControllerPassWeekdayDelegate(with weekday : String)
}


class CSPMonthViewController: UIViewController, AdManagerBannerDelegate {
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var  calendarView: JTACMonthView!
    
    let formatter = DateFormatter()
    var delegate : CSPMonthViewControllerDelegate!
    var model = CSPEventsModel()
    var month = ""
    var events : [AnyObject] = []
    var dates: [Date] = []
    var dt : String = ""
    var isRangeSelection = true
    var circle = CSPSessionManager.currentCircle
    private var checkInDate: Date?
    private var checkOutDate: Date?
    var monthComponents : [String] = []
    private lazy var cspMonthViewController: CSPMonthViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "CSPMonthViewController_id") as! CSPMonthViewController
        return viewController
    }()
    
    fileprivate let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter
    }()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarView.ibCalendarDelegate = self
        calendarView.ibCalendarDataSource = self
        calendarView.allowsRangedSelection = isRangeSelection
        calendarView.allowsMultipleSelection = isRangeSelection
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        getEvents(withUID: circle?.uid ?? "")
        setupCalendar()
        monthComponents = Calendar.current.shortMonthSymbols
    }
    
    func setupCalendar() {
        
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        calendarView.visibleDates { (visibleDates) in
        self.setupViewsOfCalendar(from: visibleDates)
            
        }
    }
 
    func getEvents(withUID uid: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CSPEventModel")
        do {
            events = try CSPDBManager.shared.managedObjectContext.fetch(fetchRequest) as! [CSPEventModel]
            for task in self.getAllTasks() {
                events.append(task)
            }

        } catch {
            print(error)
        }
    }
    
    func getAllTasks() -> [CSPTaskModel]{
        return CSPDBManager.shared.getTasks()
    }
    
     func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
    
        containerView.addSubview(viewController.view)
        
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
     func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func handleCellTextColor(view: JTACDayCell, cellState: CellState) {
        guard let validCell = view as? CSPMonthCalendarCell else { return }
        
        if cellState.dateBelongsTo == .thisMonth {
            validCell.dateLabel.textColor = .black
            validCell.contentViewBackGroundColor.backgroundColor = .white
       }
        else {
            validCell.contentViewBackGroundColor.backgroundColor = .polar
            validCell.viewDateInTableView.backgroundColor = .polar
            validCell.tableView.backgroundColor = .polar
            validCell.dateLabel.textColor = .gray
        }
    }
    
    func setupViewsOfCalendar(from visibleDates : DateSegmentInfo) {
      
        let date = visibleDates.monthDates.first!.date
        self.formatter.dateFormat = "MMMM"
        month = formatter.string(from: date)
        self.delegate?.cspPMonthViewControllerDelegate(with: month)
    }
    
    @IBAction func backClickedCell(_ sender: UIButton) {
  
    }
    
    @IBAction func newTaskBUttonClicked(_ sender: Any) {
        if CSPSessionManager.isSubscribed || CSPSessionManager.currentCircle?.eventsArray.count ?? 0 < 100 {
            if let vc = R.storyboard.main.cspEventViewController_id() {
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let vc = R.storyboard.settings.cspGoldViewController_id() {
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension CSPMonthViewController: JTACMonthViewDataSource {
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        let cell = cell as! CSPMonthCalendarCell
        cell.dateLabel.text = cellState.text
        print(cellState)
       
    }
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        let newDate = formatter.string(from: date)
        let startDate = formatter.date(from: newDate)!
        let endDate = formatter.date(from: "2100 12 31")!
        return ConfigurationParameters(startDate: startDate, endDate: endDate)
    }
    
}

extension CSPMonthViewController: JTACMonthViewDelegate {
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CSPMonthCalendarCell", for: indexPath) as! CSPMonthCalendarCell
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        let cellDate = formatter.string(from: cellState.date)
        
        handleCellTextColor(view: cell, cellState: cellState)
        cell.cellState = cellState.text
        
        if cellState.text == "1" {
            let formatter = DateFormatter()
            formatter.dateFormat = " MMM "
            let newDate = formatter.string(from: date)
            cell.dateLabel.text = cellState.text
            cell.labelMonth.text = newDate.uppercased()
        } else {
            cell.dateLabel.text = cellState.text
            cell.labelMonth.text = ""
        }
        
        for event in events.filter({ return $0.start == cellDate}) {
            
                if cellDate == event.start {
                    cell.events.append(event)
                    cell.eventNames.append(event.content)
                    cell.tableView.reloadData()
                    cell.tableView.isHidden = false
                } else {
                    cell.tableView.reloadData()
                }
           
        }
      
        return cell
    }
    
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        let cell = calendarView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspMonthCalendarCell.identifier, for: indexPath) as! CSPMonthCalendarCell
        if cellState.isSelected {
            dt = dateFormatter.string(from: date)
            let formatter = DateFormatter()
            formatter.dateFormat = " EEE  "
            let newDate = formatter.string(from: date)
            self.delegate.cspMonthViewControllerPassDateDelegate(with: dt)
            self.delegate.cspMonthViewControllerPassWeekdayDelegate(with: newDate)
            cell.tableView.reloadData()
            self.removeFromParent()
            self.view.removeFromSuperview()
        }
    handleCellTextColor(view: cell, cellState: cellState)
     
    }
    
    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        let cell = calendarView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspMonthCalendarCell.identifier, for: indexPath) as! CSPMonthCalendarCell
        if cellState.isSelected == false {
            dt = dateFormatter.string(from: date)
            let formatter = DateFormatter()
            formatter.dateFormat = " EEE  "
            let newDate = formatter.string(from: date)
            self.delegate.cspMonthViewControllerPassWeekdayDelegate(with: newDate)
            self.delegate.cspMonthViewControllerPassDateDelegate(with: dt)
            self.removeFromParent()
            self.view.removeFromSuperview()
        }
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        
       setupViewsOfCalendar(from: visibleDates)
    }
}

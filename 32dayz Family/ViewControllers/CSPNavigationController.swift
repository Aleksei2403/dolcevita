//
//  CSPNavigationController.swift
//  FamilyOrganizer
//
//  Created by User on 31.10.16.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import UIKit

class CSPNavigationController: UINavigationController, AdManagerBannerDelegate {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        AdManager.shared.delegateBanner = self
//        AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: R.font.muliBold(size: 18)!,
                                             NSAttributedString.Key.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: R.font.muliSemiBold(size: 17)!], for: .normal)
        navigationBar.barTintColor = UIColor(0x2CBDEA)
        navigationBar.tintColor = UIColor(0x136781)
        navigationBar.isTranslucent = false
    }
    
}

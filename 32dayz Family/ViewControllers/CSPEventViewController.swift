//
//  CSPEventViewController.swift
//  32dayz Family
//
//  Created by mac on 25.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import FSCalendar
import KMPlaceholderTextView
import CoreData
import Firebase
import GooglePlaces
import MapKit

protocol CSPEventViewControllerDelegate {
    func cspEventViewControllerDelegate(with event: CSPEventModel)
}
protocol CSPEventViewControllerDelegateToVcCalendar {
    func cspEventViewControllerDelegateToVcCalendar()
}

class CSPEventViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, FSCalendarDelegate, FSCalendarDataSource, CSPAlertEventViewControllerDelegate, UITableViewDelegate, UITableViewDataSource , AdManagerBannerDelegate{
    
    enum Times: String {
        case atTimeOfEvent = "At time of event"
        case fiveMinute = "5 minutes before"
        case tenMinute = "10 minutes before"
        case fifteenMinute = "15 minutes before"
        case therttenMinute = "30 minutes before"
        case oneHour = "1 hour before"
        case twoHour = "2 hour before"
        case oneDay = "1 day before"
        case twoDay = "2 days before"
        case oneWeak = "1 weak before"
    }
    
    @IBOutlet weak var viewEndEvent: UIView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    var startTime: NSDate?
    var endsTime: NSDate?
    var delegate : CSPEventViewControllerDelegate!
    var delegate1: CSPEventViewControllerDelegateToVcCalendar!
    var startsTime : String = ""
    var endTime: String = ""
    var calendar : FSCalendar!
    var event : CSPEventModel!
    var model : LocalNotificationRequeust?
    var events = [NSManagedObject]()
    var circle = CSPSessionManager.currentCircle
    var currentUser = CSPSessionManager.currentUser
    let dateFormatter = DateFormatter()
    var times: Times!
    var remindTime: [Int] = []
    var arrayReminders: [String] = []
    var reminderTimeText: String = ""
    @IBOutlet weak var nameLocation: UILabel!
    static let placeholder = "Your task details..."
    
    var onEventCreation: ((CSPEventModel?) -> ())?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var allMembersView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var allDaySwitch: UISwitch!
    @IBOutlet weak var endsDateLabel: UILabel!
    @IBOutlet weak var startsDateLabel: UILabel!
    @IBOutlet weak var endsCalendarView: UIView!
    @IBOutlet weak var startsCalendarView: UIView!
    @IBOutlet weak var contentTextView: KMPlaceholderTextView!
    @IBOutlet weak var startsDateButton: UIButton!
    @IBOutlet weak var endsDateButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var endsLabel: UILabel!
    @IBOutlet weak var startsLabel: UILabel!
    @IBOutlet weak var allDayLabel: UILabel!
    @IBOutlet weak var whosGoingLabel: UILabel!
    @IBOutlet weak var constraintHeightTextView: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomScrollView: NSLayoutConstraint!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    lazy var df: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMMM yyyy, HH:mm"
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(R.nib.cspTaskMemberCell)
        collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        collectionView.reloadData()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        let xib = UINib(nibName: "CSPReminderTableViewCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "CSPReminderTableViewCell")
        if event == nil {
            event = CSPDBManager.shared.createNewEvent()
        }

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
       contentTextView.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        
        setCalendarStart()
        setCalendarEnds()
        setup(withEvent: event)
        
        
        contentTextView.font = R.font.latoRegular(size: 17)
        contentTextView.textColor = .black
        allDaySwitch.isOn = false
        whosGoingLabel.font = R.font.latoRegular(size: 17)
        whosGoingLabel.textColor = .black
        
        allDayLabel.font = R.font.latoRegular(size: 17)
        allDayLabel.textColor = .black
        titleLabel.text = event.uid == nil ? "New Event" : "Edit Event"
        if eventMembersCount > 0 {
            allMembersView.isHidden = true
        } else {
            allMembersView.isHidden = false
        }
        
        if titleLabel.text == "Edit Event" {
            dateFormatter.dateFormat =  "E, d MMMM yyyy, HH:mm"
            if let event = self.event {
                if let start = event.startTime {
                    startsDateLabel.text = dateFormatter.string(from: start as Date)
                }
                if let end = event.endsTime {
                    endsDateLabel.text = dateFormatter.string(from: end as Date)
                }
                if let location = event.location {
                    nameLocation.text = location
                }
            }
            
        } else {
            dateFormatter.dateFormat =  "E, d MMMM yyyy, HH:mm"
            let date = Date().nearestHour()
            event.startTime = date as NSDate?
            startsDateLabel.text = dateFormatter.string(from: date!)
            let end = Calendar.current.date(byAdding: .hour, value: 1, to: date!)
            event.endsTime = end as NSDate?
            event.date = date as NSDate?
            endsDateLabel.text = dateFormatter.string(from: end!)
            event.start = dateFormatter.string(from: date!)
            startsLabel.font = R.font.latoRegular(size: 17)
            startsLabel.textColor = .black
            endsLabel.font = R.font.latoRegular(size: 17)
            endsLabel.textColor = .black
        }
         
        hideKeyboardWhenTappedAround()
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
        view.endEditing(true)
    }
        
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newValue = change?[.newKey] {
                let newSize = newValue as! CGSize
                self.tableViewHeight.constant = newSize.height
            }
        }
    }
    
    func setCalendarEnds() {
        calendar = FSCalendar(frame: CGRect(x: 0.0, y: 0.0, width: self.endsCalendarView.frame.size.width, height: self.endsCalendarView.frame.size.height))
        calendar.scrollDirection = .horizontal
        calendar.headerHeight = 0
        calendar.firstWeekday = 2
        calendar.appearance.headerDateFormat = "M"
        calendar.appearance.caseOptions = FSCalendarCaseOptions.weekdayUsesUpperCase
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        calendar.appearance.weekdayFont = R.font.latoRegular(size: 12)
        calendar.appearance.todayColor = .turquoiseBlue
        calendar.appearance.titleFont = R.font.latoRegular(size: 12)
        calendar.appearance.headerTitleColor = UIColor(0.0, 0.0, 0.0, 0.87)
        calendar.appearance.weekdayTextColor = UIColor(0.0, 0.0, 0.0, 0.57)
        calendar.appearance.headerTitleFont = R.font.latoRegular(size: 20)
        endsDateLabel.text = ""
        calendar.delegate = self
        calendar.dataSource = self
        endsCalendarView.addSubview(calendar)
        endsCalendarView.isHidden = true
    }
    
    func setCalendarStart() {
        calendar = FSCalendar(frame: CGRect(x: 0.0, y: 0.0, width: self.startsCalendarView.frame.size.width, height: self.startsCalendarView.frame.size.height))
        calendar.scrollDirection = .horizontal
        calendar.headerHeight = 0
        calendar.firstWeekday = 2
        calendar.appearance.headerDateFormat = "M"
        calendar.appearance.caseOptions = FSCalendarCaseOptions.weekdayUsesUpperCase
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        calendar.appearance.weekdayFont = R.font.latoRegular(size: 12)
        calendar.appearance.todayColor = .turquoiseBlue
        calendar.appearance.titleFont = R.font.latoRegular(size: 12)
        calendar.appearance.headerTitleColor = UIColor(0.0, 0.0, 0.0, 0.87)
        calendar.appearance.weekdayTextColor = UIColor(0.0, 0.0, 0.0, 0.57)
        calendar.appearance.headerTitleFont = R.font.latoRegular(size: 20)
        startsDateLabel.text = ""
        calendar.delegate = self
        calendar.dataSource = self
        startsCalendarView.addSubview(calendar)
        startsCalendarView.isHidden = true
    }
    
    func sendNotifications(timeIntrval: TimeInterval){
        if timeIntrval >= 0 {
            let randomString = NSUUID().uuidString
            let content = UNMutableNotificationContent()
            content.title = "Dolce Vita"
            content.body = "You haven't completed the event"
            content.sound = UNNotificationSound.default
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeIntrval, repeats: false)
            let request = UNNotificationRequest(identifier: randomString, content: content, trigger: trigger)
            AppDelegate.shared.center.add(request)
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardEndRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintHeightTextView.constant = keyboardEndRect.height
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintHeightTextView.constant = 0
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    

    
    @IBAction func searchLocationButtonClicked(_ sender: Any) {
        if let vc = R.storyboard.main.searchResultTableViewController_id() {
            vc.delegate = self
            let nc = CSPNavigationController(rootViewController: vc)
            present(nc, animated: true, completion: nil)
        }
    }
    
    func secondScreenSaveClicked(place: MKMapItem) {
        nameLocation.text = "\(place.name ?? ""), \(place.placemark )"
    }
    func cspSearchLocationViewControllerDelegate(with text: String) {
        nameLocation.text = text
        event.location = text
    }
    
    fileprivate func adjustTextViewHeight() {
        let size = contentTextView.sizeThatFits(CGSize(width: contentTextView.frame.width, height: .greatestFiniteMagnitude))
        constraintHeightTextView.constant = size.height
    }
    
    // MARK: - Actions
    
 
    @IBAction func whosGoingButtonClicked(_ sender: Any) {
        if CSPSessionManager.isSubscribed {
            if let vc = R.storyboard.main.cspAddMembersEventViewController_id() {
                vc.selectedUsers = self.event.members?.array as? [CSPUserModel] ?? []
                vc.onMembersSelection = { users in
                    (self.event.members?.array as? [CSPUserModel])?.forEach { self.event.removeFromMembers($0) }
                    users.forEach { self.event.addToMembers($0) }
                    self.allMembersView.isHidden = true
                    if let event = self.event {
                        self.setup(withEvent: event)
                    }
                    
                    // MARK: - Analytics
                    
                    AnalyticsManager.logEvent(type: .EventMemberAdded)
                    
                    // MARK: -
                    
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }

    }
    
    
    @IBAction func endsCalendarButton(_ sender: Any) {
        endsDateButton.isSelected = !endsDateButton.isSelected
        if allDaySwitch.isOn != true {
            if endsCalendarView.isHidden == true {
                endsCalendarView.isHidden = false
                startsCalendarView.isHidden = true
            } else if endsCalendarView.isHidden == false {
                endsCalendarView.isHidden = true
            }
        }
       
    }
    
    @IBAction func startButton(_ sender: Any) {
        startsDateButton.isSelected = !startsDateButton.isSelected
        
            if startsCalendarView.isHidden == true {
                startsCalendarView.isHidden = false
                endsCalendarView.isHidden = true
            } else if startsCalendarView.isHidden == false {
                startsCalendarView.isHidden = true
            }
        
    }
    
    @IBAction func allDayAction(_ sender: UISwitch) {
        if sender.isOn {
            startsLabel.text = "Date"
            viewEndEvent.isHidden = true
            endsCalendarView.isHidden = true
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "E, d MMMM yyyy, HH:mm"
            let newDate = dateFormatter.string(from: date)
            startsDateLabel.text = "\(newDate)"
            event.date = date  as NSDate
            startTime = date as NSDate
            endsTime = date as NSDate
        } else {
            startsDateLabel.text = ""
            endsDateLabel.text =  ""
        }
       
    }
    
    @IBAction func clickedCancelButton(_ sender: Any) {
        if event.uid == nil {
            CSPDBManager.shared.deleteEvent(event)
            navigationController?.popViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }

    func cspAlertEventViewControllerDelegateText(with text: String) {
        reminderTimeText = text
        arrayReminders.insert(text, at: arrayReminders.count - 1)
        
        tableView.reloadData()
    }
    
    func cspAlertEventViewControllerDelegateTime(with time: Int) {
        remindTime.append(time)
    }
    
    @IBAction func clickedDoneButton(_ sender: Any)  {
       
        if allDaySwitch.isOn == true {
            event.allDay = true
        } else if allDaySwitch.isOn != true {
            event.allDay = false
        }
        
        guard let key = event.uid ?? Database.database().reference().child(Table.Events).childByAutoId().key else {
            return
        }
        event.content = contentTextView.text
        event.location = nameLocation.text
        event.uid = key
        event.author = currentUser!
        event.circle = circle
        event.createdAt = Date() as NSDate
        if allMembersView.isHidden == false {
            event.addToMembers((circle?.members)!)
        }
        let someDate = Date()
        let timeInterval = someDate.timeIntervalSince1970
        for time in remindTime {
            if let date = event.date {
                sendNotifications(timeIntrval: date.timeIntervalSince1970 - timeInterval - Double(time))
            }
        }
        if event.content != nil && event.content != "" {
            let ai = UIActivityIndicatorView(style: .gray)
            ai.color = UIColor(0x136781)
            ai.hidesWhenStopped = true
            ai.startAnimating()
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: ai)
            CSPAPIManager.shared.saveEvent(self.event) { [weak self] error in
                self?.event.circle = self?.circle
                if error == nil {
                    if let event = self?.event {
                        self?.delegate?.cspEventViewControllerDelegate(with: event)
                        if event.uid == nil {
                            self?.delegate1.cspEventViewControllerDelegateToVcCalendar()
                        }
                    }
                    self?.clickedCancelButton(sender)
                    NotificationCenter.default.post(name: NN.Update, object: nil)
                    
                } 
                ai.stopAnimating()
                let controllers = self?.navigationController?.viewControllers
                              for vc in controllers! {
                                if vc is CSPCalendarViewController {
                                    _ = self?.navigationController?.popToViewController(vc as! CSPCalendarViewController, animated: true)
                                }
                             }
                //Analytics
                // MARK: - Analytics
                
                AnalyticsManager.logEvent(type: .EventCreated, parameters: ["event_created":Parameter(.source, .journal)])
            }
        }
        else {
            presentAlertController(text: "Please, type details of task")
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        var timeDatePicker = ""
        print(date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "E, d MMMM yyyy, HH:mm"
        if startsDateButton.isSelected {
            event.startTime = nil
            
            let myDatePicker: UIDatePicker = UIDatePicker()
            if #available(iOS 13.4, *) {
                myDatePicker.timeZone = .current
                myDatePicker.datePickerMode = .time
                myDatePicker.preferredDatePickerStyle = .wheels
                myDatePicker.backgroundColor = .white
            }
            myDatePicker.date = date
            myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
            let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .alert)
            alertController.view.addSubview(myDatePicker)
            let selectAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
                print("Selected Date: \(myDatePicker.date)")
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "E, d MMMM yyyy, HH:mm"
                self.event.startTime = myDatePicker.date as NSDate
                self.event.date = myDatePicker.date as NSDate
                self.event.start = dateFormatter1.string(from: date)
                let end = Calendar.current.date(byAdding: .hour, value: 1, to: self.event.startTime! as Date)
                self.event.endsTime = end as NSDate?
                self.endsDateLabel.text = dateFormatter.string(from: end!)
                timeDatePicker = dateFormatter1.string(from: myDatePicker.date)
                self.startsDateLabel.text = "\(timeDatePicker)"
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(selectAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true)

        } else if endsDateButton.isSelected {
            event.endsTime = nil
            var timeDatePicker = ""
            let myDatePicker: UIDatePicker = UIDatePicker()
            if #available(iOS 13.4, *) {
                myDatePicker.timeZone = .current
                myDatePicker.datePickerMode = .time
                myDatePicker.preferredDatePickerStyle = .wheels
                myDatePicker.backgroundColor = .white
            }
        myDatePicker.date = date
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .alert)
        alertController.view.addSubview(myDatePicker)
        let selectAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "E, d MMMM yyyy, HH:mm"
            self.event.endsTime = myDatePicker.date as NSDate
            timeDatePicker = dateFormatter1.string(from: myDatePicker.date)
            self.endsDateLabel.text = "\(timeDatePicker)"
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(selectAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
        }
        
    }
    
    private func setup(withEvent event: CSPEventModel) {
        self.event = event
        self.collectionView.reloadData()
        title = event.uid == nil ? "New Event" : "Edit Event"
        if let content = event.content {
            contentTextView.text = content
            contentTextView.textColor = UIColor(0x4c4c4c)
        } else {
            contentTextView.textColor = UIColor(0xd2d5db)
        }
    }
    
    private var maxItems: Int {
        get {
            return Int(collectionView.frame.width / (47.5)) //pm cell width (40) + spacing (7.5)
        }
    }
    
       fileprivate var eventMembersCount: Int {
        get {
            return event.members?.count ?? 0
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrayReminders.count == 0 {
            arrayReminders = ["none"]
            return arrayReminders.count
        } else {
            
            return arrayReminders.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CSPReminderTableViewCell") as! CSPReminderTableViewCell
        
            cell.textReminder.text = arrayReminders[indexPath.row]
       
        if PURCHASE_MANAGER.isExpired {
            if indexPath.row == 0 {
                cell.reminderLabel.text = "First reminder"
                cell.goldLabel.isHidden = true
            } else {
                cell.reminderLabel.text = "More reminders"
            }

        } else {
            if indexPath.row == 0 {
                cell.reminderLabel.text = "First reminder"
                cell.goldLabel.isHidden = true
            } else if cell.textReminder.text == "none" {
                cell.goldLabel.isHidden = true
                cell.reminderLabel.text = "Add reminder"
                cell.textReminder.text = ""
            } else {
                cell.goldLabel.isHidden = true
                cell.reminderLabel.text = "Reminder \(indexPath.row + 1)"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if PURCHASE_MANAGER.isExpired {
        if indexPath.row == 1 {
            if let vc = R.storyboard.main.cspVipViewReminderViewController_id() {
            self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let vc = R.storyboard.main.cspAlertEventViewController_id() {
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
        }
        } else {
            if let vc = R.storyboard.main.cspAlertEventViewController_id() {
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return eventMembersCount > maxItems ? maxItems : eventMembersCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspTaskMemberCell_id.identifier, for: indexPath) as! CSPTaskMemberCell
        if indexPath.item == maxItems - 1 && eventMembersCount > maxItems {
            cell.config(forUsers: eventMembersCount - maxItems + 1)
        } else {
            cell.config(forUser: event.membersArray[indexPath.row])
        }
        return cell
    }
    
}

extension Date {
    func nearestHour() -> Date? {
        var components = NSCalendar.current.dateComponents([.minute], from: self)
        let minute = components.minute ?? 0
        components.minute = minute >= 1 ? 60 - minute : -minute
        return Calendar.current.date(byAdding: components, to: self)
    }
}


extension CSPEventViewController {

    func hideKeyboardWhenTappedAround() {
            let tapGesture = UITapGestureRecognizer(target: self,
                             action: #selector(hideKeyboard))
            view.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false

        }

        @objc func hideKeyboard() {
            contentTextView.resignFirstResponder()
        }

}

extension CSPEventViewController: SearchResultTableViewControllerDelegate {
    func searchResultTableViewControllerDelegate(place: MKMapItem) {
        nameLocation.text = "\(place.name ?? ""), \(place.placemark.formattedAddress ?? "")"
    }
}

//
//  CSPSelectListViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/31/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPSelectListViewController: UIViewController, CSPNewListViewControllerDelegate, AdManagerBannerDelegate {
    
    
    
    let model = CSPSelectListModel()
    var onListSelection: ((CSPListModel?) -> ())?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        onListSelection?(model.selectedList)
        navigationController?.popViewController(animated: true)
    }
    
    func cspNewListViewControllerDelegate(with list: CSPListModel) {
        model.selectedList = list
        self.tableView.reloadData()
    }
    
}

extension CSPSelectListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return model.lists.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspSelectListCell_id.identifier, for: indexPath) as! CSPSelectListCell
            let list = model.lists[indexPath.row]
            cell.config(forList: list, isSelected: model.selectedList == list)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspAddListTableViewCell_id.identifier, for: indexPath) as! CSPAddListTableViewCell
            cell.addListLabel.text = " + Add List"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            model.selectedList = model.lists[indexPath.row]
            tableView.reloadData()
            
        } else {
            if let vc = R.storyboard.main.cspNewListViewController_id() {
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
    }
    
}

//
//  CSPManageMembersViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/16/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPManageMembersViewController: UIViewController, AdManagerBannerDelegate {
    
    let model = CSPManageMembersModel()
    var onMembersChange: (([CSPUserModel]) -> ())?
    var members : [CSPUserModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Actions
   
    @IBAction func clickedInviteButton(_ sender: UIButton) {
        if let vc = R.storyboard.account.cspInviteMembersViewController_id() {
            vc.model.circleUID = model.circleUID
            vc.mode = .Settings
            vc.onUserInvite = { [weak self] user in
                self?.members.append(user)
                self?.tableView.reloadData()
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func didMove(toParent parent: UIViewController?) {
        onMembersChange?(members)
    }
    
}

extension CSPManageMembersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspManageMemberCell_id.identifier, for: indexPath) as! CSPManageMemberCell
        var user = members[indexPath.row]
        cell.config(forUser: user, isHidden: user == model.author)
        cell.onRemoveUser = { [weak self] user in
            if let index = self?.members.firstIndex(of: user) {
            self?.members.remove(at: index)
                
            }
            guard let circleUID = self?.model.circleUID else {
                return
            }
            CSPAPIManager.shared.removeInvite(user: user, toCircle: circleUID) { [weak self] _ in
                tableView.reloadData()
                
            }
        }
        return cell
    }
    
}

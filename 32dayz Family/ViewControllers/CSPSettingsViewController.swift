//
//  CSPSettingsViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import StoreKit
import MessageUI

let inviteFriendsText = "DolceVita is a family organizing app that I use. It's free, try it. \(APPSTORE_LINK)"

class CSPSettingsViewController: UIViewController, AdManagerBannerDelegate {
    
    var model = CSPSettingsModel()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footerLabel: UILabel!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appName = "DolceVita"
        let version = UIApplication.versionBuild()
        let footerAttributedString = NSMutableAttributedString(string: appName + "\n" + version, attributes: [NSAttributedString.Key.foregroundColor: UIColor(0x919191),
                                                                                                              NSAttributedString.Key.font: R.font.muliBold(size: 12)!])
        footerAttributedString.addAttribute(NSAttributedString.Key.font, value: R.font.muliBold(size: 16)!, range: NSMakeRange(0, appName.count))
        footerAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(0x707070), range: NSMakeRange(0, appName.count))
        footerLabel.attributedText = footerAttributedString
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NN.Update, object: nil)
        
    }
    
    @objc fileprivate func reloadData() {
        model = CSPSettingsModel()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Actions

    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
}

extension CSPSettingsViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return model.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = CSPSettingsHeaderView.instanceFromNib()
        header.config(withTitle: model.getSectionTitle(forSection: section))
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspSettingsCell_id.identifier, for: indexPath) as! CSPSettingsCell
        cell.config(forSettingsItem: model.getSettingsItem(atIndexPath: indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = model.getSettingsItem(atIndexPath: indexPath)
        switch item.type {
        case .Circle:
            if let vc = R.storyboard.settings.cspManageCircleViewController_id() {
                vc.model.circle = model.getCircle(atIndexPath: indexPath)
                navigationController?.pushViewController(vc, animated: true)
            }
            break
        case .NewCircle:
            if let vc = R.storyboard.account.cspCreateCircleViewController_id() {
                vc.mode = .Settings
                navigationController?.pushViewController(vc, animated: true)
            }
            break
        case .AccountInfo:
            if let vc = R.storyboard.settings.cspAccountInfoViewController_id() {
                navigationController?.pushViewController(vc, animated: true)
            }
            break
        case .ContactUs:
            if MFMailComposeViewController.canSendMail() {
                let mcvc = MFMailComposeViewController()
                mcvc.mailComposeDelegate = self
                mcvc.setToRecipients(["support@hygger.io"]) //pm fake email
                mcvc.setSubject("DolceVita")
                mcvc.setMessageBody("\n\nThe following auto-inserted information may be helpful in providing you with support:\nApp Version: \(UIApplication.buildNumber())\nDevice: \(UIDevice.current.modelName)\nOS: \(UIDevice.current.systemVersion)", isHTML: false)
                present(mcvc, animated: true, completion: nil)
            }
            break
        case .RateUs:
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
                UIApplication.shared.open(URL(string: APPSTORE_LINK)!, options: [:], completionHandler: nil)
            }
            break
        case .TellFriend:
            let vc = UIActivityViewController(activityItems: [inviteFriendsText], applicationActivities: [])
            present(vc, animated: true, completion: nil)
            
            // MARK: - Analytics
            
            AnalyticsManager.logEvent(type: .ApplicationShared)
            // MARK: -
            
            break
        }
    }
    
}

extension CSPSettingsViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

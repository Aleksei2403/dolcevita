//
//  CSPManageCircleViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/16/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import MessageUI

class CSPManageCircleViewController: UIViewController, MFMessageComposeViewControllerDelegate, AdManagerBannerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    var phoneInvited: String = ""
    var model = CSPManageCircleModel()
    let membersModel = CSPManageMembersModel()
    var arrayInvitedMEmbers : [CSPUserModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        setupNavigationBar()
        
    }
    
    fileprivate func setupNavigationBar() {
        navigationItem.rightBarButtonItem = doneButton
        navigationItem.leftBarButtonItem = UIBarButtonItem()
    }
    
    // MARK: - Actions
    
    @IBAction func clickedDeleteCircleButton(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this circle? This can not be undone.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
            let circles = self.model.getCircles()
            self.model.deleteCircle { [weak self] error in
                if error == nil {
                    if circles.count > 1 {
                        if self?.model.circle.uid == CSPSessionManager.currentCircle?.uid {
                            CSPSessionManager.setCurrentCircle(circles.filter({ $0.uid != self?.model.circle.uid }).first!)
                        }
                        self?.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: NN.Update, object: nil)
                    } else {
                        CSPSessionManager.signOut()
                        self?.navigationController?.dismiss(animated: false) {
                            (UIApplication.shared.keyWindow?.rootViewController as? CSPNavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                    
                    // MARK: - Analytics
                    
                    AnalyticsManager.logEvent(type: .CircleDeleted)
                    // MARK: -
                    
                } else {
                    //pm show alert error
                }
            }
        }
        alert.addAction(cancel)
        alert.addAction(delete)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        let ai = UIActivityIndicatorView(style: .gray)
        ai.color = UIColor(0x136781)
        ai.hidesWhenStopped = true
        ai.startAnimating()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: ai)
        model.updateCircle { [weak self] in
            ai.stopAnimating()
            self?.navigationItem.rightBarButtonItem = sender
            NotificationCenter.default.post(name: NN.Update, object: nil)
            self?.navigationController?.popViewController(animated: true)
            
            // MARK: - Analytics
            
            AnalyticsManager.logEvent(type: .CircleNameChanged)
            // MARK: -
            
        }
    }
    
    
    func onMembersChange() {
        setupNavigationBar()
        tableView.reloadData()
    }
    
}

extension CSPManageCircleViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.sectionsCount
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return model.sectionHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = CSPSettingsHeaderView.instanceFromNib()
        header.config(withTitle: model.sectionTitle(forSection: section))
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.rowsCount(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return model.heightForRow(atIndexPath: indexPath)
    }
    func CSPManageMembersViewControllerDelegate(_ members: [CSPUserModel]) {
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspCircleNameCell_id.identifier, for: indexPath) as! CSPCircleNameCell
            cell.config(withName: model.circleName, isEditable: model.circle.author == CSPSessionManager.currentUser)
            cell.onTextChange = { [weak self] name in
                self?.model.circleName = name
                self?.setupNavigationBar()
            }
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspCircleMembersCell_id.identifier, for: indexPath) as! CSPCircleMembersCell
            cell.config(withMembers: model.circleMembers.filter({ return $0.notVerification == false}), isEditable: model.circle.author == CSPSessionManager.currentUser)
            cell.onCellSelection = { [weak self] in
                if self?.model.circle.author == CSPSessionManager.currentUser {
                    guard let vc = R.storyboard.settings.cspManageMembersViewController_id(),
                          let members = self?.model.circleMembers else { return }
                    vc.model.circleUID = self?.model.circle.uid
                    vc.members = members
                    vc.model.author = self?.model.circle.author
                    vc.onMembersChange = { [weak self] members in
                        self?.model.circleMembers = members
                        self?.setupNavigationBar()
                        self?.onMembersChange()
                    }
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
            return cell
        } else if indexPath.section == 2 {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspCircleInvitedMembers_id.identifier, for: indexPath) as! CSPCircleInvitedMembersCell
                cell.configinvitedMembers( phone: phoneInvited, withInvitedMembers: model.circleMembers.filter ({ return $0.notVerification == true}), isEditable: model.circle.author == CSPSessionManager.currentUser)
                cell.sendAgainInvite = { [weak self] phone in
                    if MFMessageComposeViewController.canSendText() {
                        let vc = MFMessageComposeViewController()
                        vc.body = inviteFriendsText
                        vc.recipients = [phone!]
                        vc.messageComposeDelegate = self
                        self?.present(vc, animated: true, completion: nil)
                    }
                }
                cell.onCellSelection = { [weak self] in
                    if self?.model.circle.author == CSPSessionManager.currentUser {
                        guard let vc = R.storyboard.settings.cspManageMembersViewController_id(),
                              let members = self?.model.circleMembers.filter ({ return $0.notVerification == true}) else { return }
                        vc.model.circleUID = self?.model.circle.uid
                        vc.members = members
                        vc.model.author = self?.model.circle.author
                        vc.onMembersChange = { [weak self] members in
                            self?.model.circleMembers = members
                            self?.setupNavigationBar()
                            self?.onMembersChange()
                        }
                        
                    }
                }
                return cell
            case 1:
                
                return tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspCircleInviteCell_id.identifier, for: indexPath)
            default:
                return UITableViewCell()
            }
        } else if indexPath.section == 3 {
            let cellIdentifier = model.cellIdentifier(atIndexPath: indexPath)
            switch cellIdentifier {
            case /*R.reuseIdentifier.cspSubscriptionInfoCell_id.identifier,*/ R.reuseIdentifier.cspSwitchToThisCircle_id.identifier:
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CSPSettingsCell
                return cell
            case R.reuseIdentifier.cspDeleteCircleCell_id.identifier:
                return tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            default:
                return UITableViewCell()
            }
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            
        } else if indexPath.section == 1 {
            print("clicked1")
            
        } else if indexPath.section == 2 {
            print("click to manageMembers")
                let vc = R.storyboard.settings.cspManageMembersViewController_id()
                let members = self.model.circleMembers
                vc?.model.circleUID = self.model.circle.uid
                vc?.members = members ?? []
                vc?.model.author = self.model.circle.author
                vc?.onMembersChange = { [weak self] members in
                    self?.model.circleMembers = members
                    self?.setupNavigationBar()
                    self?.onMembersChange()
                }
                self.navigationController?.pushViewController(vc!, animated: true)
        } else if indexPath.section == 3 {
            print("clicked3")
            switch indexPath.row {
            case 0 :

                if model.circle == CSPSessionManager.currentCircle {
                    CSPSessionManager.setCurrentCircle(model.circle)
                    NotificationCenter.default.post(name: NN.Update, object: nil)
                    navigationController?.popViewController(animated: true)
                }
                else if model.circle != CSPSessionManager.currentCircle {
                    CSPSessionManager.setCurrentCircle(model.circle)
                    NotificationCenter.default.post(name: NN.Update, object: nil)
                    navigationController?.popViewController(animated: true)
                }
                
            case 1:
                if let vc = R.storyboard.account.cspInviteMembersViewController_id() {
                    vc.model.circleUID = membersModel.circleUID
                    vc.mode = .Settings
                    vc.onUserInvite = { [weak self] user in
                        self?.membersModel.members.append(user)
                        if let members = self?.membersModel.members {
                            self?.model.circleMembers = members
                        }
                        self?.onMembersChange()
                    }
                    navigationController?.pushViewController(vc, animated: true)
                }
            default:
                return
            }
        }
    }
    
    
}


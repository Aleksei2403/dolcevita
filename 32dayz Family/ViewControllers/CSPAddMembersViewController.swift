//
//  CSPAddMembersViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/31/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPAddMembersViewController: UIViewController, AdManagerBannerDelegate {
    
    var users: [CSPUserModel] {
        get {
            return CSPSessionManager.currentCircle?.members?.array as? [CSPUserModel] ?? []
        }
    }
    
    var selectedUsers: [CSPUserModel] = []
    
    var onMembersSelection: (([CSPUserModel]) -> ())?

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero)
    }

    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        onMembersSelection?(selectedUsers)
        navigationController?.popViewController(animated: true)
    }
    
}

extension CSPAddMembersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspAddMemberCell_id.identifier, for: indexPath) as! CSPAddMemberCell
        let user = users[indexPath.row]
        cell.config(forUser: user, isSelected: selectedUsers.contains(user))
        cell.onSelectUser = { [weak self] user in
            self?.selectedUsers.append(user)
            tableView.reloadData()
        }
        cell.onDeselectUser = { [weak self] user in
            guard let index = self?.selectedUsers.firstIndex(of: user) else { return }
            self?.selectedUsers.remove(at: index)
            tableView.reloadData()
        }
        return cell
    }
    
}


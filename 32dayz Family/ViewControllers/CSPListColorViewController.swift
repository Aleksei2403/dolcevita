//
//  CSPListColorViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/3/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPListColorViewController: UIViewController, AdManagerBannerDelegate {
    
    let model = CSPListColorModel()
    
    var onColorSelection: ((CSPColorModel?) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
    }
    
    @objc fileprivate func didSelectColor() {
        onColorSelection?(model.currentColor)
        navigationController?.popViewController(animated: true)
    }

}

extension CSPListColorViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return model.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspListColorCell_id, for: indexPath)!
        let color = model.getColor(atIndexPath: indexPath)
        cell.config(withColor: color, isSelected: color.hex == model.currentColor?.hex)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let color = model.getColor(atIndexPath: indexPath)
        model.currentColor = color
        collectionView.reloadData()
        perform(#selector(didSelectColor), with: nil, afterDelay: 0.25)
    }
    
}

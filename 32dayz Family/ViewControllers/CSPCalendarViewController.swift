//
//  CSPCalendarViewController.swift
//  32dayz Family
//
//  Created by mac on 18.06.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import FSCalendar
import FirebaseDatabase
import  Nuke
import CoreData

let popUpCalendarNotificationName = NSNotification.Name(rawValue: "CSPPopUpCalendarViewController")

protocol cspCalendarViewControllerDelegate {
    func  cspCalendarViewControllerDelegate(wiyh event: CSPEventModel)
}

class CSPCalendarViewController: UIViewController, CSPPopUpCalendarViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CSPMonthViewControllerDelegate,CSPEventViewControllerDelegateToVcCalendar, AdManagerBannerDelegate {
    
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    static let shared = CSPCalendarViewController()
    @IBOutlet weak var currentMonthButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var constraintViewPastEvents: NSLayoutConstraint!
    @IBOutlet weak var selectedMonthButton: UIButton!
    @IBOutlet weak var membersButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var agendaView: UIView!
    @IBOutlet weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var newTaskButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var agendaButton: UIButton!
    @IBOutlet weak var segmentedView: UIView!
    @IBOutlet weak var labelMonth: UILabel!
    @IBOutlet weak var heightTopView: NSLayoutConstraint!
    var delegate: cspCalendarViewControllerDelegate!
    var endTime: NSDate?
    var startTime : NSDate?
    
    var model = CSPEventsModel()
    var mode: MenuItemType = .Events
    var calendarHelper : CalendarHelper!
    var calendar: FSCalendar!
    var numDays : [Int] = []
    var filterNumDays : [String] = []
    var taskDate : [String] = []
    var taskContent : [String] = []
    var dateArray : [String] = []
    var weekdays : [String] = []
    var circle = CSPSessionManager.currentCircle
    var user = CSPSessionManager.currentUser
    var currentDate = Date()
    var members : [String] = []
    var name : String = ""
    var currMN = ""
    var events = [CSPEventModel]()
    var eventsArray = [AnyObject]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate //Singlton instance
    var context:NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(notificationFromCSPPopUpCalendarViewController(_:)),
                                               name: popUpCalendarNotificationName,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = .white
        }
        cancelButton.isHidden = true
        searchBar.isHidden = true
        newTaskButton.layer.cornerRadius = 27
        newTaskButton.backgroundColor = .turquoiseBlue
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.reloadData()
        let xib = UINib(nibName: "CSPCalendarViewCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "CSPCalendarViewCell")
        labelMonth.font = R.font.muliBold(size: 18)
        let currentDate = Date()
        let nameFormatter = DateFormatter()
        let cal = Calendar.current
        let range = cal.range(of: .day, in: .year, for: currentDate)!
        for day in range {
            numDays.append(day)
        }
        nameFormatter.dateFormat = "MMMM"
        name = nameFormatter.string(from: currentDate)
        labelMonth.text = name
        segmentedView.layer.cornerRadius = 8
        agendaButton.backgroundColor = .white
        agendaButton.setTitleColor(.turquoiseBlue, for: UIControl.State.normal)
        agendaButton.titleLabel?.font =  UIFont(name: R.font.latoRegular.fontName, size: 14)
        agendaButton.layer.cornerRadius = 8
        monthButton.setTitleColor(.black, for: UIControl.State.normal)
        monthButton.titleLabel?.font =  UIFont(name: R.font.latoRegular.fontName, size: 14)
        monthButton.layer.cornerRadius = 8
        currentMonthButton.titleLabel?.text = currMN
        setVc()
        getWeekDay()
        self.getEvents(withUID: self.circle?.uid ?? "")
        hideKeyboardWhenTappedAround()
    }
   
    @objc func notificationFromCSPPopUpCalendarViewController(_ notification: Notification) {
        filterNumDays = []
        filterNumDays.insert(notification.object as! String, at: 0)
        labelMonth.text = notification.object as? String
        self.tableView.reloadData()
    }
    
    private lazy var cspMonthViewController: CSPMonthViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "CSPMonthViewController_id") as! CSPMonthViewController
        viewController.delegate = self
        viewController.model = model
        self.add(asChildViewController: viewController)

        return viewController
    }()

    private func getEvents(withUID uid: String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CSPEventModel")
        fetchRequest.relationshipKeyPathsForPrefetching = ["members"]
        do {
            events = try CSPDBManager.shared.managedObjectContext.fetch(fetchRequest) as! [CSPEventModel]
            
            CSPAPIManager.shared.getCircles { (error) in
                NotificationCenter.default.post(name: NN.Update, object: nil)
                self.tableView.reloadData()
            }
            eventsArray = events
            for task in self.getAllTasks() {
                eventsArray.append(task)
            }

        } catch {
            print(error)
        }
    }
    
    func getAllTasks() -> [CSPTaskModel]{
        return CSPDBManager.shared.getTasks()
    }
    
     func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    
   
     func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    @IBAction func clickedCalendar(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAlert = storyboard.instantiateViewController(withIdentifier: "CSPPopUpCalendarViewController_id")
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(myAlert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func monthClickedButton(_ sender: Any) {
        if PURCHASE_MANAGER.isExpired {
            let vc = R.storyboard.main.cspVipViewController_id()
            self.navigationController?.pushViewController(vc!, animated: true)

        } else  {
            DispatchQueue.main.async { [self] in
                self.agendaButton.setTitleColor(.black, for: UIControl.State.normal)
                self.agendaButton.backgroundColor = .clear
                self.monthButton.setTitleColor(.turquoiseBlue, for: UIControl.State.normal)
                self.monthButton.backgroundColor = .white
                self.monthButton.titleLabel?.font =  UIFont(name: R.font.latoRegular.fontName, size: 14)
                self.add(asChildViewController: cspMonthViewController)
           }
       }
      
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        if searchBar.isHidden {
            heightTopView.constant = 44
        } else {
            cancelButton.isHidden = true
            labelMonth.isHidden = false
            segmentedView.isHidden = false
            selectedMonthButton.isHidden = false
            membersButton.isHidden = false
            searchButton.isHidden = false
            monthView.isHidden = false
            agendaView.isHidden = false
            searchBar.isHidden = true
            menuButton.isHidden = false
            filterNumDays = dateArray
            eventsArray = events
            heightTopView.constant = 44
            tableView.reloadData()
        }
    }
    
    @IBAction func searchBarButtonClicked(_ sender: Any) {
        if searchBar.isHidden {
            heightTopView.constant = 0
            cancelButton.isHidden = false
            searchBar.isHidden = false
            menuButton.isHidden = true
            selectedMonthButton.isHidden = true
            membersButton.isHidden = true
            searchButton.isHidden = true
            monthView.isHidden = true
            agendaView.isHidden = true
            segmentedView.isHidden = true
            labelMonth.isHidden = true
        }
    }
    
    @IBAction func membersButtonClicked(_ sender: Any) {
        let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        self.filterNumDays = []
        self.eventsArray = []
        var members = [CSPUserModel]()
        var events = [CSPEventModel]()
        var users: [CSPUserModel] {
            get {
                return CSPSessionManager.currentCircle?.members?.array as? [CSPUserModel] ?? []
            }
        }
        
        for event in eventsArray {
            events.append(event as! CSPEventModel)
        }
        
        for user in users {
            members.append(user)
        }
        
        for author in members {
            let action = UIAlertAction(title: author.fullName, style: .default) { (action) in
                print("Title: \("user.fullName")")
                for event in self.events {
                for day in self.dateArray {
                    for eventMembers in event.membersArray {
                        if let date = event.date {
                        let taskDate1 = dateFormatter.string(from: date as Date)
                        if action.title == eventMembers.fullName && taskDate1 == day {
                                self.eventsArray.append(event)
                                self.filterNumDays.append(day)
                        }
                        }
                    }
                }
            }
                self.tableView.reloadData()
            }
            actionSheetAlertController.addAction(action)
            
        }
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in
            self.filterNumDays = self.dateArray
            self.eventsArray = self.events
            self.tableView.reloadData()
        }
            actionSheetAlertController.addAction(cancelActionButton)

            self.present(actionSheetAlertController, animated: true, completion: nil)
    }
    
    func setVc() {
        let nameFormatter = DateFormatter()
        nameFormatter.dateFormat = "MMMM"
        name = nameFormatter.string(from: currentDate)
        labelMonth.text = name
        self.agendaButton.setTitleColor(.turquoiseBlue, for: UIControl.State.normal)
        self.monthButton.setTitleColor(.black, for: UIControl.State.normal)
        self.agendaButton.backgroundColor = .white
        self.monthButton.backgroundColor = .clear
    }
    
    func cspMonthViewControllerPassDateDelegate(with text: String) {
        setVc()
        filterNumDays = []
        filterNumDays.insert(text, at: 0)
        self.tableView.reloadData()
    }
   
    @IBAction func agendaClickedButton(_ sender: Any) {
        let nameFormatter = DateFormatter()
        nameFormatter.dateFormat = "MMMM"
        name = nameFormatter.string(from: currentDate)
        labelMonth.text = name
        self.agendaButton.setTitleColor(.turquoiseBlue, for: UIControl.State.normal)
        self.monthButton.setTitleColor(.black, for: UIControl.State.normal)
        self.agendaButton.backgroundColor = .white
        self.monthButton.backgroundColor = .clear
       self.remove(asChildViewController: self.cspMonthViewController)
    }
    
    @IBAction func clickedMenuButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedNewEventButton(_ sender: UIButton) {
        if CSPSessionManager.isSubscribed || CSPSessionManager.currentCircle?.eventsArray.count ?? 0 < 100 {
            if let vc = R.storyboard.main.cspEventViewController_id() {
                vc.onEventCreation = { _ in
                    
                }
                vc.delegate1 = self
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let vc = R.storyboard.settings.cspGoldViewController_id() {
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    @IBAction func clickedNewTaskButton(_ sender: UIButton) {
        if CSPSessionManager.isSubscribed || CSPSessionManager.currentCircle?.tasksArray.count ?? 0 < 100 {
            guard (CSPSessionManager.currentCircle?.listsArray.count ?? 0) > 0 else {
                let alertController = UIAlertController(title: "You have no any lists", message: "Please, add a list before adding new tasks", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                present(alertController, animated: true, completion: nil)
                return
            }
            if let vc = R.storyboard.main.cspTaskViewController_id() {
                vc.onTaskCreation = { _ in
                    DispatchQueue.main.async {
                        self.getEvents(withUID: self.circle?.uid ?? "")
                        self.tableView.reloadData()
                    }
                    
                    // MARK: - Analytics
                    
                    AnalyticsManager.logEvent(type: .TaskCreated, parameters: ["task_created" : Parameter(.source, .journal)])
                    // MARK: -
                    
                }
                let nc = CSPNavigationController(rootViewController: vc)
                
                present(nc, animated: true, completion: nil)
            }
        } else {
            if let vc = R.storyboard.settings.cspGoldViewController_id() {
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func newTaskButtonClicked(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let task = UIAlertAction(title: "Add a Task", style: .default) { _ in
            
            self.clickedNewTaskButton(sender)
        }
        let event = UIAlertAction(title: "Add an Event", style: .default) { _ in
            self.clickedNewEventButton(sender)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        actionSheet.addAction(event)
        actionSheet.addAction(task)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)

    }
  
    func getWeekDay() {
        let calendar1 = Calendar.current
        let formatter = DateFormatter()
        let dateFormatter = DateFormatter()
        var nextDate = Date()
        var date = Date()
        var previousDate = Date()
        formatter.dateFormat = "EE"
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let today = formatter.string(from: nextDate)
        let todayDay = dateFormatter.string(from: date)
        weekdays = [today]
        dateArray = [todayDay]
        for _ in numDays {
            previousDate = calendar1.date(byAdding: .day, value: 1, to: nextDate)!
            nextDate = previousDate
            let previous = formatter.string(from: previousDate)
            weekdays.append(previous)
        }
        for _ in numDays {
            previousDate = calendar1.date(byAdding: .day, value: 1, to: date)!
            date = previousDate
            let previous = dateFormatter.string(from: previousDate)
            dateArray.append(previous)
            filterNumDays = dateArray
        }
        
    }
   
    func cspMonthViewControllerPassWeekdayDelegate(with weekday: String) {
        weekdays.insert(weekday, at: 0)
        self.tableView.reloadData()
    }
    
    func cspPMonthViewControllerDelegate(with month: String) {
        self.tableView.reloadData()
        labelMonth.text = month
    
    }
    
    func cspPopUpCalendarViewControllerDelegate(with day: String) {
        filterNumDays.insert(day, at: 0)
        self.tableView.reloadData()
    }
    
    func cspEventViewControllerDelegateToVcCalendar() {
        self.getEvents(withUID: circle?.uid ?? "")
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterNumDays.sorted(by: { return $0 > $1}).count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return filterNumDays[section]
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
        {
        let headerView = UIView(frame: CGRect(x: 16, y: 0, width: tableView.bounds.size.width, height: 30))
        
        let formatter = DateFormatter()
        let nextDate = Date()
        let res = nextDate.addingTimeInterval((24*60*60))
        formatter.dateFormat = "dd MMMM yyyy"
        let tommorow = formatter.string(from: res)
        let crntDate = formatter.string(from: currentDate as Date)
        headerView.backgroundColor = .white
        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1))
        separatorView.backgroundColor = .blackk
        headerView.addSubview(separatorView)
        let labelWeakday = UILabel()
        labelWeakday.frame = CGRect.init(x: 34, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        labelWeakday.text = weekdays[section]
        labelWeakday.font = R.font.latoBold(size: 12)
        labelWeakday.textColor = .black
        headerView.addSubview(labelWeakday)
        
        let labelDate = UILabel()
        labelDate.frame = CGRect.init(x: 74, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        labelDate.text = filterNumDays[section]
        labelDate.font = R.font.latoBold(size: 12)
        labelDate.textColor = .black
        headerView.addSubview(labelDate)
        
        if  filterNumDays[section] == crntDate  {
            separatorView.backgroundColor = .turquoiseBlue
            headerView.backgroundColor = .turquoiseBlue
            labelDate.text = "\(filterNumDays[section]) (Today)"
            labelWeakday.textColor = .white
            labelDate.textColor = .white
        } else if  filterNumDays[section] == tommorow {
            labelDate.text = "\(filterNumDays[section]) (Tomorrow)"
        }
 
            return headerView
        }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] }).count == 0 {
            return 46
        } else {
            return UITableView.automaticDimension
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let eventsArrayCount = eventsArray.filter ({ return $0.start == filterNumDays[section] }).count
        if eventsArrayCount == 0 {
            let firstElement = ["1"]
            return firstElement.count
        } else {
            return eventsArray.filter ({ return $0.start == filterNumDays[section] }).count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CSPCalendarViewCell") as!
            CSPCalendarViewCell
        
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "HH:mm"
        cell.imageListColor.isHidden = true
        cell.listColorView.isHidden = true
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        let eventsArrayCount = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] }).count
                if eventsArrayCount == 0 {
                    cell.noEventsScheduledLabel.text = "No events scheduled"
                    cell.collectionView.isHidden = true
                    cell.allDayLabel.isHidden = true
                } else  {
                    if eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row].allDay == true {
                        cell.allDayLabel.text = "All day"
                        cell.allDayLabel.isHidden = false
                        cell.labelStartEvent.isHidden = true
                        cell.labelFinishEvent.isHidden = true
                    } else  {
                        cell.allDayLabel.isHidden = true
                        cell.labelStartEvent.isHidden = false
                        cell.labelFinishEvent.isHidden = false
                    }
                    
                    cell.event = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row] as? CSPEventModel
                    cell.noEventsScheduledLabel.text = ""
                    
                    if eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row].list != nil  {
                        if let list = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row].list as? CSPListModel {
                            cell.config(forList: list)
                        }
                        if let task = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row] as? CSPTaskModel  {
                            if let content = task.content {
                                cell.nameTask.numberOfLines = 0
                                cell.nameTask.text = "Due Date for  \"\(content)\""
                            }
                        }
                        
                    } else {
                        cell.imageListColor.isHidden = true
                        cell.listColorView.isHidden = true
                        cell.nameTask.text = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row].content!
                    }
                    
                    if let start = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row].startTime {
                        let eventEndsDate = timeformatter.string(from: ((start!) as NSDate) as Date )
                        cell.labelStartEvent.text = eventEndsDate
                    }
                    if let end = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row].endsTime {
                        if let ends = end {
                        let eventEndsDate = timeformatter.string(from: ends as Date )
                        
                        cell.labelFinishEvent.text = eventEndsDate
                        }
                    }
                    cell.collectionView.isHidden = false
                    cell.collectionView.reloadData()
                }

       return  cell
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let eventsArrayCount = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] }).count

        if eventsArrayCount != 0 {
            if eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row].list != nil  {
             
            } else {
            if let vc = R.storyboard.main.cspViewTaskViewController_id() {
                vc.event = eventsArray.filter ({ return $0.start == filterNumDays[indexPath.section] })[indexPath.row] as? CSPEventModel
            self.navigationController?.pushViewController(vc, animated: true)
            }
                
            }
        }
        
       
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterNumDays = []
        eventsArray = []
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        DispatchQueue.main.async {
            if searchText == "" {
                self.eventsArray = self.events
                self.filterNumDays = self.dateArray
            } else {
                    for event in self.events {
                        for day in self.dateArray {
                        if let content = event.content {
                            if content.lowercased().contains(searchText.lowercased()) && event.start == day {
                                self.eventsArray.append(event)
                                self.filterNumDays.append(day)
                            }
                        }
                            
                    }
                }
                
            }
            
            self.tableView.reloadData()
        }
        
    }
    
}

extension CSPCalendarViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboardView))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboardView() {
        view.endEditing(true)
    }
}



//
//  CSPNewListViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/3/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import GoogleMobileAds

protocol CSPNewListViewControllerDelegate {
    func cspNewListViewControllerDelegate(with list: CSPListModel)
}
class CSPNewListViewController: UIViewController, AdManagerBannerDelegate {
    
    let model = CSPNewListModel()
    var delegate: CSPNewListViewControllerDelegate!
    var onListCreation: (() -> ())?
    @IBOutlet weak var banner: GADBannerView!
    @IBOutlet weak var listNameTextField: UITextField!
    @IBOutlet weak var listColorButton: UIButton!
    @IBOutlet weak var listColorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        
        model.loadColors { [weak self] in
            DispatchQueue.main.async {
                self?.updateCurrentColor()
            }
        }
        
        listNameTextField.becomeFirstResponder()
        
    }
    
    fileprivate func updateCurrentColor() {
        listColorView.backgroundColor = model.currentColor?.value
    }

    @IBAction func clickedListColorButton(_ sender: UIButton) {
        view.endEditing(true)
        if let vc = R.storyboard.main.cspListColorViewController_id() {
            vc.model.colors = model.listColors
            vc.model.currentColor = model.currentColor
            vc.onColorSelection = { [weak self] color in
                self?.model.currentColor = color
                self?.updateCurrentColor()
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func clickedCancelButton(_ sender: UIBarButtonItem) {
        //navigationController?.popViewController(animated: true)
        if let nc = navigationController {
            nc.popViewController(animated: true)
            nc.isNavigationBarHidden = false
        } else {
            dismiss(animated: true, completion: nil)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedSaveButton(_ sender: UIBarButtonItem) {
        model.listName = listNameTextField.text
        if model.canCreateList() {
            model.createList { [weak self] success in
                if success {
                    self?.onListCreation?()
                    let lists =  CSPDBManager.shared.getLists()
                    if let list = lists.last {
                        
                        self?.delegate.cspNewListViewControllerDelegate(with: list)
                    }
                    //if
                    self?.clickedCancelButton(sender)
                    
                    // MARK: - Analytics
                    
                    AnalyticsManager.logEvent(type: .ListCreated)
                    // MARK: -
                    
                } else {
                    //pm show alert error list creation
                }
            }
        } else {
            presentAlertController(text: "Please, type name of your list")
        }
    }
    
}

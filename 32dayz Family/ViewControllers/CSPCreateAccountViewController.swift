//
//  CSPCreateAccountViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import RSKImageCropper

enum CSPCreateAccountTextField: Int {
    case FirstName
    case LastName
}

class CSPCreateAccountViewController: UIViewController, AdManagerBannerDelegate {

    @IBOutlet var presenter: CSPCreateAccountPresenter!
    let model = CSPCreateAccountModel()
    
    @IBOutlet weak var firstNameTextField: CSPTextField!
    @IBOutlet weak var lastNameTextField: CSPTextField!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var createAndContinueButton: CSPButton!
    
    private let imagePicker = UIImagePickerController()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //MARK: - Keyboard
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardEndRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        presenter.showKeyboard(keyboardEndRect.height)
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        presenter.hideKeyboard()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }

    // MARK: - Actions
    
    @IBAction func clickedAddPhotoButton(_ sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let chooseFromLib = UIAlertAction(title: "Choose from Library", style: .default) { _ in
            self.showImagePicker(withType: .photoLibrary)
        }
        
        let takePhoto = UIAlertAction(title: "Take a Photo", style: .default) { _ in
            self.showImagePicker(withType: .camera)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        
        optionMenu.addAction(chooseFromLib)
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(cancelAction)
        
        present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func clickedCreateAndContinue(_ sender: UIButton?) {
        view.endEditing(true)
        
        model.firstName = firstNameTextField.text ?? ""
        model.lastName = lastNameTextField.text ?? ""
    
        if model.firstName == "" {
            presenter.showFirstNameError(view)
        }
        if model.lastName == "" {
            presenter.showLastNameError(view)
        }
        
        if model.firstName != "" && model.lastName != "" {
            createAndContinueButton.showLoading()
            model.createAccount { [weak self] success in
                if success {
                    self?.model.applyInvites { circles in
                        if circles.count == 0 {
                            self?.createAndContinueButton.hideLoading(success: true) {
                                if let vc = R.storyboard.account.cspCreateCircleViewController_id() {
                                    self?.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else if circles.count == 1 {
                            CSPSessionManager.setCurrentCircle(circles.first!)
                            self?.createAndContinueButton.hideLoading(success: true) {
                                guard let mainViewController = R.storyboard.main.cspJournalViewController_id(),
                                    let menuViewController = R.storyboard.main.cspMenuViewController_id() else {
                                        CSPAPIManager.shared.signOut()
                                        return
                                }
                                let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300)
                                let nc = CSPNavigationController(rootViewController: mainViewController)
                                nc.isNavigationBarHidden = true
                                drawerController.mainViewController = nc
                                drawerController.drawerViewController = menuViewController
                                self?.navigationController?.pushViewController(drawerController, animated: true)
                            }
                        } else if circles.count > 1 {
                            self?.createAndContinueButton.hideLoading(success: true) {
                                if let vc = R.storyboard.account.cspCircleSelectionViewController_id() {
                                    self?.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                    }
                } else {
                    self?.createAndContinueButton.hideLoading(success: false) { }
                }
            }
        }
    }
    
    // MARK: -
    
    private func showImagePicker(withType type: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(type) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = type
            imagePicker.delegate = self
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
}

extension CSPCreateAccountViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case CSPCreateAccountTextField.FirstName.rawValue:
            lastNameTextField.becomeFirstResponder()
            break
        case CSPCreateAccountTextField.LastName.rawValue:
            clickedCreateAndContinue(nil)
            break
        default:
            break
        }
        return false
    }
    
}

extension CSPCreateAccountViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let vc = RSKImageCropViewController(image: pickedImage, cropMode: .circle)
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension CSPCreateAccountViewController: RSKImageCropViewControllerDelegate {
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        model.photo = croppedImage
        avatarImageView.image = croppedImage
        navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        model.photo = croppedImage
        avatarImageView.image = croppedImage
        navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, willCropImage originalImage: UIImage) {
        print("will crop")
    }
    
}

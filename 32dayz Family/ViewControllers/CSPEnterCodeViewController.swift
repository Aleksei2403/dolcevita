//
//  CSPEnterCodeViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import UserNotifications

class CSPEnterCodeViewController: UIViewController, AdManagerBannerDelegate {

    @IBOutlet var presenter: CSPEnterCodePresenter!
    let model = CSPEnterCodeModel()
    var mode: CSPPhoneConfirmationMode = .Authorization
    
    @IBOutlet weak var codeTextField: CSPTextField!
    @IBOutlet weak var continueButton: CSPButton!
    @IBOutlet weak var resendCodeButton: CSPButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        codeTextField.becomeFirstResponder()
        if mode == .Settings {
            navigationController?.isNavigationBarHidden = true
        }
    }
    
    // MARK: - Actions
    
    @IBAction func clickedBackButton(_ sender: UIButton) {
        CSPAPIManager.shared.clearVerificationID()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedContinueButton(_ sender: UIButton) {
       
        view.endEditing(true)
        continueButton.showLoading()
        model.confirmCode { [weak self] error in
            if error == nil {
                if self?.mode == .Authorization {
                    self?.model.getUser { user in
                        if user == nil {
                            self?.continueButton.hideLoading(success: true) {
                                let center = UNUserNotificationCenter.current()
                                center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                                }
                                if let vc = R.storyboard.account.cspCreateAccountViewController_id() {
                                    self?.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else {
                            CSPSessionManager.setCurrentUser(user!)
                            self?.model.getCircles { circles in
                                if circles.count == 0 {
                                    self?.continueButton.hideLoading(success: true) {
                                        if let vc = R.storyboard.account.cspCreateCircleViewController_id() {
                                            self?.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                } else if circles.count == 1 {
                                    CSPSessionManager.setCurrentCircle(circles.first!)
                                    self?.continueButton.hideLoading(success: true) {
                                        guard let mainViewController = R.storyboard.main.cspJournalViewController_id(),
                                            let menuViewController = R.storyboard.main.cspMenuViewController_id() else {
                                                CSPAPIManager.shared.signOut()
                                                return
                                        }
                                        let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300)
                                        let nc = CSPNavigationController(rootViewController: mainViewController)
                                        nc.isNavigationBarHidden = true
                                        drawerController.mainViewController = nc
                                        drawerController.drawerViewController = menuViewController
                                        self?.navigationController?.pushViewController(drawerController, animated: true)
                                    }
                                } else if circles.count > 1 {
                                    self?.continueButton.hideLoading(success: true) {
                                        if let vc = R.storyboard.account.cspCircleSelectionViewController_id() {
                                            self?.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if self?.mode == .Settings {
                    self?.model.updatePhone {
                        if let vc = (self?.navigationController?.viewControllers.filter { $0 is CSPAccountInfoViewController })?.first {
                            self?.navigationController?.popToViewController(vc, animated: true)
                        }
                        
                        // MARK: - Analytics
                        
                        AnalyticsManager.logEvent(type: .PhoneNumberUpdated)
                        // MARK: -
                        
                    }
                }
            } else {
                self?.continueButton.hideLoading(success: false) {
                    if let view = self?.view {
                        self?.presenter.showError(view)
                    }
                    self?.codeTextField.becomeFirstResponder()
                }
            }
        }
    }
    
    @IBAction func clickedResendCodeButton(_ sender: UIButton) {
        resendCodeButton.showLoading()
        CSPAPIManager.shared.verifyPhone(model.phone) { [weak self] error in
            self?.resendCodeButton.hideLoading(success: false) { }
        }
    }
    
    @IBAction func textFieldTextChanged(_ sender: UITextField) {
        model.codeString = sender.text ?? ""
        presenter.hideError(view)
    }
    
}

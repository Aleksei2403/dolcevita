//
//  CSPInviteViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPInviteViewController: UIViewController, AdManagerBannerDelegate {

    @IBOutlet var presenter: CSPInvitePresenter!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setupConstraints()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func clickedInviteButton(_ sender: UIButton) {
        if let vc = R.storyboard.account.cspInviteMembersViewController_id() {
            
            vc.model.circleUID = CSPSessionManager.currentCircle?.uid
            vc.mode = .Authorization
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func clickedInviteLaterButton(_ sender: UIButton) {
        guard let mainViewController = R.storyboard.main.cspJournalViewController_id(),
            let menuViewController = R.storyboard.main.cspMenuViewController_id() else {
                CSPAPIManager.shared.signOut()
                return
        }
        let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300)
        let nc = CSPNavigationController(rootViewController: mainViewController)
        nc.isNavigationBarHidden = true
        drawerController.mainViewController = nc
        drawerController.drawerViewController = menuViewController
        navigationController?.pushViewController(drawerController, animated: true)
    }
    
}

//
//  CSPEnterPhoneViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

enum CSPPhoneConfirmationMode {
    case Authorization
    case Settings
}

class CSPEnterPhoneViewController: UIViewController, AdManagerBannerDelegate {

    @IBOutlet var presenter: CSPEnterPhonePresenter!
    let model = CSPEnterPhoneModel()
    var mode: CSPPhoneConfirmationMode = .Authorization
    
    @IBOutlet weak var countryCodeButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var errorStackView: UIStackView!
    @IBOutlet weak var sendCodeButton: CSPButton!
    
    @IBOutlet weak var constraintBottomSendCodeButton: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomInfoLabel: NSLayoutConstraint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setupConstraints()
        view.layoutIfNeeded()
        model.fetchCarrier()
        setupCountryCodeButton(forCountry: model.selectedCountry)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        phoneNumberTextField.becomeFirstResponder()
        if mode == .Settings {
            navigationController?.isNavigationBarHidden = true
        }
    }
    
    fileprivate func setupCountryCodeButton(forCountry country: CSPCountry) {
        countryCodeButton.setTitle("+" + String(country.code) + " " + country.name!, for: .normal)
        countryCodeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: countryCodeButton.frame.size.width - 25, bottom: 0, right: 0)
        countryCodeButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    }

    // MARK: - Actions
    
    @IBAction func clickedBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        if mode == .Settings {
            navigationController?.isNavigationBarHidden = false
        }
    }
    
    @IBAction func clickedCountryCodeButton(_ sender: UIButton) {
        if let vc = R.storyboard.authorization.cspCountrySelectionViewController_id() {
            vc.delegate = self
            vc.model.countries = model.countries
            vc.model.selectedCountry = model.selectedCountry
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func clickedSendCodeButton(_ sender: UIButton) {
        if model.phoneString == "" {
            presenter.showError(view)
        } else {
            view.endEditing(true)
            sendCodeButton.showLoading()
            model.verifyPhone { [weak self] error in
                if error == nil {
                    self?.sendCodeButton.hideLoading(success: true) {
                        guard let vc = R.storyboard.authorization.cspEnterCodeViewController_id(),
                            let phone = self?.model.phone,
                            let mode = self?.mode else {
                                return
                        }
                        vc.model.phone = phone
                        vc.mode = mode
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    self?.sendCodeButton.hideLoading(success: false) {
                        if let vc = R.storyboard.authorization.cspPhoneFailureViewController_id() {
                            self?.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func textFieldTextChanged(_ sender: UITextField) {
        model.phoneString = sender.text ?? ""
    }
    
}

extension CSPEnterPhoneViewController: CSPCountrySelectionDelegate {
    
    func didSelect(country: CSPCountry) {
        model.selectedCountry = country
        setupCountryCodeButton(forCountry: country)
    }
    
}

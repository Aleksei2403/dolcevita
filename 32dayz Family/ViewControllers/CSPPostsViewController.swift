//
//  CSPPostsViewController.swift
//  32dayz Family
//
//  Created by mac on 30.05.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke
import CoreData

class CSPPostsViewController: UIViewController, CSPPostViewControllerDelegate, AdManagerBannerDelegate {
    
    static let shared = CSPPostsViewController()
    let circle = CSPSessionManager.currentCircle
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var buttonAddCommentClicked: UIButton!
    @IBOutlet weak var initialsAuthorView: UIView!
    @IBOutlet weak var initialsAuthorCommentLabel: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var textCommentView: UITextView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var postImagePlaceholderView: UIView!
    @IBOutlet weak var postImageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postContentLabel: UILabel!
    @IBOutlet weak var constraintHeightPostImagePlaceholderView: NSLayoutConstraint!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var heightTextComment: NSLayoutConstraint!
    
    @IBOutlet weak var viewTextHeight: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    var comments = [CSPCommentModel]()
    var posts : [CSPPostModel] = []
    var post : CSPPostModel?
    var uidComment : String?
    var userNameComment: String = ""
    var authorComment = CSPSessionManager.currentUser
   
    fileprivate let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialsAuthorView.layer.cornerRadius = 23
        authorImageView.layer.cornerRadius = 23
        textCommentView.layer.borderWidth = 1
        textCommentView.layer.borderColor = UIColor.blackk.cgColor
        textCommentView.layer.cornerRadius = 6
        let xib = UINib(nibName: "CSPCommentsViewCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "CSPCommentsViewCell_id")
        commentsLabel.font = UIFont(name: R.font.latoRegular.fontName, size: 11)
        buttonAddCommentClicked.setTitle("Add Comment", for: .normal)
        buttonAddCommentClicked.titleLabel?.font =  UIFont(name: R.font.latoRegular.fontName, size: 14)
        buttonAddCommentClicked.layer.cornerRadius = 6
        buttonAddCommentClicked.setTitleColor(.white, for: .normal)
        buttonAddCommentClicked.backgroundColor = UIColor.turquoiseBlue
        buttonAddCommentClicked.addTarget(self, action: #selector(buttonFooterClicked(_:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
       hideKeyboardWhenTappedAround()
        registerNotifications()
        for i in posts {
            post = i
            userNameLabel.text = i.author?.fullName
            uidComment = i.uid
            initialsAuthorCommentLabel.text = authorComment?.initials
            if let photo = authorComment?.photoURL {
                Nuke.loadImage(with: URL(string: photo)!, into: authorImageView)
            }
            
            let dateString = "Posted \(i.createdAt!.stringRepresentation())"
            postDateLabel.text = dateString
            postContentLabel.text = i.content
            if let url = i.author?.photoURL {
                userPhotoImageView.isHidden = false
                userInitialsView.isHidden = true
                Nuke.loadImage(with: URL(string: url)!, into: userPhotoImageView)
            } else {
                userPhotoImageView.isHidden = true
                userInitialsView.isHidden = false
                userInitialsLabel.text = i.author?.initials
            }
            
            if let mediaURL = i.mediaURL {
                postImageActivityIndicator.startAnimating()
                postImagePlaceholderView.isHidden = false
                constraintHeightPostImagePlaceholderView.constant = UIScreen.main.bounds.width * CGFloat(i.mediaAspectRatio?.doubleValue ?? 1)
                Nuke.loadImage(with: URL(string: mediaURL)!, into: postImageView)
            } else {
                postImagePlaceholderView.isHidden = true
            }
        }
        getComments(withUID: uidComment!)
        tableView.reloadData()
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newValue = change?[.newKey] {
                let newSize = newValue as! CGSize
                self.tableViewHeight.constant = newSize.height
            }
        }
    }
    
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
 
    
    @objc func keyboardWillShow(_ notification: Foundation.Notification) {

        let userInfo = notification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = self.view.convert(keyboardFrame, from: nil)

            var contentInset:UIEdgeInsets = self.scrollView!.contentInset
            contentInset.bottom = keyboardFrame.size.height + 20
            let loc = self.textCommentView?.convert(textCommentView!.bounds, to: self.view)

            if keyboardFrame.origin.y <  loc!.origin.y {
                self.scrollView?.contentOffset = CGPoint.init(x: (self.scrollView?.contentOffset.x)!, y: loc!.origin.y)
            }

            if self.scrollView?.contentInset.bottom == 0 {
                self.scrollView?.contentInset = contentInset
            }

        }

    @objc func keyboardWillHide(notification:NSNotification) {
        scrollView.contentOffset = .zero
    }

    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @IBAction func buttonDeletePost(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this post? This can not be undone.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
            CSPAPIManager.shared.deletePost(self.post!) { [weak self] error in
                if error == nil {
                    CSPAPIManager.shared.getCircles { (error) in
                        self?.backClickedButton((Any).self)
                        NotificationCenter.default.post(name: NN.Update, object: nil)
                    }
                    
                } else {
                    //pm show alert error
                }
            }
        }
        alert.addAction(cancel)
        alert.addAction(delete)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func buttonEditPostClicked(_ sender: Any) {
        if let vc = R.storyboard.main.cspPostViewController_id() {
            vc.post = post
            vc.delegate = self
            let nc = CSPNavigationController(rootViewController: vc)
            self.present(nc, animated: true, completion: nil)
        }
    }
    
    @IBAction func backClickedButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func getComments(withUID uid: String){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CSPCommentModel")
        do {
            comments = try CSPDBManager.shared.managedObjectContext.fetch(fetchRequest) as! [CSPCommentModel]
            
        } catch {
            print(error)
        }
    }
    
    func cspPostViewControllerSaveClicked(with postText: String) {
        postContentLabel.text = postText
    }
    

    public func saveComment(content: String, uid: String, postId: String, author: String,authorImage: String, circleId: String, createdAt: NSDate) {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        if let entity = NSEntityDescription.entity(forEntityName: "CSPCommentModel", in: managedContext){
            let comment = NSManagedObject(entity: entity, insertInto: managedContext)
            comment.setValue(content, forKey: "content")
            comment.setValue(uid, forKey: "uid")
            comment.setValue(postId, forKey: "postId")
            comment.setValue(author, forKey: "author")
            comment.setValue(authorImage, forKey: "authorImage")
            comment.setValue(circleId, forKey: "circleId")
            comment.setValue(createdAt, forKey: "createdAt")
            do {
                try managedContext.save()
                comments.append(comment as! CSPCommentModel)
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func delete(comment: NSManagedObject) {
        CSPDBManager.shared.managedObjectContext.delete(comment)
        do {
            try CSPDBManager.shared.managedObjectContext.save()
        } catch {
            print(error)
        }
    }
    
    @objc func buttonFooterClicked(_ sender: Any) {
        textCommentView.resignFirstResponder()
        CSPAPIManager.shared.saveComment(content: self.textCommentView.text, uid: uidComment ?? "", authorImage: authorComment?.photoURL ?? "") {  error in
            
        }
        
        self.saveComment(content: self.textCommentView.text, uid: uidComment ?? "", postId: uidComment ?? "", author: authorComment?.initials ?? "", authorImage: authorComment?.photoURL ?? "", circleId: circle?.uid ?? "", createdAt: NSDate() )
        
        self.tableView.reloadData()
        self.textCommentView.text = ""
        textCommentView.becomeFirstResponder()
    }
    
}

extension CSPPostsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        commentsLabel.text = "COMMENTS(\(comments.filter({ return $0.postId == uidComment}).count))"
        return comments.filter({ return $0.postId == uidComment}).count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let comment = comments.filter({ return $0.postId == uidComment})[sourceIndexPath.item]
        comments.remove(at: sourceIndexPath.item)
        comments.insert(comment, at: destinationIndexPath.item)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let comment = comments.filter({ return $0.postId == uidComment})[indexPath.row]
            comments.remove(at: indexPath.item)
            CSPAPIManager.shared.deleteComment(comment) { (error) in
                
            }
            self.delete(comment: comment)
            DispatchQueue.main.async {
                tableView.reloadData ()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CSPCommentsViewCell_id", for: indexPath) as? CSPCommentsViewCell {
            let comment = comments.filter({ return $0.postId == uidComment})[indexPath.row]
            var date = NSDate()
            if let dt = comment.createdAt {
                date = dt as NSDate
            }
            let dateLabel = "Posted \(date.stringRepresentation())"
            cell.dateCommentLabel.text = dateLabel
            cell.authorCommentLabel.text = comment.value(forKey: "author") as? String
            cell.textCommentLabel.text = comment.value(forKey: "content") as? String
            cell.userInitialsLabel.text = comment.value(forKey: "author") as? String
            Nuke.loadImage(with: URL(string: comment.value(forKey: "authorImage") as? String ?? "")!, into: cell.userPhotoImageView)
            return cell
        }
        return UITableViewCell()
    }
}



extension CSPPostsViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}


//
//  CSPTasksViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/18/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class CSPTasksViewController: CSPFloatingButtonViewController, AdManagerBannerDelegate, UISearchBarDelegate {
    
    enum ButtonChecked {
        case first
        case second
        case third
        case forth
        case five
        }
    
    let model = CSPTasksModel()
    var buttonChecked: ButtonChecked = .first
    lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        rc.tintColor = UIColor(0x2cbdea)
        return rc
    }()
    
    lazy var dateSelectorView: CSPDateSelectorView = {
        let dsv = CSPDateSelectorView.instanceFromNib()
        dsv.onDateSelection = { [weak self] date in
            guard let task = self?.model.currentTask else { return }
            task.date = date
            self?.saveTaskAndReload(task)
        }
        dsv.onDatePick = { [weak self] in
            self?.datePickerView.show()
        }
        dsv.onDateRemove = { [weak self] in
            guard let task = self?.model.currentTask else { return }
            task.date = nil
            self?.saveTaskAndReload(task)
        }
        return dsv
    }()
    
    lazy var datePickerView: CSPDatePickerView = {
        let dpv = CSPDatePickerView.instanceFromNib()
        dpv.onDatePick = { [weak self] date in
            guard let task = self?.model.currentTask else { return }
            task.date = date as NSDate
            self?.saveTaskAndReload(task)
        }
        return dpv
    }()
    
    var allTasks: [CSPTaskModel] = []
    var myTasks: [CSPTaskModel] = []
    var allTasksArray: [(NSDate?, [CSPTaskModel])] = []
    var myTasksArray: [(NSDate?, [CSPTaskModel])] = []
    var overdueTasks: [CSPTaskModel] = []
    var tasksAll : [CSPTaskModel] = []
    var tasksMy : [CSPTaskModel] = []
    var myOverdueTasks: [CSPTaskModel] = []
    var tableSection = 0
    @IBOutlet weak var sortedButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyImageView: UIImageView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var cancelSearchButton: UIButton!
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        searchBar?.isHidden = false
        tableView.addSubview(refreshControl)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: R.font.myriadProRegular(size: 15)!], for: .normal)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: R.font.myriadProRegular(size: 15)!, NSAttributedString.Key.foregroundColor: UIColor(0x2cbdea)], for: .selected)
        tableView.estimatedRowHeight = 130
        tableView.tableFooterView = UIView(frame: .zero)
        
        prepareUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = .white
        }
        self.myTasksArray = model.myTasksArray
        self.allTasksArray = model.allTasksArray
        allTasks = model.allTasks
        myTasks = model.myTasks
        searchBar.delegate = self
        searchBar.isHidden = true
        cancelSearchButton.isHidden = true
    }
    fileprivate func prepareUI() {
        switch model.mode {
        case .Today:
            titleLabel.text = "Today"
            emptyImageView.image = R.image.todayEmptyState()
            emptyLabel.text = "No tasks for today\nEnjoy your day!"
            break
        case .Next7Days:
            titleLabel.text = "Next 7 Days"
            emptyImageView.image = R.image.daysEmptyIcon()
            emptyLabel.text = "No tasks for the next 7 days"
            break
        case .Someday:
            titleLabel.text = "Someday"
            emptyImageView.image = R.image.daysEmptyIcon()
            emptyLabel.text = "No someday tasks"
            break
        case .Inbox:
            titleLabel.text = "Inbox"
            emptyImageView.image = R.image.inboxEmptySate()
            emptyLabel.text = "Your Inbox is empty"
            break
        case .CustomList:
            titleLabel.text = model.list?.name
            emptyImageView.image = R.image.customEmptyState()
            emptyLabel.text = "This List is empty"
            
            break
        default:
            break
        }
        
        reloadUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModel), name: NN.Update, object: nil)
        
        if model.mode == .Inbox || model.mode == .CustomList {
            let lp = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
            tableView.addGestureRecognizer(lp)
        }
    }

    fileprivate func reloadUI() {
        if segmentedControl.selectedSegmentIndex == 0 {
            tableView.isHidden = model.allTasks.count == 0 && model.overdueTasks.count == 0
        } else if segmentedControl.selectedSegmentIndex == 1 {
            tableView.isHidden = model.myTasks.count == 0 && model.myOverdueTasks.count == 0
        }
        tableView.reloadData()
    }
    
    fileprivate func deleteFromModel(_ task: CSPTaskModel) {
        if let index = model.overdueTasks.firstIndex(of: task) {
            model.overdueTasks.remove(at: index)
        }
        if let index = model.allTasks.firstIndex(of: task) {
            model.allTasks.remove(at: index)
        }
        model.completeTask(task) { _ in
            NotificationCenter.default.post(name: NN.Update, object: nil)
        }
    }
    
    fileprivate func saveTaskAndReload(_ task: CSPTaskModel) {
        model.saveTask(task) { error in
            if error == nil {
                NotificationCenter.default.post(name: NN.Update, object: nil)
            } else {
                //pm alert with error
            }
        }
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        model.getCircles { [weak self] _ in
            self?.reloadModel()
            DispatchQueue.main.async {
                refreshControl.endRefreshing()
            }
        }
    }
    
    private var snapshot: UIView? = nil
    private var sourceIndexPath: IndexPath? = nil
    
    @objc private func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        let state = gestureRecognizer.state
        let location = gestureRecognizer.location(in: tableView)
        let ip = tableView.indexPathForRow(at: location)
        switch state {
        case .began:
            if ip != nil {
                sourceIndexPath = ip
                if let cell = tableView.cellForRow(at: ip!) {
                    snapshot = customSnapshot(from: cell)
                    var center = cell.center
                    snapshot?.center = center
                    snapshot?.alpha = 0
                    tableView.addSubview(snapshot!)
                    UIView.animate(withDuration: 0.25, animations: {
                        center.y = location.y
                        self.snapshot?.center = center
                        self.snapshot?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                        self.snapshot?.alpha = 0.98
                        cell.alpha = 0
                    }) { _ in
                        cell.isHidden = true
                    }
                }
            }
            break
        case .changed:
            if var center = snapshot?.center {
                center.y = location.y
                snapshot?.center = center
                if ip != nil && ip != sourceIndexPath {
                    model.swapAt(sourceIndexPath!.row, ip!.row)
                    tableView.moveRow(at: sourceIndexPath!, to: ip!)
                    sourceIndexPath = ip
                }
            }
            break
        case .ended:
            if sourceIndexPath != nil {
                model.commitChanges { _ in }
                if let cell = tableView.cellForRow(at: sourceIndexPath!) {
                    cell.isHidden = false
                    cell.alpha = 0
                    UIView.animate(withDuration: 0.25, animations: {
                        self.snapshot?.center = cell.center
                        self.snapshot?.transform = .identity
                        self.snapshot?.alpha = 0
                        cell.alpha = 1
                    }) { _ in
                        self.sourceIndexPath = nil
                        self.snapshot?.removeFromSuperview()
                        self.snapshot = nil
                    }
                }
            }
            break
        default:
            break
        }
    }
    
    @objc fileprivate func reloadModel() {
        model.reload()
        DispatchQueue.main.async {
            self.reloadUI()
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    private func customSnapshot(from view: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let snapshot = UIImageView(image: image)
        snapshot.layer.masksToBounds = false
        snapshot.layer.cornerRadius = 0.0
        snapshot.layer.shadowOffset = CGSize(width: -5, height: 0)
        snapshot.layer.shadowRadius = 5.0
        snapshot.layer.shadowOpacity = 0.4
        return snapshot
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.model.mode == .Inbox || self.model.mode == .CustomList {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.model.allTasks = []
            } else {
                self.model.myTasks = []
            }
        } else {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                if self.tableSection == 0 && self.overdueTasks.count > 0 {
                    self.model.overdueTasks = []
                }
                self.model.allTasksArray = []
                
            } else {
                if self.tableSection == 0 && self.myOverdueTasks.count > 0 {
                    self.model.myOverdueTasks = []
                }
                self.model.myTasksArray = []
            }
        }
        DispatchQueue.main.async {
            if searchText == "" {
                self.reloadModel()
            } else {
                if self.model.mode == .Inbox || self.model.mode == .CustomList {
                    if self.segmentedControl.selectedSegmentIndex == 0 {
                        for task in self.allTasks {
                            if let content = task.content {
                                if content.lowercased().contains(searchText.lowercased()){
                                    self.model.allTasks.append(task)
                                }
                            }
                        }
                    } else {
                        for task in self.myTasks {
                            if let content = task.content {
                                if content.lowercased().contains(searchText.lowercased()){
                                    self.model.myTasks.append(task)
                                }
                            }
                        }
                    }
                } else {
                    if self.segmentedControl.selectedSegmentIndex == 0 {
                        if self.tableSection == 0 && self.overdueTasks.count > 0 {
                            for task in self.overdueTasks {
                                if let content = task.content {
                                    if content.lowercased().contains(searchText.lowercased()){
                                        self.model.overdueTasks.append(task)
                                    }
                                }
                            }
                             
                        }
                        let index = self.model.overdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                        if let allTasks = self.allTasksArray[safe: index]?.1 {
                            self.model.allTasksArray = self.sortedArray(allTasks.filter({return ($0.content?.lowercased().contains(searchText.lowercased()))! }))
                        }
                        
                    } else {
                        if self.tableSection == 0 && self.myOverdueTasks.count > 0 {
                            for task in self.myOverdueTasks {
                                if let content = task.content {
                                    if content.lowercased().contains(searchText.lowercased()){
                                        self.model.myOverdueTasks.append(task)
                                    }
                                }
                            }
                        }
                        let index = self.myOverdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                        if let myTasks = self.myTasksArray[safe: index]?.1 {
                            self.model.myTasksArray = self.sortedArray(myTasks.filter({return ($0.content?.lowercased().contains(searchText.lowercased()))! }))
                        }
                    }
                }
                
            }
            
            self.tableView.reloadData()
        }
        
    }
    private func sortedDueDate() {
        if self.model.mode == .Inbox || self.model.mode == .CustomList {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.model.allTasks = self.allTasks.sorted { $0.start ?? "" > $1.start ?? "" }
            } else {
                self.model.myTasks = self.myTasks.sorted { $0.start ?? "" > $1.start ?? "" }
            }
        } else {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                if self.tableSection == 0 && self.overdueTasks.count > 0 {
                    self.model.overdueTasks = self.overdueTasks.sorted { $0.start ?? "" > $1.start ?? "" }
                }
                let index = self.overdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                
                if let allTasks = self.allTasksArray[safe: index]?.1 {
                    self.model.allTasksArray = self.sortedArray(allTasks.sorted(by: { $0.start ?? "" > $1.start ?? "" }))
                }
            } else {
                if self.tableSection == 0 && self.myOverdueTasks.count > 0 {                    self.model.myOverdueTasks = self.myOverdueTasks.sorted(by: { $0.start ?? "" > $1.start ?? "" })
                }
                let index = self.myOverdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                if let myTasks = self.myTasksArray[safe: index]?.1 {
                    self.model.myTasksArray = self.sortedArray(myTasks.sorted(by: { $0.start ?? "" > $1.start ?? "" }))
                }
                
            }
        }
        
        self.tableView.reloadData()
    }
    private func alphabeticallySorted() {
        if self.model.mode == .Inbox || self.model.mode == .CustomList {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                self.model.allTasks = self.allTasks.sorted { $0.content?.lowercased() ?? "" < $1.content?.lowercased() ?? "" }
            } else {
                self.model.myTasks = self.myTasks.sorted { $0.content?.lowercased() ?? "" < $1.content?.lowercased() ?? "" }
            }
        } else {
            if self.segmentedControl.selectedSegmentIndex == 0 {
                if self.tableSection == 0 && self.overdueTasks.count > 0 {
                    self.model.overdueTasks = self.overdueTasks.sorted { $0.content?.lowercased() ?? "" < $1.content?.lowercased() ?? "" }
                }
                let index = self.overdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                
                if let allTasks = self.allTasksArray[safe: index]?.1 {
                            self.model.allTasksArray = self.sortedArray(allTasks.sorted(by: { $0.content?.lowercased() ?? "" < $1.content?.lowercased() ?? "" }))
                }
            } else {
                if self.tableSection == 0 && self.myOverdueTasks.count > 0 {                    self.model.myOverdueTasks = self.myOverdueTasks.sorted(by: { $0.content?.lowercased() ?? "" < $1.content?.lowercased() ?? "" })
                }
                let index = self.myOverdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                if let myTasks = self.myTasksArray[safe: index]?.1 {
                    self.model.myTasksArray = self.sortedArray(myTasks.sorted(by: { $0.content?.lowercased() ?? "" < $1.content?.lowercased() ?? "" }))
                }
                
            }
        }
        
        self.tableView.reloadData()
        
    }
    
    private func sortedPriority() {
        let prioritys = ["None", "Low","Normal","High","Highest"]
        if model.mode == .Inbox || model.mode == .CustomList {
            if segmentedControl.selectedSegmentIndex == 0 {
                self.model.allTasks = []
                for priority in prioritys {
                    for task in self.allTasks {
                        if priority == task.priority {
                            self.model.allTasks.append(task)
                        }
                    }
                }
            } else {
                self.model.myTasks = []
                for priority in prioritys {
                    for task in self.myTasks {
                        if priority == task.priority {
                            self.model.myTasks.append(task)
                        }
                    }
                }
            }
        } else {
            self.model.overdueTasks = []
            if self.segmentedControl.selectedSegmentIndex == 0 {
                for priority in prioritys {
                    for task in self.overdueTasks {
                        if priority == task.priority {
                            self.model.overdueTasks.append(task)
                        }
                    }
                }
                self.model.allTasksArray = []
                self.tasksAll = []
                let index = self.overdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                if let allTasks = self.allTasksArray[safe: index]?.1 {
                    for priority in prioritys {
                    for task in allTasks {
                        if task.priority == priority {
                            self.tasksAll.append(task)
                            self.model.allTasksArray = self.sortedArray(self.tasksAll)
                        }
                    }
                    }
                }
            } else {
                if self.tableSection == 0 && self.myOverdueTasks.count > 0 {
                    self.model.myOverdueTasks = []
                    for priority in prioritys {
                        for task in self.myOverdueTasks {
                            if priority == task.priority {
                                self.model.myOverdueTasks.append(task)
                            }
                        }
                    }
                }
                
                self.model.myTasksArray = []
                self.tasksMy = []
                let index = self.myOverdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                if let myTasks = self.myTasksArray[safe: index]?.1 {
                    for priority in prioritys {
                        for task in myTasks {
                            if task.priority == priority {
                                self.tasksMy.append(task)
                                self.model.myTasksArray = self.sortedArray(self.tasksMy)
                            }
                        }
                    }
                }
            }
        }
        self.tableView.reloadData()
    }
    private func sortedMembers() {
        let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        if model.mode == .Inbox || model.mode == .CustomList {
            if segmentedControl.selectedSegmentIndex == 0 {
                model.allTasks = []
            } else {
                model.myTasks = []
            }
        } else {
            if segmentedControl.selectedSegmentIndex == 0 {
                if tableSection == 0 && model.overdueTasks.count > 0 {
                    model.overdueTasks = []
                }
                model.allTasksArray = []
            } else {
                if tableSection == 0 && model.myOverdueTasks.count > 0 {
                    model.myOverdueTasks = []
                }
                model.myTasksArray = []
            }
        }
        var members = [CSPUserModel]()
        var users: [CSPUserModel] {
            get {
                return CSPSessionManager.currentCircle?.members?.array as? [CSPUserModel] ?? []
            }
        }
        
        for user in users {
            members.append(user)
        }
        
        for author in members {
            let action = UIAlertAction(title: author.fullName, style: .default) { (action) in
                print("Title: \("user.fullName")")
                if self.model.mode == .Inbox || self.model.mode == .CustomList {
                    if self.segmentedControl.selectedSegmentIndex == 0 {
                        for task in self.allTasks {
                            if task.membersArray.filter({ ($0.fullName == action.title)}).first?.fullName == action.title {
                                self.model.allTasks.append(task)
                            }
                        }
                    } else {
                        for task in self.myTasks {
                            if task.membersArray.filter({ ($0.fullName == action.title)}).first?.fullName == action.title {
                                self.model.myTasks.append(task)
                            }
                        }
                    }
                } else {
                    if self.segmentedControl.selectedSegmentIndex == 0 {
                        if self.tableSection == 0 && self.overdueTasks.count > 0 {
                            for task in self.overdueTasks {
                                if task.membersArray.filter({ ($0.fullName == action.title)}).first?.fullName == action.title {
                                    self.model.overdueTasks.append(task)
                                }
                            }
                        }
                        self.tasksAll = []
                        let index = self.overdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                       
                        if let allTasks = self.allTasksArray[safe: index]?.1 {
                            for task in allTasks {
                                if task.membersArray.filter({ ($0.fullName == action.title)}).first?.fullName == action.title {
                                    self.tasksAll.append(task)
                                    self.model.allTasksArray = self.sortedArray(self.tasksAll)
                                }
                            }
                        }
                    } else {
                        self.tasksMy = []
                        if self.tableSection == 0 && self.myOverdueTasks.count > 0 {
                            for task in self.myOverdueTasks {
                                if task.membersArray.filter({ ($0.fullName == action.title)}).first?.fullName == action.title {
                                    self.model.myOverdueTasks.append(task)
                                }
                            }
                        }
                        let index = self.myOverdueTasks.count > 0 ? self.tableSection - 1 : self.tableSection
                        if let myTasks = self.myTasksArray[safe: index]?.1 {
                            for task in myTasks {
                                if task.membersArray.filter({ ($0.fullName == action.title)}).first?.fullName == action.title {
                                    self.tasksMy.append(task)
                                    self.model.myTasksArray = self.sortedArray(self.tasksMy)
                                }
                            }
                        }
                    }
                }
                self.tableView.reloadData()
            }
            
            actionSheetAlertController.addAction(action)
            
        }
        
        let allMembersButton = UIAlertAction(title: "All Members", style: .default) { (cancel) in
            self.reloadModel()
            self.tableView.reloadData()
        }
        
        actionSheetAlertController.addAction(allMembersButton)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in
            self.tasksAll = []
            self.tasksMy = []
            self.reloadModel()
        }
        actionSheetAlertController.addAction(cancelActionButton)
        
        self.present(actionSheetAlertController, animated: true, completion: nil)
    }
    
    private func sortedArray(_ array: [CSPTaskModel]) -> [(NSDate?, [CSPTaskModel])] {
        let dates = array.compactMap { $0.date?.truncateTime }.removeDuplicates().sorted { $0 < $1 }
        var sortedAray = dates.map { date -> (NSDate?, [CSPTaskModel]) in
            return (date, array.filter { date.truncateTime == $0.date?.truncateTime })
        }
        let tasksWithoutDate = array.compactMap({ $0.date == nil ? $0 : nil })
        if tasksWithoutDate.count > 0 {
            sortedAray.append((nil, tasksWithoutDate))
        }
        return sortedAray
    }
    // MARK: - Actions
    
    @IBAction func sortedTaskButtonClicked(_ sender: UIButton) {
        
        let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let dueDataButton = UIAlertAction(title: "Due Data", style: .default) { [weak self] action in
            self?.sortedDueDate()
        }
        let priorityButton = UIAlertAction(title: "Priority", style: .default) { [weak self] action in
            self?.sortedPriority()
        }
        let alphabeticallyButton = UIAlertAction(title: "Alphabetically", style: .default) { [weak self] action in
            self?.alphabeticallySorted()
            
        }
        let membersButton = UIAlertAction(title: "Members", style: .default) { [weak self] action in
            self?.sortedMembers()
        }
        
                let image = self.resizeImage(image: #imageLiteral(resourceName: "Due data"), targetSize: CGSize(width: 18, height: 25))
        dueDataButton.setValue(image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                let image1 = self.resizeImage(image: #imageLiteral(resourceName: "Flag grey"), targetSize: CGSize(width: 18, height: 25))

        priorityButton.setValue(image1?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                let image2 = self.resizeImage(image: #imageLiteral(resourceName: "alphabeticallypng"), targetSize: CGSize(width: 18, height: 25))
        alphabeticallyButton.setValue(image2?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                let image3 = self.resizeImage(image:#imageLiteral(resourceName: "members"), targetSize: CGSize(width: 18, height: 25))
        membersButton.setValue(image3?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")

        dueDataButton.setValue(UIColor.gray, forKey: "titleTextColor")
        priorityButton.setValue(UIColor.gray, forKey: "titleTextColor")
        alphabeticallyButton.setValue(UIColor.gray, forKey: "titleTextColor")
        membersButton.setValue(UIColor.gray, forKey: "titleTextColor")
        dueDataButton.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        priorityButton.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alphabeticallyButton.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        membersButton.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")

            actionSheetAlertController.addAction(dueDataButton)
        actionSheetAlertController.addAction(priorityButton)
        actionSheetAlertController.addAction(alphabeticallyButton)
        actionSheetAlertController.addAction(membersButton)


        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in

        }
        cancelActionButton.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        actionSheetAlertController.addAction(cancelActionButton)

        self.present(actionSheetAlertController, animated: true, completion: nil)
    }
    @IBAction func cancelSearchButtonClicked(_ sender: Any) {
        searchButton.isHidden = false
        searchBar.isHidden = true
        cancelSearchButton.isHidden = true
        reloadModel()
    }
    
    @IBAction func clickedSearchButton(_ sender: Any) {
        if searchBar.isHidden {
            searchBar.isHidden = false
            searchButton.isHidden = true
            cancelSearchButton.isHidden = false
        }
    }
    @IBAction func clickedMenuButton(_ sender: UIButton) {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }

    @IBAction func clickedSegmentedControl(_ sender: UISegmentedControl) {
        model.expandedCellIndexPath = nil
        reloadUI()
    }
    @IBAction func clickedNewEventButton(_ sender: UIButton) {
        if let vc = R.storyboard.main.cspEventViewController_id() {
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func clickedNewTaskButton(_ sender: UIButton) {
        if CSPSessionManager.isSubscribed || CSPSessionManager.currentCircle?.tasksArray.count ?? 0 < 100 {
            guard (CSPSessionManager.currentCircle?.listsArray.count ?? 0) > 0 else {
                let alertController = UIAlertController(title: "You have no any lists", message: "Please, add a list before adding new tasks", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                present(alertController, animated: true, completion: nil)
                return
            }
            if let vc = R.storyboard.main.cspTaskViewController_id() {
                vc.onTaskCreation = { _ in
                    
                    // MARK: - Analytics
                    
                    AnalyticsManager.logEvent(type: .TaskCreated, parameters: ["task_created" : Parameter(.source, .journal)])
                    // MARK: -
                    
                }
                let nc = CSPNavigationController(rootViewController: vc)
                present(nc, animated: true, completion: nil)
            }
        } else {
            if let vc = R.storyboard.settings.cspGoldViewController_id() {
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func clickedFloatingButton(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let task = UIAlertAction(title: "Add a Task", style: .default) { _ in
            self.clickedNewTaskButton(sender)
        }
        let event = UIAlertAction(title: "Add an Event", style: .default) { _ in
            self.clickedNewEventButton(sender)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        actionSheet.addAction(task)
        actionSheet.addAction(event)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
}

extension CSPTasksViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if model.mode == .Today || model.mode == .Next7Days || model.mode == .Someday {
            if segmentedControl.selectedSegmentIndex == 0 {
                let toAdd = model.overdueTasks.count > 0 ? 1 : 0
                return model.allTasksArray.count + toAdd
            } else if segmentedControl.selectedSegmentIndex == 1 {
                let toAdd = model.myOverdueTasks.count > 0 ? 1 : 0
                return model.myTasksArray.count + toAdd
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if model.mode == .Inbox || model.mode == .CustomList {
            return .leastNormalMagnitude
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if model.mode == .Inbox || model.mode == .CustomList {
            return nil
        } else {
            let header = CSPTasksHeaderView.instanceFromNib()
            if segmentedControl.selectedSegmentIndex == 0 {
                if section == 0 && model.overdueTasks.count > 0 {
                    header.configForOverdue()
                } else {
                    let index = model.overdueTasks.count > 0 ? section - 1 : section
                    header.config(forDate: model.allTasksArray[index].0)
                }
                return header
            } else {
                if section == 0 && model.myOverdueTasks.count > 0 {
                    header.configForOverdue()
                } else {
                    let index = model.myOverdueTasks.count > 0 ? section - 1 : section
                    header.config(forDate: model.myTasksArray[index].0)
                }
                return header
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableSection = section
        if model.mode == .Inbox || model.mode == .CustomList {
            if  segmentedControl.selectedSegmentIndex == 0 {
                return model.allTasks.count
            }  else {
                return model.myTasks.count
            }
        } else {
            if segmentedControl.selectedSegmentIndex == 0 {
                if section == 0 && model.overdueTasks.count > 0 {
                    self.myOverdueTasks = model.myOverdueTasks
                    return model.overdueTasks.count
                }
                let index = model.overdueTasks.count > 0 ? section - 1 : section
                
                return model.allTasksArray[safe: index]?.1.count ?? 0
            } else {
                if section == 0 && model.myOverdueTasks.count > 0 {
                    self.overdueTasks = model.overdueTasks
                    return model.myOverdueTasks.count
                }
                let index = model.myOverdueTasks.count > 0 ? section - 1 : section
                
                return model.myTasksArray[safe: index]?.1.count ?? 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspTaskCell_id.identifier, for: indexPath) as! CSPTaskCell
        
        cell.delegate = self
        
        let isExpanded = indexPath == model.expandedCellIndexPath
        
        if model.mode == .Inbox || model.mode == .CustomList {
            let task = segmentedControl.selectedSegmentIndex == 0 ? model.allTasks[indexPath.row] : model.myTasks[indexPath.row]
            cell.config(withTask: task, mode: model.mode, isExpanded: isExpanded)
        } else {
            if segmentedControl.selectedSegmentIndex == 0 {
                switch indexPath.section {
                case 0:
                    if model.overdueTasks.count > 0 {
                        cell.config(withTask: model.overdueTasks[indexPath.row], mode: .Overdue, isExpanded: isExpanded)
                    } else {
                        let index = model.overdueTasks.count > 0 ? indexPath.section - 1 : indexPath.section
                        let task = model.allTasksArray[index].1[indexPath.row]
                        cell.config(withTask: task, mode: model.mode, isExpanded: isExpanded)
                    }
                default:
                    let index = model.overdueTasks.count > 0 ? indexPath.section - 1 : indexPath.section
                    let task = model.allTasksArray[index].1[indexPath.row]
                    cell.config(withTask: task, mode: model.mode, isExpanded: isExpanded)
                }
            } else if segmentedControl.selectedSegmentIndex == 1 {
                switch indexPath.section {
                case 0:
                    if model.myOverdueTasks.count > 0 {
                        cell.config(withTask: model.myOverdueTasks[indexPath.row], mode: .Overdue, isExpanded: isExpanded)
                    } else {
                        let index = model.myOverdueTasks.count > 0 ? indexPath.section - 1 : indexPath.section
                        let task = model.myTasksArray[index].1[indexPath.row]
                        cell.config(withTask: task, mode: model.mode, isExpanded: isExpanded)
                    }
                default:
                    let index = model.myOverdueTasks.count > 0 ? indexPath.section - 1 : indexPath.section
                    let task = model.myTasksArray[index].1[indexPath.row]
                    cell.config(withTask: task, mode: model.mode, isExpanded: isExpanded)
                }
            }
        }
        
        cell.onDoneClick = { [weak self] task in
            self?.model.expandedCellIndexPath = nil
            self?.deleteFromModel(task)
            self?.reloadUI()
            
            // MARK: - Analytics
            
            let sourceParameter = Parameter(.source, self?.model.mode == .Today ? .todayList : self?.model.mode == .Next7Days ? .next7DaysList : .originalList)
            let gestureParameter = Parameter(.gesture, .taskMenu)
            AnalyticsManager.logEvent(type: .TaskCompleted,parameters: ["post_completed":sourceParameter, "post_completed":gestureParameter])
            // MARK: -
            
        }
        
        cell.onCalendarClick = { [weak self] task in
            self?.model.currentTask = task
            self?.dateSelectorView.show()
        }
        
        cell.onPriorityClick = { [weak self] task in
            let prioritys = ["None", "Low","Normal","High","Highest"]
            let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            for prior in prioritys {

                let action = UIAlertAction(title: prior, style: .default) { [weak self] action in

                    action.setValue(true, forKey: "checked")
                    task.priority = action.title
                    self?.saveTaskAndReload(task)
                    self?.model.expandedCellIndexPath = indexPath == self?.model.expandedCellIndexPath ? nil : indexPath
                }

                if action.title == "None"  {
                    if task.priority == action.title {
                        action.setValue(true, forKey: "checked")
                    }
                    let image = self?.resizeImage(image: #imageLiteral(resourceName: "Vector red"), targetSize: CGSize(width: 18, height: 25))
                    action.setValue(image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                } else if action.title == "Low" {
                    if task.priority == action.title {
                        action.setValue(true, forKey: "checked")
                    }
                    let image = self?.resizeImage(image:#imageLiteral(resourceName: "Vector yellow"), targetSize: CGSize(width: 18, height: 25))
                    action.setValue(image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                }else if action.title == "Normal" {
                    let image = self?.resizeImage(image: #imageLiteral(resourceName: "Vector blue"), targetSize: CGSize(width: 18, height: 25))
                    if task.priority == action.title {
                        action.setValue(true, forKey: "checked")
                    }
                    action.setValue(image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                    
                }else if action.title == "High" {
                    let image = self?.resizeImage(image: #imageLiteral(resourceName: "Vector green"), targetSize: CGSize(width: 18, height: 25))
                    action.setValue(image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                    if task.priority == action.title {
                        action.setValue(true, forKey: "checked")
                    }
                }else if action.title == "Highest" {
                    let image = self?.resizeImage(image: #imageLiteral(resourceName: "Flag grey"), targetSize: CGSize(width: 18, height: 25))
                    action.setValue(image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
                    if task.priority == action.title {
                        action.setValue(true, forKey: "checked")
                    }
                }
                action.setValue(UIColor.black, forKey: "titleTextColor")
                action.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")

                actionSheetAlertController.addAction(action)
            }

            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in

            }
            cancelActionButton.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            actionSheetAlertController.addAction(cancelActionButton)

            self?.present(actionSheetAlertController, animated: true, completion: nil)
        }
        
        cell.onEditClick = { [weak self] task in
            if let vc = R.storyboard.main.cspTaskViewController_id() {
                vc.task = task
                let nc = CSPNavigationController(rootViewController: vc)
                self?.present(nc, animated: true, completion: nil)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if model.expandedCellIndexPath != nil {
            if let cell = tableView.cellForRow(at: model.expandedCellIndexPath!) as? CSPTaskCell {
                cell.collapse()
            }
        }
        model.expandedCellIndexPath = indexPath == model.expandedCellIndexPath ? nil : indexPath
        if model.expandedCellIndexPath != nil {
            if let cell = tableView.cellForRow(at: model.expandedCellIndexPath!) as? CSPTaskCell {
                cell.expand()
            }
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
}

extension CSPTasksViewController: MGSwipeTableCellDelegate {
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        if direction == .leftToRight {
            
            swipeSettings.transition = .drag
            expansionSettings.buttonIndex = 0
            expansionSettings.fillOnTrigger = true
            
            return [
                MGSwipeButton(title: "", icon: R.image.ic_cell_done(), backgroundColor: UIColor(0x58c232)) { [weak self] cell in
                    if let task = (cell as? CSPTaskCell)?.task {
                        self?.model.expandedCellIndexPath = nil
                        self?.deleteFromModel(task)
                        self?.reloadUI()
                        
                        // MARK: - Analytics
                        
                        let sourceParameter = Parameter(.source, self?.model.mode == .Today ? .todayList : self?.model.mode == .Next7Days ? .next7DaysList : .originalList)
                        let gestureParameter = Parameter(.gesture, .swipe)
                        AnalyticsManager.logEvent(type: .TaskCompleted, parameters: ["post_completed":sourceParameter,"post_completed":gestureParameter])
                        // MARK: -
                        
                    }
                    return false
                }
            ]
        } else {
            return nil
        }
    }
    
}



//
//  CSPPostViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/3/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke
import RSKImageCropper
protocol CSPPostViewControllerDelegate {
    func cspPostViewControllerSaveClicked(with postText: String)
}
class CSPPostViewController: UIViewController, AdManagerBannerDelegate {
    
    var post: CSPPostModel!
    var onPostCreation: ((CSPPostModel?, UIImage?) -> ())?
    var delegate: CSPPostViewControllerDelegate?
    static let placeholder = "Your post details..."
    
    private let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var constraintAspectRatioPostImageView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightTextView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightDeleteButton: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightAddImageButton: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomAddImageButton: NSLayoutConstraint!
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        if post == nil {
            post = CSPDBManager.shared.createNewPost()
        }
        
        setup(withPost: post)
        
        addImageButton.imageView?.contentMode = .scaleAspectFill
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        contentTextView.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    //MARK: - Keyboard
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardEndRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintBottomAddImageButton.constant = keyboardEndRect.height
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        constraintBottomAddImageButton.constant = 0
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: -
    
    private func setup(withPost post: CSPPostModel) {
        title = post.uid == nil ? "New Post" : "Edit Post"
        if let url = post.mediaURL {
            postImageView.isHidden = false
            postImageView.removeConstraint(constraintAspectRatioPostImageView)
            constraintAspectRatioPostImageView = NSLayoutConstraint(item: postImageView, attribute: .height, relatedBy: .equal,
                                                                    toItem: postImageView, attribute: .width,
                                                                    multiplier: CGFloat(post.mediaAspectRatio?.floatValue ?? 1), constant: 0)
            postImageView.addConstraint(constraintAspectRatioPostImageView)
            postImageView.layoutIfNeeded()
            Nuke.loadImage(with: URL(string: url)!, into: postImageView)
        } else {
            postImageView.isHidden = true
        }
        if let content = post.content {
            contentTextView.text = content
            contentTextView.textColor = UIColor(0x4c4c4c)
        } else {
            contentTextView.text = CSPPostViewController.placeholder
            contentTextView.textColor = UIColor(0xd2d5db)
        }
        adjustTextViewHeight()
//        if post.author?.uid != CSPSessionManager.currentUser?.uid {
//            constraintHeightDeleteButton.constant = 0
            deleteButton.isHidden = true
//        }
    }
    
    fileprivate func adjustTextViewHeight() {
        let size = contentTextView.sizeThatFits(CGSize(width: contentTextView.frame.width, height: .greatestFiniteMagnitude))
        constraintHeightTextView.constant = size.height
    }

    // MARK: - Actions
    
    @IBAction func clickedAddImageButton(_ sender: UIButton) {
        view.endEditing(true)
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let chooseFromLib = UIAlertAction(title: "Choose from Library", style: .default) { _ in
            self.showImagePicker(withType: .photoLibrary)
        }
        
        let takePhoto = UIAlertAction(title: "Take a Photo", style: .default) { _ in
            self.showImagePicker(withType: .camera)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        
        optionMenu.addAction(chooseFromLib)
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(cancelAction)
        
        present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func clickedCancelButton(_ sender: UIBarButtonItem?) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        delegate?.cspPostViewControllerSaveClicked(with: post.content ?? "")
        if postImageView.image != nil || (post.content != nil && post.content != "") {
            let ai = UIActivityIndicatorView(style: .gray)
            ai.color = UIColor(0x136781)
            ai.hidesWhenStopped = true
            ai.startAnimating()
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: ai)
            CSPAPIManager.shared.savePost(post, postImageView.image) { [weak self] error in
                if error == nil {
                    self?.clickedCancelButton(sender)
                    NotificationCenter.default.post(name: NN.Update, object: nil)
                    if self?.post.uid == nil {
                        self?.onPostCreation?(self?.post, self?.postImageView.image)
                    }
                    
                } else {
                    //pm alert error
                }
                ai.stopAnimating()
                self?.navigationItem.rightBarButtonItem = sender
                
                
            }
        } else {
            //pm alert - no text typed
        }
    }
    
    @IBAction func clickedDeleteButton(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this post? This can not be undone.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
            CSPAPIManager.shared.deletePost(self.post) { [weak self] error in
                if error == nil {
                    self?.clickedCancelButton(nil)
                    NotificationCenter.default.post(name: NN.Update, object: nil)
                } else {
                    //pm show alert error
                }
            }
        }
        alert.addAction(cancel)
        alert.addAction(delete)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: -
    
    private func showImagePicker(withType type: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(type) {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = type
            imagePicker.delegate = self
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
}

extension CSPPostViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == CSPPostViewController.placeholder {
            textView.text = ""
            textView.textColor = UIColor(0x4c4c4c)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            contentTextView.text = CSPPostViewController.placeholder
            contentTextView.textColor = UIColor(0xd2d5db)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        post.content = textView.text
        adjustTextViewHeight()
    }
    
}

extension CSPPostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let vc = RSKImageCropViewController(image: pickedImage, cropMode: .square)
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension CSPPostViewController: RSKImageCropViewControllerDelegate {
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        postImageView.isHidden = false
        postImageView.image = croppedImage
        postImageView.removeConstraint(constraintAspectRatioPostImageView)
        constraintAspectRatioPostImageView = NSLayoutConstraint(item: postImageView, attribute: .height, relatedBy: .equal,
                                                                toItem: postImageView, attribute: .width,
                                                                multiplier: croppedImage.size.height / croppedImage.size.width, constant: 0)
        postImageView.addConstraint(constraintAspectRatioPostImageView)
        postImageView.layoutIfNeeded()
        navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        postImageView.isHidden = false
        postImageView.image = croppedImage
        postImageView.removeConstraint(constraintAspectRatioPostImageView)
        constraintAspectRatioPostImageView = NSLayoutConstraint(item: postImageView, attribute: .height, relatedBy: .equal,
                                                                toItem: postImageView, attribute: .width,
                                                                multiplier: croppedImage.size.height / croppedImage.size.width, constant: 0)
        postImageView.addConstraint(constraintAspectRatioPostImageView)
        postImageView.layoutIfNeeded()
        navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, willCropImage originalImage: UIImage) {
        print("will crop")
    }
    
}

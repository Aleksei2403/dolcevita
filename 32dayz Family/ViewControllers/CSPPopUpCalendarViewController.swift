//
//  CSPPopUpCalendarViewController.swift
//  32dayz Family
//
//  Created by mac on 18.06.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import FSCalendar
protocol CSPPopUpCalendarViewControllerDelegate {
    func cspPopUpCalendarViewControllerDelegate(with day: String)
}

class CSPPopUpCalendarViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource, AdManagerBannerDelegate {
    

    @IBOutlet var view1: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var backButtonClicked: UIButton!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var labelMonht: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate: CSPPopUpCalendarViewControllerDelegate?
    var calendar: FSCalendar!
    var dt : String = ""
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if PURCHASE_MANAGER.isExpired {
            AdManager.shared.delegateBanner = self
            AdManager.shared.createBannerAdInContainerView(viewController: self, unitId: AppDelegate.AdIds.banner.rawValue)
        }
        setCalendar()
        
    }
    
    func setCalendar() {
       calendarView.layer.cornerRadius = 8
        calendar = FSCalendar(frame: CGRect(x: 0.0, y: 0.0, width: self.calendarView.frame.size.width, height: self.calendarView.frame.size.height))
        calendar.scrollDirection = .horizontal
        calendar.headerHeight = 60
        calendar.firstWeekday = 2
        calendar.dataSource = self
        calendar.delegate = self
        calendar.appearance.headerDateFormat = "dd MMMM yyyy"
        calendar.appearance.caseOptions = FSCalendarCaseOptions.weekdayUsesUpperCase
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        calendar.appearance.weekdayFont = R.font.latoRegular(size: 12)
        calendar.appearance.todayColor = .turquoiseBlue
        calendar.appearance.titleFont = R.font.latoRegular(size: 12)
        calendar.appearance.headerTitleColor = UIColor(0.0, 0.0, 0.0, 0.87)
        calendar.appearance.weekdayTextColor = UIColor(0.0, 0.0, 0.0, 0.57)
        calendar.appearance.headerTitleFont = R.font.latoRegular(size: 20)
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 0.40
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.addSubview(blurEffectView)
        calendarView.addSubview(calendar)
        
    }
    
    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        dt = formatter.string(from: date)
       
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
        
   
    }

    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
    
    @IBAction func backClicked(_ sender: Any) {
        print("back")
        if dt != "" {
        NotificationCenter.default.post(name: popUpCalendarNotificationName, object: dt)
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func previousMonthButton(_ sender: Any) {
        calendar.setCurrentPage(getPreviousMonth(date: calendar.currentPage), animated: true)
    }
    
    @IBAction func nextMonthButton(_ sender: Any) {
        calendar.setCurrentPage(getNextMonth(date: calendar.currentPage), animated: true)
    }
    
}

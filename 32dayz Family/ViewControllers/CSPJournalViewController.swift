//
//  CSPJournalViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//
import GoogleMobileAds
import UIKit
import Nuke
import StoreKit
import CoreData
import FBSDKCoreKit
import Amplitude


fileprivate let isPresentedRateUSKey = "isPresentedRateUSKey"


class CSPJournalViewController: CSPFloatingButtonViewController, GADBannerViewDelegate {
    
    let model = CSPJournalModel()

    let circle = CSPSessionManager.currentCircle
    var tableItems = [AnyObject]()
    var adsToLoad = [GADBannerView]()
    var loadStateForAds = [GADBannerView: Bool]()
    let adUnitID = "ca-app-pub-2106792415220263/7082031750"
    // A banner ad is placed in the UITableView once per `adInterval`. iPads will have a
    // larger ad interval to avoid mutliple ads being on screen at the same time.
    let adInterval = UIDevice.current.userInterfaceIdiom == .phone ? 4 : 2
    // The banner ad height.
    let adViewHeight = CGFloat(200)
    @IBOutlet weak var banner: GADBannerView!
    @IBOutlet weak var controllerTitle: UILabel!
    @IBOutlet weak var userPhotoImageView: AvatarImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var newPostButton: UIButton!
    @IBOutlet weak var emptyStateImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var comments = [CSPCommentModel]()
    var uidComment: String = ""
    fileprivate var isPresentedRateUS: Bool {
        get { UserDefaults.standard.bool(forKey: isPresentedRateUSKey) }
        set { UserDefaults.standard.set(true, forKey: isPresentedRateUSKey) }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        rc.tintColor = UIColor(0x2cbdea)
        return rc
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppEvents.logEvent(.viewedContent)
        tableView.register(
          UINib(nibName: "BannerTableViewCell", bundle: nil),
          forCellReuseIdentifier: "BannerTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.addSubview(refreshControl)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 135
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModel), name: NN.Update, object: nil)
        if !isPresentedRateUS {
            scheduleOnMainQueueAfter(delay: 2.0) {
                SKStoreReviewController.requestReview()
            }
            isPresentedRateUS = true
        }
        
    }
    
    func addBannerAds() {
      var index = adInterval
      // Ensure subview layout has been performed before accessing subview sizes.
      tableView.layoutIfNeeded()
        while index < model.posts.count {
        let adView = GADBannerView()
        model.posts.insert(adView, at: index)
        adsToLoad.append(adView)
        loadStateForAds[adView] = false

        index += adInterval
      }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadModel()
        
        if PURCHASE_MANAGER.isExpired {
            addBannerAds()
            GADMobileAds.sharedInstance().start(completionHandler: nil)
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ kGADSimulatorID ]
            if model.posts.count <= 4 {
                if PURCHASE_MANAGER.isExpired {
                banner.adUnitID = AppDelegate.AdIds.banner.rawValue
                banner.load(GADRequest())
                banner.rootViewController = self
                }
            }
            
        }
        
        
        controllerTitle.text = CSPSessionManager.currentCircle?.name
        if CSPSessionManager.currentUser?.photoURL != nil {
            userPhotoImageView.set(user: CSPSessionManager.currentUser!)
            userPhotoImageView.isHidden = false
            userInitialsView.isHidden = true
        } else {
            userPhotoImageView.isHidden = true
            userInitialsView.isHidden = false
            userInitialsLabel.text = CSPSessionManager.currentUser?.initials
        }
       
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        model.getCircles { [weak self] _ in
            self?.reloadModel()
            DispatchQueue.main.async {
                refreshControl.endRefreshing()
            }
        }
    }
    
    @objc fileprivate func reloadModel() {
        model.reload()
        DispatchQueue.main.async {
            self.reloadUI()
        }
    }
    
    fileprivate func reloadUI() {
        tableView.reloadData()
        emptyStateImage.isHidden = model.posts.count != 0
        tableView.backgroundColor = model.posts.count == 0 ? .white : UIColor(0xf7f7f7)
    }
    
    // MARK: - Actions
    
    @IBAction func clickedMenuButton(_ sender: UIButton) {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    @IBAction func clickedInviteMembersButton(_ sender: UIButton) {
        if let vc = R.storyboard.account.cspInviteMembersViewController_id() {
            vc.model.circleUID = CSPSessionManager.currentCircle?.uid
            vc.mode = .Journal
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func clickedNewEventButton(_ sender: UIButton) {
        if let vc = R.storyboard.main.cspEventViewController_id() {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func clickedNewPostButton(_ sender: UIButton) {
        if let vc = R.storyboard.main.cspPostViewController_id() {
            vc.onPostCreation = { [weak self] post, image in
                self?.tableView.scrollToTop(animated: false)
                
                // MARK: - Analytics
                
                let sourceParameter = Parameter(.source, sender == self?.floatingButton ? .button : .header)
                let contentParameter = Parameter(.content, post?.content != nil && post?.content != "" && image != nil ? .textAndPhoto : image == nil ? .text : .photo)
                AnalyticsManager.logEvent(type: .PostCreated, parameters: ["post_created" : sourceParameter, "post_created": contentParameter])
                
                // MARK: -
                
            }
            let nc = CSPNavigationController(rootViewController: vc)
            present(nc, animated: true, completion: nil)
        }
    }
    
    @IBAction func clickedNewTaskButton(_ sender: UIButton) {
        if CSPSessionManager.isSubscribed || CSPSessionManager.currentCircle?.tasksArray.count ?? 0 < 100 {
            guard (CSPSessionManager.currentCircle?.listsArray.count ?? 0) > 0 else {
                let alertController = UIAlertController(title: "You have no any lists", message: "Please, add a list before adding new tasks", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                present(alertController, animated: true, completion: nil)
                return
            }
            if let vc = R.storyboard.main.cspTaskViewController_id() {
                vc.onTaskCreation = { _ in
                    
                    // MARK: - Analytics
                    
                    AnalyticsManager.logEvent(type: .TaskCreated, parameters: ["task_created":Parameter(.source, .journal)])
                    
                    // MARK: -
                    
                }
                let nc = CSPNavigationController(rootViewController: vc)
                present(nc, animated: true, completion: nil)
            }
        } else {
            if let vc = R.storyboard.settings.cspGoldViewController_id() {
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func clickedFloatingButton(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let post = UIAlertAction(title: "Add a Post", style: .default) { _ in
            self.clickedNewPostButton(sender)
        }
        let task = UIAlertAction(title: "Add a Task", style: .default) { _ in
            self.clickedNewTaskButton(sender)
        }
        let event = UIAlertAction(title: "Add an Event", style: .default) { _ in
            self.clickedNewEventButton(sender)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        //        actionSheet.addAction(invite)
        actionSheet.addAction(task)
        actionSheet.addAction(post)
        actionSheet.addAction(event)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func getComments(withUID uid: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CSPCommentModel")
        do {
            comments = try CSPDBManager.shared.managedObjectContext.fetch(fetchRequest) as! [CSPCommentModel]
        } catch {
            print(error)
        }
    }
    
}

extension CSPJournalViewController: UITableViewDataSource, UITableViewDelegate {


 func numberOfSections(in tableView: UITableView) -> Int {
  return 1
}

 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
  if let tableItem = model.posts[indexPath.row] as? GADBannerView {
    //let isAdLoaded = loadStateForAds[tableItem]
    return 230
  }
  return UITableView.automaticDimension
}

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return model.posts.count
}

 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath ) -> UITableViewCell {

    if let bannerView = model.posts[indexPath.row] as? GADBannerView {
      let reusableAdCell = tableView.dequeueReusableCell(
        withIdentifier: "BannerTableViewCell",
        for: indexPath) as! BannerTableViewCell
        // Remove previous GADBannerView from the content view before adding a new one.
        for subview in reusableAdCell.contentView.subviews {
          subview.removeFromSuperview()
        }
        let newView = UIView()
        bannerView.frame = CGRect(x: 0, y: 0, width: reusableAdCell.contentView.frame.width, height: reusableAdCell.contentView.frame.height)
      
        bannerView.adUnitID = AppDelegate.AdIds.banner.rawValue
        bannerView.load(GADRequest())
        bannerView.rootViewController = self
            // Center GADBannerView in the table cell's content view.
        
       bannerView.center = reusableAdCell.contentView.center
        bannerView.backgroundColor = .white
        newView.frame = CGRect(x: 0, y: 0, width: reusableAdCell.contentView.frame.width, height: reusableAdCell.contentView.frame.height)
        newView.backgroundColor = .white
        newView.addSubview(bannerView)
        reusableAdCell.contentView.addSubview(newView)
        
      return reusableAdCell

  } else {
    let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspPostCell_id.identifier, for: indexPath) as! CSPPostCell
    uidComment = model.posts[indexPath.row].uid ?? ""
    
    CSPAPIManager.shared.downloadComments(for: uidComment) { array in
        for item in array {
            if let travelJson = item as? [String:Any] {
                if let author = travelJson["author"] as? String,
                   let uid = travelJson["uid"] as? String {
                    CSPDBManager.shared.saveComment(uid, array, author)
                }
            }
        }
    }
    
    getComments(withUID: uidComment)
    cell.commentsLabel.text = "COMMENTS(\(comments.filter({ return $0.postId == uidComment}).count))"
    
    cell.config(forPost: model.posts[indexPath.row] as! CSPPostModel)
    cell.onMoreClick = { [weak self] post in
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let edit = UIAlertAction(title: "Edit Post", style: .default) { _ in
            if let vc = R.storyboard.main.cspPostViewController_id() {
                vc.post = post
                let nc = CSPNavigationController(rootViewController: vc)
                self?.present(nc, animated: true, completion: nil)
            }
        }
        let delete = UIAlertAction(title: "Delete Post", style: .destructive) { _ in
            let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this post? This can not be undone.", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
            let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
                self?.model.deletePost(post) { error in
                    if error == nil {
                        self?.model.getCircles({ (error) in
                            self?.reloadModel()
                        })
                        
                    } else {
                        //pm show alert error
                    }
                }
            }
            alert.addAction(cancel)
            alert.addAction(delete)
            self?.present(alert, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        if post.postType == .Post {
            actionSheet.addAction(edit)
        }
        actionSheet.addAction(delete)
        actionSheet.addAction(cancel)
        self?.present(actionSheet, animated: true, completion: nil)
    }
    
    return cell
    
  }
 }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("clicked post")
        let vc = R.storyboard.main.cspPostsViewController_id()
        vc?.posts.append(model.posts[indexPath.row] as! CSPPostModel)
        navigationController?.pushViewController(vc!, animated: true)
    }
}

//
//  CSPCountrySelectionViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

protocol CSPCountrySelectionDelegate: class {
    func didSelect(country: CSPCountry)
}

class CSPCountrySelectionViewController: UIViewController, AdManagerBannerDelegate {
    
    weak var delegate: CSPCountrySelectionDelegate?
    
    let model = CSPCountrySelectionModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let indexPath = model.indexPathForSelectedCountry() {
            tableView.reloadData()
            tableView.scrollToRow(at: indexPath, at: .middle, animated: false)
        }
        tableView.sectionIndexBackgroundColor = .white
        tableView.sectionIndexTrackingBackgroundColor = .white
        tableView.sectionIndexColor = UIColor(0x256bd5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    @IBAction func clickedDoneButton(_ sender: UIBarButtonItem) {
        delegate?.didSelect(country: model.selectedCountry!)
        navigationController?.popViewController(animated: true)
    }
}

extension CSPCountrySelectionViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = CSPAlphabetHeaderView.instanceFromNib()
        header.config(forLetter: model.letter(forSection: section))
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return model.letters.map { String($0) }
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspCountryCell_id.identifier,
                                                 for: indexPath) as! CSPCountryCell
        let country = model.country(atIndexPath: indexPath)
        cell.config(forCountry: country)
        if country.name == model.selectedCountry?.name {
            cell.configSelectedState()
        } else {
            cell.configUnselectedState()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        model.selectedCountry = model.country(atIndexPath: indexPath)
        tableView.reloadData()
    }
    
}

//
//  CSPHomeViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPHomeViewController: UIViewController, AdManagerBannerDelegate {

    let model = CSPHomeModel()
    let contentString = NSMutableAttributedString(string: "By creating account, you are agree to our ToS & Privacy Policy.", attributes: [NSAttributedString.Key.font: R.font.latoRegular(size: 11)!])
    
    @IBOutlet weak var createNewAccountButton: CSPButton!
    @IBOutlet weak var logBackInButton: UIButton!
    @IBOutlet weak var bottomTextView: UITextView!
    @IBOutlet weak var launchScreenView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentString.setAsLink(textToFind: "ToS & Privacy Policy", linkURL: "https://google.com", font: R.font.latoBold(size: 11)!) //pm - fake url
        bottomTextView.attributedText = contentString
        bottomTextView.textAlignment = .center
        bottomTextView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(0x4a5050)]
        
        guard let vc = R.storyboard.authorization.cspSpinnerViewController_id() else { return }
        present(vc, animated: false) { [weak self] in
            self?.launchScreenView.removeFromSuperview()
        }
        
        if CSPSessionManager.currentUser != nil && CSPSessionManager.currentCircle != nil {
            model.getCircles { [weak self] circles in
                if circles.count > 0 {
                    guard let mainViewController = R.storyboard.main.cspJournalViewController_id(),
                        let menuViewController = R.storyboard.main.cspMenuViewController_id() else {
                            CSPAPIManager.shared.signOut()
                            return
                    }
                    let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300)
                    let nc = CSPNavigationController(rootViewController: mainViewController)
                    nc.isNavigationBarHidden = true
                    drawerController.mainViewController = nc
                    drawerController.drawerViewController = menuViewController
                    self?.navigationController?.pushViewController(drawerController, animated: true)
                }
                vc.dismissSelf()
            }
        } else {
            vc.dismissSelf()
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction func clickedCreateNewAccount(_ sender: UIButton) {
        if CSPAPIManager.shared.verificationID == nil {
            if let vc = R.storyboard.authorization.cspEnterPhoneViewController_id() {
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let vc = R.storyboard.authorization.cspEnterCodeViewController_id() {
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func clickedLogBackIn(_ sender: UIButton) {
        if let vc = R.storyboard.authorization.cspEnterPhoneViewController_id() {
            navigationController?.pushViewController(vc, animated: true)
        }
    }

}

//
//  CSPVipViewController.swift
//  32dayz Family
//
//  Created by mac on 6.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPVipViewController: UIViewController, AdManagerBannerDelegate {

    @IBOutlet weak var dolceVitaLabel: UILabel!
    @IBOutlet weak var upgradeLabel: UILabel!
    @IBOutlet weak var upgradeButton: UIButton!
    @IBOutlet weak var bestMobileLabel: UILabel!
    @IBOutlet weak var notNowButton: UIButton!
    @IBOutlet weak var getMontViewLabel: UILabel!
    private var subscriptionsIds: Set<String> {
        get {
            return Set([
                "\(bundleID).1_year_29"
            ])
        }
    }
    
    private var bundleID: String {
        return Bundle.main.bundleIdentifier ?? "error"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getMontViewLabel.font = R.font.latoRegular(size: 28)
        bestMobileLabel.font = R.font.latoRegular(size: 16)
        upgradeButton.layer.cornerRadius = 6
        upgradeButton.titleLabel?.font = R.font.latoRegular(size: 14)
        upgradeButton.titleLabel?.textAlignment = .center
        upgradeButton.setTitle("Upgrade your account\n29,99$/year",for: .normal)
        upgradeButton.titleLabel?.lineBreakMode = .byWordWrapping
        upgradeLabel.font = R.font.latoRegular(size: 14)
        notNowButton.titleLabel?.font = R.font.muliLight(size: 16)
        notNowButton.titleLabel?.textAlignment = .center
        notNowButton.setTitle("Not now",for: .normal)
        notNowButton.titleLabel?.lineBreakMode = .byWordWrapping

    }
  
    @IBAction func backClickedButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func upgradeClickedButton(_ sender: Any) {
        PURCHASE_MANAGER.purchase(productId: "\(bundleID).1_year_29" ) { [weak self] transactionId, success, error in
                     guard success else {
                        self?.presentAlertController(title: "Sorry", text: "The operation can not be completed")
                          return
                    }
                     
                }
            
        }
    
    @IBAction func restorePurchaseButtonClicked(_ sender: Any) {
        PURCHASE_MANAGER.restore { (_: String?, success, error) in
                    
            guard success else {
                self.presentAlertController(title: "Sorry", text: "No active subscription or purchase found")
                return
            }
            self.presentAlertController(title: "Success!", text: "All your purchases have been restored successfully")
                self.navigationController?.popToRootViewController(animated: true)
            
            return
        }
    }
}

//
//  CSPGoldSubscriptionViewController.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/29/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPGoldSubscriptionViewController: UIViewController {
    
    let model = CSPGoldSubscriptionModel()
    
    lazy var numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.formatterBehavior = .behavior10_4
        nf.numberStyle = .currency
        return nf
    }()
    
    @IBOutlet var checkmarkButtons: [UIButton]!
    @IBOutlet var productLabels: [UILabel]!
    @IBOutlet var priceLabels: [UILabel]!
    @IBOutlet var weekLabels: [UILabel]!
    @IBOutlet var activityIndicators: [UIActivityIndicatorView]!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productLabels.forEach { $0.font = R.font.muliBold(size: DDV(13, 13, 16, 16)) }
        
        priceLabels.forEach { $0.font = R.font.muliBold(size: DDV(22, 22, 24, 26)) }
        
        productLabels.forEach { label in
            switch label.tag {
            case 0:
                label.attributedText = model.twelveMonthsAttributedString
                break
            case 1:
                label.attributedText = model.threeMonthsAttributedString
                break
            case 2:
                label.text = model.oneMonthString
                break
            default:
                break
            }
        }
        
        CSPIAPManager.shared.getProducts { [weak self] error in
            if error == nil {
                DispatchQueue.main.async {
                    self?.updateUI()
                }
            } else {
                //pm show alert error
            }
        }
        
    }
    
    fileprivate func updateUI() {
        checkmarkButtons.forEach { $0.isSelected = $0.tag == 0 }
        activityIndicators.forEach { $0.stopAnimating() }
        priceLabels.forEach { label in
            let product = CSPIAPManager.shared.products[label.tag]
            numberFormatter.locale = product.priceLocale
            
            var price = product.price.doubleValue
            if product.productIdentifier.hasSuffix("1month") {
                price = price / Double(4)
            } else if product.productIdentifier.hasSuffix("3months") {
                price = price / Double(12)
            } else if product.productIdentifier.hasSuffix("12months") {
                price = price / Double(48)
            }
            
            if let priceString = numberFormatter.string(from: NSDecimalNumber(value: price)) {
                label.text = priceString
            }
        }
    }
    
    // MARK: - Actions

    @IBAction func clickedCloseButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedCheckmarkButton(_ sender: UIButton) {
        checkmarkButtons.forEach { $0.isSelected = $0 == sender }
    }
    
    @IBAction func clickedBuyNowButton(_ sender: CSPButton) {
        guard let index = checkmarkButtons.filter({ $0.isSelected }).first?.tag else { return }
        sender.showLoading()
        model.purchase(atIndex: index) { [weak self] error in
            if error == nil {
                self?.model.validatePurchase {
                    sender.hideLoading(success: true) {
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
            } else {
                sender.hideLoading(success: false) { }
                //pm show alert error
            }
        }
    }
    
    @IBAction func clickedRestorePurchasesButton(_ sender: CSPButton) {
        sender.showLoading()
        model.restore { [weak self] error in
            if error == nil {
                self?.model.validatePurchase {
                    self?.dismiss(animated: true, completion: nil)
                }
            } else {
                sender.hideLoading(success: false) { }
                //pm show alert error
            }
        }
        
    }
    
}

//
//  CSPCreateCircleModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/19/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPCreateCircleModel {
    
    var circleName = ""
    
    func createCircle(completion: @escaping (Bool) -> ()) {
        CSPAPIManager.shared.createCircle(circleName) { error in
            completion(error == nil)
        }
    }
    
}

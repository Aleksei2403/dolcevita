//
//  CSPInviteModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/20/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPInviteModel {

    var uid: String
    var phone: String
    var circleUID: String
    
    init(_ uid: String, _ dictionary: NSDictionary) {
        self.uid = uid
        self.phone = dictionary[DBKey.Phone] as? String ?? ""
        self.circleUID = dictionary[DBKey.Circle] as? String ?? ""
    }
    
}

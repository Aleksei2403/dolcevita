//
//  CSPCircleSelectionModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/19/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCircleSelectionModel {
    
    var circles = CSPDBManager.shared.getCircles()
    
    var itemSize: CGSize {
        get {
            let width = (UIScreen.main.bounds.width - 20) / 2
            return CGSize(width: width, height: width)
        }
    }
    
    var headerSize: CGSize {
        get {
            return CGSize(width: UIScreen.main.bounds.width - 20, height: 10)
        }
    }
    
    var footerSize: CGSize {
        get {
            return CGSize(width: UIScreen.main.bounds.width - 20, height: 10)
        }
    }
    
    var itemsCount: Int {
        get {
            return circles.count
        }
    }
    
    func getCircle(atIndexPath ip: IndexPath) -> CSPCircleModel {
        return circles[ip.item]
    }
    
}

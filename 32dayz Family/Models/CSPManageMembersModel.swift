//
//  CSPManageMembersModel.swift
//  32dayz Family
//
//  Created by Pavel Maksimov on 9/12/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPManageMembersModel {
    
    var circleUID: String?
    var members: [CSPUserModel]!
    var author: CSPUserModel?
    var invitedMambers:[CSPUserModel]!
    
}

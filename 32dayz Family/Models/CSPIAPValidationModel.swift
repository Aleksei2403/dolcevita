//
//  CSPIAPValidationModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/30/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import ObjectMapper

class CSPIAPValidationModel: Mappable {
    
    var latestReceiptInfo: [CSPReceiptInfo]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        latestReceiptInfo <- map["latest_receipt_info"]
    }
    
}

class CSPReceiptInfo: Mappable {
    
    var transactionId: UInt64!
    var productId: String!
    private var expiresDateString: String!
    private var originalPurchaseDateString: String!
    private var purchaseDateString: String!
    var expiresDate: TimeInterval?
    var originalPurchaseDate: TimeInterval?
    var purchaseDate: TimeInterval?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        transactionId <- map["original_transaction_id"]
        productId <- map["product_id"]
        
        expiresDateString <- map["expires_date_ms"]
        if expiresDateString.count >= 10 {
            expiresDateString = String(expiresDateString[..<expiresDateString.index(expiresDateString.startIndex, offsetBy: 10)])
        }
        expiresDate = TimeInterval(expiresDateString)
        
        originalPurchaseDateString <- map["original_purchase_date_ms"]
        if originalPurchaseDateString.count >= 10 {
            originalPurchaseDateString = String(originalPurchaseDateString[..<originalPurchaseDateString.index(originalPurchaseDateString.startIndex, offsetBy: 10)])
        }
        originalPurchaseDate = TimeInterval(originalPurchaseDateString)
        
        purchaseDateString <- map["purchase_date_ms"]
        if purchaseDateString.count >= 10 {
            purchaseDateString = String(purchaseDateString[..<purchaseDateString.index(purchaseDateString.startIndex, offsetBy: 10)])
        }
        purchaseDate = TimeInterval(purchaseDateString)
    }
    
}

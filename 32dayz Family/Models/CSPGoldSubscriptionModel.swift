//
//  CSPGoldSubscriptionModel.swift
//  32dayz Family
//
//  Created by Pavel Maksimov on 9/12/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPGoldSubscriptionModel {
    
    var textSize: CGFloat = DDV(13, 13, 16, 16)
    
    lazy var twelveMonthsAttributedString: NSMutableAttributedString = {
        let duration = "12 months"
        let save = "Save 40%"
        let attrString = NSMutableAttributedString(string: duration + "\n" + save)
        attrString.addAttribute(NSAttributedString.Key.font, value: R.font.latoLightItalic(size: textSize)!, range: NSMakeRange(duration.count + 1, save.count))
        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(0x40444a), range: NSMakeRange(duration.count + 1, save.count))
        return attrString
    }()
    
    lazy var threeMonthsAttributedString: NSMutableAttributedString = {
        let duration = "3 months"
        let save = "Save 20%"
        let attrString = NSMutableAttributedString(string: duration + "\n" + save)
        attrString.addAttribute(NSAttributedString.Key.font, value: R.font.latoLightItalic(size: textSize)!, range: NSMakeRange(duration.count + 1, save.count))
        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(0x40444a), range: NSMakeRange(duration.count + 1, save.count))
        return attrString
    }()
    
    lazy var oneMonthString: String = {
        return "1 month"
    }()
    
    func purchase(atIndex index: Int, _ completion: @escaping (Error?) -> ()) {
//        CSPIAPManager.shared.purchase(CSPIAPManager.shared.products[index]) { error in
//            completion(error)
//        }
    }
    
    func restore(_ completion: @escaping (Error?) -> ()) {
//        CSPIAPManager.shared.restore { error in
//            completion(error)
//        }
    }
    
    func validatePurchase(_ completion: @escaping () -> ()) {
        CSPAPIManager.shared.validateSubscription {
            completion()
        }
    }
    
}

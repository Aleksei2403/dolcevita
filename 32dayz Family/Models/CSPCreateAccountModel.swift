//
//  CSPCreateAccountModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/19/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCreateAccountModel {
    
    var photo: UIImage?
    var firstName: String = ""
    var lastName: String = ""
    var phone: String = ""
    var id: String = ""
    var circleUid: String = ""
    
    func createAccount(completion: @escaping (Bool) -> ()) {
        CSPAPIManager.shared.createAccount(account: (_: firstName, _: lastName, _: photo)) { error in
            completion(error == nil)
        }
    }
    func createInvaiteAccount(completion: @escaping (Bool) -> ()) {
        CSPAPIManager.shared.createInvitedAccount(account: (_: firstName, _: lastName, _: phone, _: id, _: circleUid)) { error in
            completion(error == nil)
        }
    }
    
    func applyInvites(completion: @escaping ([CSPCircleModel]) -> ()) {
        CSPAPIManager.shared.applyInvites(forPhone: CSPSessionManager.currentUser?.phone ?? "") {
            CSPAPIManager.shared.getCircles { circles in
                completion(circles)
            }
        }
    }
    
}

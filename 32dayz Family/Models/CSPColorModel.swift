//
//  CSPColorModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import UIKit

class CSPColorModel {
    
    var key: String
    var hex: String
    var value: UIColor
    
    init(_ key: String, _ value: String) {
        self.key = key
        self.hex = value
        self.value = UIColor.getUIColor(withHex: value)
    }
    
}

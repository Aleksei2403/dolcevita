//
//  CSPUserModel+CoreDataProperties.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/28/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import CoreData


extension CSPUserModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSPUserModel> {
        return NSFetchRequest<CSPUserModel>(entityName: "CSPUserModel")
    }

    @NSManaged public var createdAt: Date?
    @NSManaged public var firstName: String?
    @NSManaged public var goldUntill: Date?
    @NSManaged public var lastName: String?
    @NSManaged public var notVerification: Bool
    @NSManaged public var phone: String?
    @NSManaged public var photoURL: String?
    @NSManaged public var uid: String?
    @NSManaged public var circleInvitedMembers: CSPCircleModel?
    @NSManaged public var circlesAuthor: NSSet?
    @NSManaged public var circlesMember: NSSet?

}

// MARK: Generated accessors for circlesAuthor
extension CSPUserModel {

    @objc(addCirclesAuthorObject:)
    @NSManaged public func addToCirclesAuthor(_ value: CSPCircleModel)

    @objc(removeCirclesAuthorObject:)
    @NSManaged public func removeFromCirclesAuthor(_ value: CSPCircleModel)

    @objc(addCirclesAuthor:)
    @NSManaged public func addToCirclesAuthor(_ values: NSSet)

    @objc(removeCirclesAuthor:)
    @NSManaged public func removeFromCirclesAuthor(_ values: NSSet)

}

// MARK: Generated accessors for circlesMember
extension CSPUserModel {

    @objc(addCirclesMemberObject:)
    @NSManaged public func addToCirclesMember(_ value: CSPCircleModel)

    @objc(removeCirclesMemberObject:)
    @NSManaged public func removeFromCirclesMember(_ value: CSPCircleModel)

    @objc(addCirclesMember:)
    @NSManaged public func addToCirclesMember(_ values: NSSet)

    @objc(removeCirclesMember:)
    @NSManaged public func removeFromCirclesMember(_ values: NSSet)

}

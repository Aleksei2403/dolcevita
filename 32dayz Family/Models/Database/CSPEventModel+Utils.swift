//
//  CSPEventModel+Utils.swift
//  32dayz Family
//
//  Created by mac on 28.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import Foundation
extension CSPEventModel {
    
    var membersArray: [CSPUserModel] {
        get {
            return members?.array as? [CSPUserModel] ?? []
        }
    }
    
}

//
//  CSPPostModel+CoreDataProperties.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/10/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import CoreData


extension CSPPostModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSPPostModel> {
        return NSFetchRequest<CSPPostModel>(entityName: "CSPPostModel")
    }

    @NSManaged public var content: String?
    @NSManaged public var createdAt: NSDate?
    @NSManaged public var mediaURL: String?
    @NSManaged public var mediaAspectRatio: NSNumber?
    @NSManaged public var title: String?
    @NSManaged public var type: Int16
    @NSManaged public var uid: String?
    @NSManaged public var author: CSPUserModel?
    @NSManaged public var list: CSPListModel?
    @NSManaged public var circle: CSPCircleModel?

}

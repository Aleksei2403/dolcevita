//
//  CSPUserModel+Utils.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/21/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

enum CSPUserModelType: String {
    case Default = "default"
    case Gold = "gold"
}

extension CSPUserModel {
    
    var fullName: String {
        get {
            return firstName! + " " + lastName!
        }
    }
    
    var initials: String {
        get {
            let firstCharacter = firstName!.count > 0 ? String(firstName!.first!) : ""
            let lastCharacter = lastName!.count > 0 ? String(lastName!.first!) : ""
            return firstCharacter + lastCharacter
        }
    }
    
}

//
//  CSPTaskModel+CoreDataProperties.swift
//  32dayz Family
//
//  Created by Pavel Maksimov on 9/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import CoreData


extension CSPTaskModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSPTaskModel> {
        return NSFetchRequest<CSPTaskModel>(entityName: "CSPTaskModel")
    }

    @NSManaged public var content: String?
    @NSManaged public var createdAt: NSDate?
    @NSManaged public var date: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var uid: String?
    @NSManaged public var index: Int16
    @NSManaged public var author: CSPUserModel?
    @NSManaged public var circle: CSPCircleModel?
    @NSManaged public var members: NSOrderedSet?
    @NSManaged public var list: CSPListModel?
    @NSManaged public var start : String?
    @NSManaged public var priority : String?

}

// MARK: Generated accessors for members
extension CSPTaskModel {

    @objc(insertObject:inMembersAtIndex:)
    @NSManaged public func insertIntoMembers(_ value: CSPUserModel, at idx: Int)

    @objc(removeObjectFromMembersAtIndex:)
    @NSManaged public func removeFromMembers(at idx: Int)

    @objc(insertMembers:atIndexes:)
    @NSManaged public func insertIntoMembers(_ values: [CSPUserModel], at indexes: NSIndexSet)

    @objc(removeMembersAtIndexes:)
    @NSManaged public func removeFromMembers(at indexes: NSIndexSet)

    @objc(replaceObjectInMembersAtIndex:withObject:)
    @NSManaged public func replaceMembers(at idx: Int, with value: CSPUserModel)

    @objc(replaceMembersAtIndexes:withMembers:)
    @NSManaged public func replaceMembers(at indexes: NSIndexSet, with values: [CSPUserModel])

    @objc(addMembersObject:)
    @NSManaged public func addToMembers(_ value: CSPUserModel)

    @objc(removeMembersObject:)
    @NSManaged public func removeFromMembers(_ value: CSPUserModel)

    @objc(addMembers:)
    @NSManaged public func addToMembers(_ values: NSOrderedSet)

    @objc(removeMembers:)
    @NSManaged public func removeFromMembers(_ values: NSOrderedSet)

}


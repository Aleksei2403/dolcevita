//
//  CSPCircleModel+CoreDataProperties.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/28/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import CoreData


extension CSPCircleModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSPCircleModel> {
        return NSFetchRequest<CSPCircleModel>(entityName: "CSPCircleModel")
    }
    @NSManaged public var name: String?
    @NSManaged public var uid: String?
    @NSManaged public var author: CSPUserModel?
    @NSManaged public var lists: NSOrderedSet?
    @NSManaged public var members: NSOrderedSet?
    @NSManaged public var posts: NSOrderedSet?
    @NSManaged public var tasks: NSOrderedSet?
    @NSManaged public var invitedMembers: NSOrderedSet?
    @NSManaged public var events: NSOrderedSet?
    @NSManaged public var post: CSPPostModel?
    @NSManaged public var comments: NSOrderedSet?
}
// MARK: Generated accessors for comments
extension CSPCircleModel {

    @objc(insertObject:inCommentsAtIndex:)
    @NSManaged public func insertIntoComments(_ value: CSPCommentModel, at idx: Int)

    @objc(removeObjectFromCommentsAtIndex:)
    @NSManaged public func removeFromComments(at idx: Int)

    @objc(insertComments:atIndexes:)
    @NSManaged public func insertIntoComments(_ values: [CSPCommentModel], at indexes: NSIndexSet)

    @objc(removeCommentsAtIndexes:)
    @NSManaged public func removeFromComments(at indexes: NSIndexSet)

    @objc(replaceObjectInCommentsAtIndex:withObject:)
    @NSManaged public func replaceComments(at idx: Int, with value: CSPCommentModel)

    @objc(replaceCommentsAtIndexes:withLists:)
    @NSManaged public func replaceComments(at indexes: NSIndexSet, with values: [CSPCommentModel])

    @objc(addCommentsObject:)
    @NSManaged public func addToComments(_ value: CSPCommentModel)

    @objc(removeCommentsObject:)
    @NSManaged public func removeFromComments(_ value: CSPCommentModel)

    @objc(addComments:)
    @NSManaged public func addToComments(_ values: NSOrderedSet)

    @objc(removeComments:)
    @NSManaged public func removeFromComments(_ values: NSOrderedSet)

}
// MARK: Generated accessors for lists
extension CSPCircleModel {

    @objc(insertObject:inListsAtIndex:)
    @NSManaged public func insertIntoLists(_ value: CSPListModel, at idx: Int)

    @objc(removeObjectFromListsAtIndex:)
    @NSManaged public func removeFromLists(at idx: Int)

    @objc(insertLists:atIndexes:)
    @NSManaged public func insertIntoLists(_ values: [CSPListModel], at indexes: NSIndexSet)

    @objc(removeListsAtIndexes:)
    @NSManaged public func removeFromLists(at indexes: NSIndexSet)

    @objc(replaceObjectInListsAtIndex:withObject:)
    @NSManaged public func replaceLists(at idx: Int, with value: CSPListModel)

    @objc(replaceListsAtIndexes:withLists:)
    @NSManaged public func replaceLists(at indexes: NSIndexSet, with values: [CSPListModel])

    @objc(addListsObject:)
    @NSManaged public func addToLists(_ value: CSPListModel)

    @objc(removeListsObject:)
    @NSManaged public func removeFromLists(_ value: CSPListModel)

    @objc(addLists:)
    @NSManaged public func addToLists(_ values: NSOrderedSet)

    @objc(removeLists:)
    @NSManaged public func removeFromLists(_ values: NSOrderedSet)

}

// MARK: Generated accessors for members
extension CSPCircleModel {

    @objc(insertObject:inMembersAtIndex:)
    @NSManaged public func insertIntoMembers(_ value: CSPUserModel, at idx: Int)

    @objc(removeObjectFromMembersAtIndex:)
    @NSManaged public func removeFromMembers(at idx: Int)

    @objc(insertMembers:atIndexes:)
    @NSManaged public func insertIntoMembers(_ values: [CSPUserModel], at indexes: NSIndexSet)

    @objc(removeMembersAtIndexes:)
    @NSManaged public func removeFromMembers(at indexes: NSIndexSet)

    @objc(replaceObjectInMembersAtIndex:withObject:)
    @NSManaged public func replaceMembers(at idx: Int, with value: CSPUserModel)

    @objc(replaceMembersAtIndexes:withMembers:)
    @NSManaged public func replaceMembers(at indexes: NSIndexSet, with values: [CSPUserModel])

    @objc(addMembersObject:)
    @NSManaged public func addToMembers(_ value: CSPUserModel)

    @objc(removeMembersObject:)
    @NSManaged public func removeFromMembers(_ value: CSPUserModel)

    @objc(addMembers:)
    @NSManaged public func addToMembers(_ values: NSOrderedSet)

    @objc(removeMembers:)
    @NSManaged public func removeFromMembers(_ values: NSOrderedSet)

}

// MARK: Generated accessors for invitedMembers
extension CSPCircleModel {

    @objc(insertObject:inInvitedMembersAtIndex:)
    @NSManaged public func insertIntoInvitedMembers(_ value: CSPUserModel, at idx: Int)

    @objc(removeObjectFromInvitedMembersAtIndex:)
    @NSManaged public func removeFromInvitedMembers(at idx: Int)

    @objc(insertInvitedMembers:atIndexes:)
    @NSManaged public func insertIntoInvitedMembers(_ values: [CSPUserModel], at indexes: NSIndexSet)

    @objc(removeInvitedMembersAtIndexes:)
    @NSManaged public func removeFromInvitedMembers(at indexes: NSIndexSet)

    @objc(replaceObjectInInvitedMembersAtIndex:withObject:)
    @NSManaged public func replaceInvitedMembers(at idx: Int, with value: CSPUserModel)

    @objc(replaceInvitedMembersAtIndexes:withInvitedMembers:)
    @NSManaged public func replaceInvitedMembers(at indexes: NSIndexSet, with values: [CSPUserModel])

    @objc(addInvitedMembersObject:)
    @NSManaged public func addToInvitedMembers(_ value: CSPUserModel)

    @objc(removeInvitedMembersObject:)
    @NSManaged public func removeFromInvitedMembers(_ value: CSPUserModel)

    @objc(addInvitedMembers:)
    @NSManaged public func addToInvitedMembers(_ values: NSOrderedSet)

    @objc(removeInvitedMembers:)
    @NSManaged public func removeFromInvitedMembers(_ values: NSOrderedSet)

}

// MARK: Generated accessors for posts
extension CSPCircleModel {

    @objc(insertObject:inPostsAtIndex:)
    @NSManaged public func insertIntoPosts(_ value: CSPPostModel, at idx: Int)

    @objc(removeObjectFromPostsAtIndex:)
    @NSManaged public func removeFromPosts(at idx: Int)

    @objc(insertPosts:atIndexes:)
    @NSManaged public func insertIntoPosts(_ values: [CSPPostModel], at indexes: NSIndexSet)

    @objc(removePostsAtIndexes:)
    @NSManaged public func removeFromPosts(at indexes: NSIndexSet)

    @objc(replaceObjectInPostsAtIndex:withObject:)
    @NSManaged public func replacePosts(at idx: Int, with value: CSPPostModel)

    @objc(replacePostsAtIndexes:withPosts:)
    @NSManaged public func replacePosts(at indexes: NSIndexSet, with values: [CSPPostModel])

    @objc(addPostsObject:)
    @NSManaged public func addToPosts(_ value: CSPPostModel)

    @objc(removePostsObject:)
    @NSManaged public func removeFromPosts(_ value: CSPPostModel)

    @objc(addPosts:)
    @NSManaged public func addToPosts(_ values: NSOrderedSet)

    @objc(removePosts:)
    @NSManaged public func removeFromPosts(_ values: NSOrderedSet)

}


// MARK: Generated accessors for tasks
extension CSPCircleModel {

    @objc(insertObject:inTasksAtIndex:)
    @NSManaged public func insertIntoTasks(_ value: CSPTaskModel, at idx: Int)

    @objc(removeObjectFromTasksAtIndex:)
    @NSManaged public func removeFromTasks(at idx: Int)

    @objc(insertTasks:atIndexes:)
    @NSManaged public func insertIntoTasks(_ values: [CSPTaskModel], at indexes: NSIndexSet)

    @objc(removeTasksAtIndexes:)
    @NSManaged public func removeFromTasks(at indexes: NSIndexSet)

    @objc(replaceObjectInTasksAtIndex:withObject:)
    @NSManaged public func replaceTasks(at idx: Int, with value: CSPTaskModel)

    @objc(replaceTasksAtIndexes:withTasks:)
    @NSManaged public func replaceTasks(at indexes: NSIndexSet, with values: [CSPTaskModel])

    @objc(addTasksObject:)
    @NSManaged public func addToTasks(_ value: CSPTaskModel)

    @objc(removeTasksObject:)
    @NSManaged public func removeFromTasks(_ value: CSPTaskModel)

    @objc(addTasks:)
    @NSManaged public func addToTasks(_ values: NSOrderedSet)

    @objc(removeTasks:)
    @NSManaged public func removeFromTasks(_ values: NSOrderedSet)

}

// MARK: Generated accessors for events
extension CSPCircleModel {

    @objc(insertObject:inEventsAtIndex:)
    @NSManaged public func insertIntoEvents(_ value: CSPEventModel, at idx: Int)

    @objc(removeObjectFromEventsAtIndex:)
    @NSManaged public func removeFromEvents(at idx: Int)

    @objc(insertEvents:atIndexes:)
    @NSManaged public func insertIntoEvents(_ values: [CSPEventModel], at indexes: NSIndexSet)

    @objc(removeEventsAtIndexes:)
    @NSManaged public func removeFromEvents(at indexes: NSIndexSet)

    @objc(replaceObjectInEventsAtIndex:withObject:)
    @NSManaged public func replaceEvents(at idx: Int, with value: CSPEventModel)

    @objc(replaceEventsAtIndexes:withTasks:)
    @NSManaged public func replaceEvents(at indexes: NSIndexSet, with values: [CSPEventModel])

    @objc(addEventsObject:)
    @NSManaged public func addToEvents(_ value: CSPEventModel)

    @objc(removeEventsObject:)
    @NSManaged public func removeFromEvents(_ value: CSPEventModel)

    @objc(addEvents:)
    @NSManaged public func addToEvents(_ values: NSOrderedSet)

    @objc(removeEvents:)
    @NSManaged public func removeFromEvents(_ values: NSOrderedSet)

}

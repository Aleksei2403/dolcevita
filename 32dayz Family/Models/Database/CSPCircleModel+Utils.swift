//
//  CSPCircleModel+Utils.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/7/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

extension CSPCircleModel {
    
    var tasksArray: [CSPTaskModel] {
        get {
            return tasks?.array as? [CSPTaskModel] ?? []
        }
    }
    
    var eventsArray: [CSPEventModel] {
        get {
            return events?.array as? [CSPEventModel] ?? []
        }
    }
    
    var postsArray: [CSPPostModel] {
        get {
            return posts?.array as? [CSPPostModel] ?? []
        }
    }
    
    var listsArray: [CSPListModel] {
        get {
            return (lists?.array as? [CSPListModel] ?? []).sorted { $0.createdAt!.compare($1.createdAt! as Date) == .orderedAscending }
        }
    }
    
    var membersArray: [CSPUserModel] {
        get {
            return members?.array as? [CSPUserModel] ?? []
        }
    }
    
    var invitedMembersArray: [CSPUserModel] {
        get {
            return invitedMembers?.array as? [CSPUserModel] ?? []
        }
    }
    
}

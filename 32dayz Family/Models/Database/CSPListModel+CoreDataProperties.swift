//
//  CSPListModel+CoreDataProperties.swift
//  32dayz Family
//
//  Created by Pavel Maksimov on 9/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import CoreData


extension CSPListModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CSPListModel> {
        return NSFetchRequest<CSPListModel>(entityName: "CSPListModel")
    }

    @NSManaged public var circleUID: String?
    @NSManaged public var color: String?
    @NSManaged public var createdAt: NSDate?
    @NSManaged public var name: String?
    @NSManaged public var uid: String?
    @NSManaged public var tasksCount: Int16
    @NSManaged public var circle: CSPCircleModel?
    

}

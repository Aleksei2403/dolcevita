//
//  CSPPostModel+Utils.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/9/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

enum CSPPostType: Int16 {
    case Post = 0
    case CompletedTask = 1
}

extension CSPPostModel {
    
    var postType: CSPPostType {
        get {
            return CSPPostType(rawValue: type) ?? .Post
        }
    }
    
}

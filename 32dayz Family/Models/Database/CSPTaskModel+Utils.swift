//
//  CSPTaskModel+Utils.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/7/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

extension CSPTaskModel {
    
    var membersArray: [CSPUserModel] {
        get {
            return members?.array as? [CSPUserModel] ?? []
        }
    }
    
}

//
//  CSPAccountInfoModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/21/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

enum CSPAccountInfoCellType {
    
    case FirstName
    case LastName
    case Phone
    
}

class CSPAccountInfoModel {
    
    private let arrayCellTypes = [
        CSPAccountInfoCellType.FirstName,
        CSPAccountInfoCellType.LastName,
        CSPAccountInfoCellType.Phone
    ]
    
    var cellsCount: Int {
        get {
            return arrayCellTypes.count
        }
    }
    
    let cellHeight: CGFloat = 60
    
    func getCellType(atIndexPath ip: IndexPath) -> CSPAccountInfoCellType {
        return arrayCellTypes[ip.row]
    }
    
}

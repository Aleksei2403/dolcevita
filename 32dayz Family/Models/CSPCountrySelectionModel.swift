//
//  CSPCountrySelectionModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPCountrySelectionModel {
    
    var countries: [CSPCountry] = []
    var selectedCountry: CSPCountry?
    
    lazy var letters: [Character] = {
        return Array(Set<Character>(self.countries.map { $0.name![$0.name!.startIndex] })).sorted()
    }()
    
    lazy var dataSource: [[CSPCountry]] = {
        return self.letters.map { letter in
            return self.countries.filter { $0.name![$0.name!.startIndex] == letter }
        }
    }()
    
    func letter(forSection section: Int) -> String {
        return String(letters[section])
    }
    
    func country(atIndexPath ip: IndexPath) -> CSPCountry {
        return dataSource[ip.section][ip.row]
    }
    
    func indexPathForSelectedCountry() -> IndexPath? {
        if selectedCountry != nil {
            for i in 0..<dataSource.count {
                let arrayCountries = dataSource[i]
                for j in 0..<arrayCountries.count {
                    let country = arrayCountries[j]
                    if country.name == selectedCountry!.name {
                        return IndexPath(row: j, section: i)
                    }
                }
            }
        }
        return nil
    }
    
}

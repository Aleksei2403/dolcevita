//
//  CSPDevice.swift
//  FamilyOrganizer
//
//  Created by User on 03.11.16.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import UIKit

public enum CSPDevice {
    case iPhone5
    case iPhone5c
    case iPhone5s
    case iPhone6
    case iPhone6Plus
    case iPhone6s
    case iPhone6sPlus
    case iPhone7
    case iPhone7Plus
    case iPhoneSE
    indirect case simulator(CSPDevice)
    case unknown(String)
    
    public static var identifier: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let mirror = Mirror(reflecting: systemInfo.machine)
        
        let identifier = mirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    public init() {
        self = CSPDevice.mapToDevice(identifier: CSPDevice.identifier)
    }
    
    public static func mapToDevice(identifier: String) -> CSPDevice {
        switch identifier {
            
        case "iPhone5,1", "iPhone5,2":                  return .iPhone5
        case "iPhone5,3", "iPhone5,4":                  return .iPhone5c
        case "iPhone6,1", "iPhone6,2":                  return .iPhone5s
        case "iPhone7,2":                               return .iPhone6
        case "iPhone7,1":                               return .iPhone6Plus
        case "iPhone8,1":                               return .iPhone6s
        case "iPhone8,2":                               return .iPhone6sPlus
        case "iPhone9,1", "iPhone9,3":                  return .iPhone7
        case "iPhone9,2", "iPhone9,4":                  return .iPhone7Plus
        case "iPhone8,4":                               return .iPhoneSE
        case "i386", "x86_64":                          return .simulator(mapToDevice(identifier: String(validatingUTF8: getenv("SIMULATOR_MODEL_IDENTIFIER"))!))
        default:                                        return .unknown(identifier)
        }
    }
    
    public var diagonal: Float {
        switch self {
        case .iPhone5:                      return 4
        case .iPhone5c:                     return 4
        case .iPhone5s:                     return 4
        case .iPhone6:                      return 4.7
        case .iPhone6Plus:                  return 5.5
        case .iPhone6s:                     return 4.7
        case .iPhone6sPlus:                 return 5.5
        case .iPhone7:                      return 4.7
        case .iPhone7Plus:                  return 5.5
        case .iPhoneSE:                     return 4
        case .simulator(let model):         return model.diagonal
        case .unknown( _):                  return -1
        }
    }
    
    public var screenSizeInPoints: CGSize {
        switch self {
        case .iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE:
            return CGSize(width: 320, height: 568)
        case .iPhone6, .iPhone6s, .iPhone7:
            return CGSize(width: 375, height: 667)
        case .iPhone6Plus, .iPhone6sPlus, .iPhone7Plus:
            return CGSize(width: 414, height: 736)
        case .simulator(let model):
            return model.screenSizeInPoints
        case .unknown( _):
            return CGSize.zero
        }
    }
}

extension CSPDevice: CustomStringConvertible {
    
    public var description: String {
        switch self {
        case .iPhone5:                      return "iPhone 5"
        case .iPhone5c:                     return "iPhone 5c"
        case .iPhone5s:                     return "iPhone 5s"
        case .iPhone6:                      return "iPhone 6"
        case .iPhone6Plus:                  return "iPhone 6 Plus"
        case .iPhone6s:                     return "iPhone 6s"
        case .iPhone6sPlus:                 return "iPhone 6s Plus"
        case .iPhone7:                      return "iPhone 7"
        case .iPhone7Plus:                  return "iPhone 7 Plus"
        case .iPhoneSE:                     return "iPhone SE"
        case .simulator(let model):         return "Simulator (\(model))"
        case .unknown(let identifier):      return identifier
        }
    }
}

extension CSPDevice: Equatable {}

public func == (lhs: CSPDevice, rhs: CSPDevice) -> Bool {
    return lhs.description == rhs.description
}

//
//  CSPMenuModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

enum MenuItemType {
    
    case Profile
    case Journal
    case Events
    case Today
    case Next7Days
    case Someday
    case Tasks
    case Inbox
    case CustomList
    case Overdue
    
}

struct CSPMenuItem {
    
    var title: String
    var counter: Int
    var type: MenuItemType
    
    init(_ title: String, _ counter: Int = 0, _ type: MenuItemType) {
        self.title = title
        self.counter = counter
        self.type = type
    }
    
}

struct CSPMenuSmallItem {
    
    var title: String
    var counter: Int
    var color: UIColor?
    var listUID: String?
    var type: MenuItemType
    
    init(_ title: String, _ counter: Int, _ color: String? = nil, _ listUID: String? = nil, _ type: MenuItemType) {
        self.title = title
        self.counter = counter
        if color != nil {
            self.color = UIColor.getUIColor(withHex: color!)
        }
        self.listUID = listUID
        self.type = type
    }
    
}

class CSPMenuModel {
    
    var dataSource: [Any] {
        get {
            return [
                CSPMenuItem("", 0, .Profile),
                CSPMenuItem("Journal", 0, .Journal),
                CSPMenuItem("Events",0, .Events),
                CSPMenuItem("Today", getTodayTasks().count, .Today),
                CSPMenuItem("Next 7 Days", getNext7DaysTasks().count, .Next7Days),
                CSPMenuItem("Someday", getSomedayTasks().count, .Someday),
                CSPMenuItem("All Tasks", CSPDBManager.shared.getTasks().count, .Tasks)
            ] + lists()
        }
    }
    
    private func lists() -> [CSPMenuSmallItem] {
        return CSPDBManager.shared.getLists().map { CSPMenuSmallItem($0.name ?? "", getTasks(forList: $0).count, $0.color, $0.uid, $0.name == "Inbox" ? .Inbox : .CustomList) }
    }
    
    var rowsCount: Int {
        return dataSource.count
    }
    
    func menuItem(atIndexPath ip: IndexPath) -> Any {
        return dataSource[ip.row]
    }
    
    var circles: [CSPCircleModel] {
        get {
            return CSPDBManager.shared.getCircles()
        }
    }
    
    func getOverdueTasks() -> [CSPTaskModel] {
        return CSPDBManager.shared.getOverdueTasks()
    }
    
    func getTodayTasks() -> [CSPTaskModel] {
        return CSPDBManager.shared.getTodayTasks()
    }
    
    func getNext7DaysTasks() -> [CSPTaskModel] {
        return CSPDBManager.shared.getNext7DaysTasks()
    }
    
    func getSomedayTasks() -> [CSPTaskModel] {
        return CSPDBManager.shared.getSomedayTasks()
    }
    
    func getList(withListUID uid: String?) -> CSPListModel? {
        return uid == nil ? nil : CSPDBManager.shared.getList(withUID: uid!)
    }
    
    func getAllTasks() -> [CSPTaskModel]{
        return CSPDBManager.shared.getTasks()
    }
    func getAllEvents() -> [CSPEventModel] {
        return CSPDBManager.shared.getEvents()
    }
    
    func getTasks(forList list: CSPListModel) -> [CSPTaskModel] {
        return CSPDBManager.shared.getTasks(forList: list)
    }
    
}

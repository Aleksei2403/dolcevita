//
//  CSPExtensions.swift
//  FamilyOrganizer
//
//  Created by User on 20.10.16.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var isResizingDisable: Bool {
        get {
            return (objc_getAssociatedObject(self, &isResizableAssociationKey) as! Bool?) ?? false
        }
        set(newValue) {
            objc_setAssociatedObject(self, &isResizableAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension UILabel {
    func setKerning(_ kerning: CGFloat) {
        if ((self.attributedText?.length) != nil) {
            let attribString = NSMutableAttributedString(attributedString: self.attributedText!)
            attribString.addAttributes([NSAttributedString.Key.kern:kerning], range:NSMakeRange(0, self.attributedText!.length))
            self.attributedText = attribString
        }
    }
}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    func setKerning(_ kerning: CGFloat) {
        if self.titleLabel?.text != nil {
            let attributedText =  NSAttributedString(string: self.titleLabel!.text!, attributes: [NSAttributedString.Key.kern:kerning, NSAttributedString.Key.font:self.titleLabel!.font, NSAttributedString.Key.foregroundColor:self.titleLabel!.textColor])
            self.setAttributedTitle(attributedText, for: UIControl.State.normal)
        }
    }
}

extension UIViewController {
    
    func addBackgroundImage(withName name: String) {
        let image = UIImage(named: name)
        addBackgroundImage(withImage: image!)
    }
    
    func addBackgroundImage(withImage image: UIImage) {
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageViewBackground.image = image
        imageViewBackground.contentMode = UIView.ContentMode.scaleToFill
        
        view.addSubview(imageViewBackground)
        view.sendSubviewToBack(imageViewBackground)
    }
    
}



extension UIColor {
    
    convenience init(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat = 1) {
        self.init(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
    }
    
    convenience init(_ rgb: Int, _ alpha: CGFloat = 1) {
        let r = CGFloat((rgb & 0xff0000) >> 16) / 255
        let g = CGFloat((rgb & 0x00ff00) >>  8) / 255
        let b = CGFloat((rgb & 0x0000ff)      ) / 255
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }

    static func getUIColor(withHex hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

extension UIImage {
    
    func getfitInSquareImage() -> UIImage {
        let sqareWidth = max(self.size.width, self.size.height)
        let newSize = CGSize(width: sqareWidth, height: sqareWidth)
        let newOrigin = CGPoint(x: (newSize.width - self.size.width) / 2, y: (newSize.height - self.size.height) / 2)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 1.0)
        self.draw(in: CGRect(origin: newOrigin, size: self.size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func compressed() -> UIImage {
        if size.width < 1000 || size.height < 1000 { return self }
        let destinationSize = CGSize(width: size.width / 2, height: size.height / 2)
        UIGraphicsBeginImageContext(destinationSize)
        self.draw(in: CGRect(x: 0, y: 0, width: destinationSize.width, height: destinationSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

extension String {
    
    func getTwoLettersAbbreviation() -> String {
        let fullNameArr = split{$0 == " "}.map(String.init)
        let firstWord: String? = fullNameArr.count > 0 ? fullNameArr[0] : nil
        let secondWord: String? = fullNameArr.count > 1 ? fullNameArr[1] : nil
        return "\((firstWord != nil && firstWord!.count > 0) ? String(firstWord!.first!) : "")\((secondWord != nil && secondWord!.count > 0) ? String(secondWord!.first!) : "")"
    }
    
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind: String, linkURL: String, font: UIFont) {
        let foundRange = mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            addAttribute(NSAttributedString.Key.link, value: linkURL, range: foundRange)
            addAttribute(NSAttributedString.Key.font, value: font, range: foundRange)
        }
    }
    
}

private var isResizableAssociationKey: UInt8 = 0

extension NSLayoutConstraint {
    
    @IBInspectable var isResizingDisable: Bool {
        get {
            return (objc_getAssociatedObject(self, &isResizableAssociationKey) as! Bool?) ?? false
        }
        set(newValue) {
            objc_setAssociatedObject(self, &isResizableAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
}

extension NSDate {
    
    class func today() -> NSDate {
        return Date() as NSDate
    }
    
    class func tomorrow() -> NSDate {
        return NSCalendar.current.date(byAdding: .day, value: 1, to: Date())! as NSDate
    }
    
    class func nextDayAfterTomorrow() -> NSDate {
        return NSCalendar.current.date(byAdding: .day, value: 1, to: NSDate.tomorrow() as Date)! as NSDate
    }
    
    class func nextWeekMonday() -> NSDate {
        return NSCalendar.current.date(bySetting: .weekday, value: 2, of: Date())! as NSDate
    }
    
    class func someday() -> NSDate {
        return NSCalendar.current.date(byAdding: .day, value: 10, to: Date())! as NSDate
    }
    
    var isOverdue: Bool {
        get {
            let today = NSCalendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            return (self as Date).compare(today) == .orderedAscending
        }
    }
    
    var isToday: Bool {
        get {
            return NSCalendar.current.isDateInToday(self as Date)
        }
    }
    
    var isYesterday: Bool {
        get {
            return NSCalendar.current.isDateInYesterday(self as Date)
        }
    }
    
    var isTomorrow: Bool {
        get {
            return NSCalendar.current.isDateInTomorrow(self as Date)
        }
    }
    
    var isNext7Days: Bool {
        get {
            let calendar = NSCalendar.current
            let today = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            let startDate = calendar.date(byAdding: .day, value: 1, to: today)!
            let endDate = calendar.date(byAdding: .day, value: 7, to: startDate)!
            return startDate.compare(self as Date).rawValue * self.compare(endDate).rawValue >= 0
        }
    }
    
    var isSomeday: Bool { //More than 7 days
        get {
            let calendar = NSCalendar.current
            let today = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            let startDate = calendar.date(byAdding: .day, value: 7, to: today)!
            let result = self.compare(startDate).rawValue
            return result >= 0
        }
    }
    
    var stringDescription: String {
        get {
            if self.isToday {
                return "Today"
            } else if self.isYesterday {
                return "Yesterday"
            } else if self.isTomorrow {
                return "Tomorrow"
            } else {
                let df = DateFormatter()
                df.dateStyle = .medium
                df.timeStyle = .none
                return df.string(from: self as Date)
            }
        }
    }
    
    var truncateTime: NSDate {
        get {
            let calendar = NSCalendar.current
            let components = calendar.dateComponents([.year, .month, .day], from: self as Date)
            return calendar.date(from: components)! as NSDate
        }
    }
    
}

extension Date {
    
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    
    func days(from date: Date) -> Int {
        let calendar = Calendar.current
        return calendar.dateComponents([.day], from: calendar.startOfDay(for: date), to: calendar.startOfDay(for: self)).day ?? 0
    }
    
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
}

extension NSDate {
    
    static let SecondsInYear: Double = 12 * SecondsInMonth
    static let SecondsInMonth: Double = 30 * SecondsInDay
    static let SecondsInWeek: Double = 7 * SecondsInDay
    static let SecondsInDay: Double = 24 * SecondsInHour
    static let SecondsInHour: Double = 60 * SecondsInMinute
    static let SecondsInMinute: Double = 60
    
    func stringRepresentation() -> String {
        let currentTI = Date().timeIntervalSince1970
        let difference = currentTI - self.timeIntervalSince1970
        
        let years = Int(difference / NSDate.SecondsInYear)
        if years > 0 {
            return "\(years) \(years > 1 ? "years ago" : "year ago")"
        }
        
        let months = Int(difference / NSDate.SecondsInMonth)
        if months > 0 {
            return "\(months) \(months > 1 ? "months ago" : "month ago")"
        }
        
        let weeks = Int(difference / NSDate.SecondsInWeek)
        if weeks > 0 {
            return "\(weeks) \(weeks > 1 ? "weeks ago" : "week ago")"
        }
        
        let days = Int(difference / NSDate.SecondsInDay)
        if days > 0 {
            if days == 1 {
                return "yesterday"
            } else {
                return "\(days) \("days ago")"
            }
        }
        
        let hours = Int(difference / NSDate.SecondsInHour)
        if hours > 0 {
            return "\(hours) \(hours > 1 ? "hours ago" : "hour ago")"
        }
        
        let minutes = Int(difference / NSDate.SecondsInMinute)
        if minutes > 0 {
            return "\(minutes) \(minutes > 1 ? "minutes ago" : "minute ago")"
        }
        
        return "just now"
    }
    
}

extension UIApplication {
    
    class func versionBuild() -> String {
        return "Version \(buildNumber())"
    }
    
    class func buildNumber() -> String {
        return "\(appVersion()) (\(appBuild()))"
    }
    
    class func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    class func appBuild() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
}

extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

//
//  CSPSelectListModel.swift
//  32dayz Family
//
//  Created by Pavel Maksimov on 9/12/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPSelectListModel {
    
    var lists: [CSPListModel] {
        get {
            return CSPDBManager.shared.getLists()
        }
    }
    
    var selectedList: CSPListModel?
    
}

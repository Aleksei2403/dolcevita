//
//  CSPListColorModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/3/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPListColorModel {
    
    var colors: [CSPColorModel] = []
    var currentColor: CSPColorModel?
    
    var itemsCount: Int {
        get {
            return colors.count
        }
    }
    
    var cellSize: CGSize {
        get {
            let a = UIScreen.main.bounds.width - 10
            return CGSize(width: a / 4, height: a / 4)
        }
    }
    
    func getColor(atIndexPath ip: IndexPath) -> CSPColorModel {
        return colors[ip.item]
    }
    
}

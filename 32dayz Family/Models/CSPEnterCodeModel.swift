//
//  CSPEnterCodeModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/18/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPEnterCodeModel {
    
    var phone = ""
    
    var codeString = ""
    
    var code: String {
        get {
            return codeString
        }
    }
    
    func confirmCode(_ completion: @escaping (Error?) -> ()) {
        CSPAPIManager.shared.confirmCode(code) { error in
            completion(error)
        }
    }
    
    func getUser(_ completion: @escaping (CSPUserModel?) -> ()) {
        CSPAPIManager.shared.getUser { user in
            completion(user)
        }
    }
    
    func getUserWithPhone(_ completion: @escaping (CSPUserModel?) -> ()) {
        CSPAPIManager.shared.getUser(phoneNumber: phone) { (user) in
            completion(user)
        }
    }
    
    func getCircles(_ completion: @escaping ([CSPCircleModel]) -> ()) {
        CSPAPIManager.shared.getCircles { circles in
            completion(circles)
        }
    }
    
    func updatePhone(_ completion: @escaping () -> ()) {
        CSPAPIManager.shared.updatePhone(phone) {
            completion()
        }
    }
    
}

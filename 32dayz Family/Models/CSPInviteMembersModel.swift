//
//  CSPInviteMembersModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import Contacts

class CSPInviteMembersModel {
    
    var circleUID: String?
    var invitedContacts: [CNContact] = []
    var contacts: [CNContact] = []
    var searchResults: [CNContact] = []
    var letters: [Character] = []
    var dataSource: [[CNContact]] = []
    
    var existUsers: [CSPUserModel] = []
    
   
    
    var sectionsCount: Int {
        get {
            return letters.count
        }
    }
    
    func rowsCount(forSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    func letter(forSection section: Int) -> String {
        return String(letters[section])
    }
    
    func contact(atIndexPath ip: IndexPath) -> CNContact {
        return dataSource[ip.section][ip.row]
    }
    
    func loadContacts() {
        contacts = []
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey, CNContactImageDataKey] as! [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: keys)
        do {
            try CNContactStore().enumerateContacts(with: request) { [weak self] contact, stop in
                if CNContactFormatter.string(from: contact, style: .fullName) != nil {
                    self?.contacts.append(contact)
                }
            }
        } catch _ { }
        searchResults = contacts
        existUsers = CSPDBManager.shared.getCircles().first { $0.uid == circleUID }?.membersArray ?? []
        reloadDataSource()
    }
    
    func search(forString string: String) {
        searchResults = contacts.filter { $0.givenName.contains(string) || $0.familyName.contains(string) }
        reloadDataSource()
    }
    
    func resetSearch() {
        searchResults = contacts
        reloadDataSource()
    }
    
    private func reloadDataSource() {
        letters = Array(Set<Character>(self.searchResults.map { contact in            
            if contact.familyName.count > 0 {
                return contact.familyName[contact.familyName.startIndex]
            } else {
                let name = CNContactFormatter.string(from: contact, style: .fullName)!
                return name[name.startIndex]
            }
        })).sorted()
        dataSource = letters.map { letter in
            return self.searchResults.filter { contact in
                if contact.familyName.count > 0 {
                    return contact.familyName[contact.familyName.startIndex] == letter
                } else {
                    let name = CNContactFormatter.string(from: contact, style: .fullName)!
                    return name[name.startIndex] == letter
                }
            }
        }
    }
    
    func getCircles(_ completion: @escaping ([CSPCircleModel]) -> ()) {
        CSPAPIManager.shared.getCircles { circles in
            completion(circles)
        }
    }
    
    func getInvites(_ completion: @escaping ([CSPInviteModel]) -> ()) {
        if circleUID != nil {
            CSPAPIManager.shared.getInvites(forCircle: circleUID!) { invites in
                completion(invites)
            }
        } else {
            completion([])
        }
    }
    
    func isUserExistAndAlreadyInvited(phones: [String]) -> Bool {
        var isExist = false
        phones.forEach { phone in
            if existUsers.first(where: { $0.phone == phone }) != nil {
                isExist = true
            }
        }
        return isExist
    }
    
    func getUser(_ phones: [String], completion: @escaping (CSPUserModel?) -> ()) {
        
        var index = 0
        func getUser() {
            CSPAPIManager.shared.getUser(phoneNumber: phones[index]) { model in
                index += 1
                if let model = model {
                    completion(model)
                } else if index < phones.count {
                    getUser()
                } else {
                    completion(nil)
                }
            }
        }
        getUser()
    }
    
    func invite(phone: String, toCircle uid: String, _ completion: @escaping (CSPUserModel?, Error?) -> ()) {
        CSPAPIManager.shared.invite(phone: phone, toCircle: uid) { user, error in
//            self.existUsers.append(user!)
            completion(user, error)
        }
    }
    
    func invite(user: CSPUserModel, toCircle uid: String, _ completion: @escaping (CSPUserModel?, Error?) -> ()) {
        existUsers.append(user)
        CSPAPIManager.shared.invite(user: user, toCircle: uid) { user, error in
            completion(user, error)
        }
    }
}

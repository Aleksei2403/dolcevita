//
//  CSPEnterPhoneModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import CoreTelephony

class CSPCountry {
    
    var id: String
    var code: String
    var name: String?
    
    init(id: String, code: String) {
        self.id = id
        self.code = code
    }
    
}

class CSPEnterPhoneModel {
    
    lazy var countries: [CSPCountry] = []
    lazy var selectedCountry: CSPCountry = {
        return self.countries.first!
    }()
    var phoneString: String = ""
    var phone: String {
        get {
            return "+" + String(selectedCountry.code) + phoneString
        }
    }
    
    init() {
        initCountries()
    }
    
    private func initCountries() {
        if let path = Bundle.main.url(forResource: "country_id_code", withExtension: "json") {
            do {
                let data = try Data(contentsOf: path)
                let parsedData = try JSONSerialization.jsonObject(with: data, options: []) as! [String: String]
                countries = parsedData.keys.map { CSPCountry(id: $0, code: parsedData[$0]!) }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        if let path = Bundle.main.url(forResource: "country_name_code", withExtension: "json") {
            do {
                let data = try Data(contentsOf: path)
                let parsedData = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                for element in parsedData {
                    let stringCode = String(element["code"] as! Int)
                    let name = element["eng"] as! String
                    for country in countries {
                        if country.code == stringCode {
                            country.name = name
                        }
                    }
                }
                countries = countries.filter { $0.name != nil }.sorted { $0.name! < $1.name! }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
    }
    
    public func fetchCarrier() {
        if let countryID = CTTelephonyNetworkInfo().subscriberCellularProvider?.isoCountryCode {
            if let country = countries.filter( { countryID.caseInsensitiveCompare($0.id) == .orderedSame }).first {
                selectedCountry = country
            }
        }
    }
    
    public func verifyPhone(_ completion: @escaping (Error?) -> ()) {
        CSPAPIManager.shared.verifyPhone(phone) { error in
            completion(error)
        }
    }
    
}

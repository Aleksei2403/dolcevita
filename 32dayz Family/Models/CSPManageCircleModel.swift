//
//  CSPManageCircleModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/16/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit


class CSPManageCircleModel {
    
    var circle: CSPCircleModel! {
        didSet {
            circleName = circle.name
            circleMembers = circle.membersArray
            circleInvitedmembers = circle.invitedMembersArray
            for i in circleMembers {
                if i.notVerification == true {
                    countInvitedMembers.append(i)
                }
            }
            
        }
    }
    
    
    let sectionsCount: Int = 4
    
    func rowsCount(inSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            if circle == CSPSessionManager.currentCircle {
                if circle.author == CSPSessionManager.currentUser {
                    return 2
                } else {
                    return 2
                }
            } else {
                if circle.author == CSPSessionManager.currentUser {
                    return 2
                } else {
                    return 2
                }
            }
            
       //return circle.author == CSPSessionManager.currentUser ? 2 : 1
        case 3 :
            return circle.author == CSPSessionManager.currentUser ? 2 : 1
        default:
            return 0
        }
    }
    
    let sectionHeight: CGFloat = 30
    
    private let sectionTitles = ["Circle Name", "Manage Members","invited Members", "Other" ]
    
    func sectionTitle(forSection section: Int) -> String {
        return sectionTitles[section]
    }
    var countInvitedMembers:[CSPUserModel] = []
    
    func heightForRow(atIndexPath ip: IndexPath) -> CGFloat {
        switch ip.section {
        case 0:
            return 60
        case 1:
            return 70
        case 2:
            switch ip.row {
            case 0:
                return CGFloat(68 * circleMembers.filter({ return $0.notVerification == true}).count)
            case 1:
                return 60
            default:
                return .leastNormalMagnitude
            }
        case 3:
            switch ip.row {
            case 0:
                return circle == CSPSessionManager.currentCircle ? calculateBottomCellHeight() : 60
            //            case 1:
            //                return calculateBottomCellHeight()
            default:
                return .leastNormalMagnitude
            }
            
        default:
            return .leastNormalMagnitude
        }
    }
    
    func cellIdentifier(atIndexPath ip: IndexPath) -> String {
        switch ip.row {
        //        case 0:
        //            return R.reuseIdentifier.cspSubscriptionInfoCell_id.identifier
        case 0:
            return circle == CSPSessionManager.currentCircle ? R.reuseIdentifier.cspDeleteCircleCell_id.identifier : R.reuseIdentifier.cspSwitchToThisCircle_id.identifier
        //        case 1:
        //            return R.reuseIdentifier.cspCircleInviteCell_id.identifier
        case 1:
            return R.reuseIdentifier.cspDeleteCircleCell_id.identifier
        default:
            return ""
        }
    }
    
    func calculateBottomCellHeight() -> CGFloat {
        //        let sectionsHeight = CGFloat(sectionsCount) * sectionHeight
        //        var cellsHeight: CGFloat = 60 + 70 + 50
        //        if circle != CSPSessionManager.currentCircle {
        //            cellsHeight += 60
        //        }
        //        return UIScreen.main.bounds.height - 64 - sectionsHeight - cellsHeight
        return 60
    }
    
    var circleName: String?
    
    var circleNameChanged: Bool {
        get {
            return circleName != circle.name
        }
    }
    
    var circleMembers: [CSPUserModel] = []
    
    var circleInvitedmembers : [CSPUserModel] = []
    
    var circleMembersChanged: Bool {
        get {
            return circleMembers != circle.membersArray
        }
    }
    
    var circleIvitedMembersChanged : Bool {
        get {
            return circleInvitedmembers != circle.invitedMembersArray
        }
    }
    
    func updateCircle(completion: @escaping () -> ()) {
        circle.name = circleName
        //        circle.membersArray.forEach { circle.removeFromMembers($0) }
        //        circleMembers.forEach { circle.addToMembers($0) }
        CSPAPIManager.shared.updateCircle(circle) { error in
            completion()
        }
    }
    
    func deleteCircle(_ completion: @escaping (Error?) -> ()) {
        CSPAPIManager.shared.deleteCircle(circle) { error in
            completion(error)
        }
    }
    
    func getCircles() -> [CSPCircleModel] {
        return CSPDBManager.shared.getCircles()
    }
    
}

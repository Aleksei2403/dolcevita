//
//  CSPNewListModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/3/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPNewListModel {
    
    var listName: String?
    var currentColor: CSPColorModel?
    
    var listColors: [CSPColorModel] = []
    
    func loadColors(completion: @escaping () -> ()) {
        CSPAPIManager.shared.getColors { [weak self] colors in
            self?.listColors = colors
            self?.currentColor = colors.first
            completion()
        }
    }
    
    func canCreateList() -> Bool {
        return listName != nil && listName != "" && currentColor != nil
    }
    
    func createList(completion: @escaping (Bool) -> ()) {
        CSPAPIManager.shared.createList((_: listName!, _: currentColor!)) { error in
            completion(error == nil)
        }
    }
    
}

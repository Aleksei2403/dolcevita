//
//  CSPSettingsModel.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

enum CSPSettingsItemType {
    
    case Circle
    case NewCircle
    case AccountInfo
    case ContactUs
    case RateUs
    case TellFriend
    
}

struct CSPSettingsItem {
    
    var title: String
    var isSelected: Bool
    var isAuthor: Bool
    var type: CSPSettingsItemType
    
    init(_ title: String, _ isSelected: Bool, _ isAuthor: Bool, _ type: CSPSettingsItemType) {
        self.title = title
        self.isSelected = isSelected
        self.isAuthor = isAuthor
        self.type = type
    }
    
}

class CSPSettingsModel {
    
    var sectionTitle = ["Manage Circles", "Manage Account", nil]
    
    var dataSource: [[CSPSettingsItem]]
    
    var circles = CSPDBManager.shared.getCircles()
    
    init() {
        var circlesSettingsItems = circles.map { CSPSettingsItem($0.name ?? "", $0 == CSPSessionManager.currentCircle!, $0.author == CSPSessionManager.currentUser, .Circle) }
        circlesSettingsItems.append(CSPSettingsItem("+ Create New Circle", false, false, .NewCircle))
        dataSource = [
            circlesSettingsItems,
            [
                CSPSettingsItem("Account Info", false, false, .AccountInfo)
            ],
            [
                CSPSettingsItem("Contact Us", false, false, .ContactUs),
                CSPSettingsItem("Rate Us on AppStore", false, false, .RateUs),
                CSPSettingsItem("Tell a Friend", false, false, .TellFriend)
            ]
        ]
    }
    
    func getSectionTitle(forSection section: Int) -> String? {
        return sectionTitle[section]
    }
    
    func getSettingsItem(atIndexPath ip: IndexPath) -> CSPSettingsItem {
        return dataSource[ip.section][ip.row]
    }
    
    func getCircle(atIndexPath ip: IndexPath) -> CSPCircleModel {
        return circles[ip.row]
    }
    
}

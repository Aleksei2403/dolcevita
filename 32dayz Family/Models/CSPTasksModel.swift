//
//  CSPTasksModel.swift
//  32dayz Family
//
//  Created by Pavel Maksimov on 9/12/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import Amplitude
class CSPTasksModel {
    
    var mode: MenuItemType = .Today
    var list: CSPListModel?
    
    // MARK: - Data
    
    private var _overdueTasks: [CSPTaskModel] = []
    private var _tasks: [CSPTaskModel] = []
    var overdueTasks: [CSPTaskModel] {
        get {
            return _overdueTasks
        }
        set {
            _overdueTasks = newValue
            myOverdueTasks = newValue.filter { $0.membersArray.contains(CSPSessionManager.currentUser!) }
        }
    }
    var allTasks: [CSPTaskModel] {
        get {
            return _tasks
        }
        set {
            _tasks = newValue
            myTasks = newValue.filter { $0.membersArray.contains(CSPSessionManager.currentUser!) }
            sortAllTasks()
        }
    }
    var myOverdueTasks: [CSPTaskModel] = []
    var myTasks: [CSPTaskModel] = []
    
    private func sortAllTasks() {
        allTasksArray = sortedArray(allTasks)
        myTasksArray = sortedArray(myTasks)
    }
    
    private func sortedArray(_ array: [CSPTaskModel]) -> [(NSDate?, [CSPTaskModel])] {
        let dates = array.compactMap { $0.date?.truncateTime }.removeDuplicates().sorted { $0 < $1 }
        var sortedAray = dates.map { date -> (NSDate?, [CSPTaskModel]) in
            return (date, array.filter { date.truncateTime == $0.date?.truncateTime })
        }
        let tasksWithoutDate = array.compactMap({ $0.date == nil ? $0 : nil })
        if tasksWithoutDate.count > 0 {
            sortedAray.append((nil, tasksWithoutDate))
        }
        return sortedAray
    }
    
    var allTasksArray: [(NSDate?, [CSPTaskModel])] = []
    var myTasksArray: [(NSDate?, [CSPTaskModel])] = []
    
    func swapAt(_ firstIndex: Int, _ secondIndex: Int) {
        CSPDBManager.shared.swapTasks(_tasks[firstIndex], _tasks[secondIndex])  //pm changes tasks indexes be reference
        CSPAPIManager.shared.saveIndexesFor(firstTask: _tasks[firstIndex], secondTask: _tasks[secondIndex])
        _tasks.swapAt(firstIndex, secondIndex)//pm swap in allTasks array
        allTasks = _tasks                                                       //pm update myTasks order
    }
    
    // MARK: -
    
    var expandedCellIndexPath: IndexPath?
    
    var currentTask: CSPTaskModel?
    
    func reload() {
        switch mode {
        case .Today:
            allTasks = CSPDBManager.shared.getTodayTasks()
            break
        case .Next7Days:
            allTasks = CSPDBManager.shared.getNext7DaysTasks()
            break
        case .Someday:
            allTasks = CSPDBManager.shared.getSomedayTasks()
            break
        case .Inbox, .CustomList:
            if list != nil {
                allTasks = CSPDBManager.shared.getTasks(forList: list!)
            }
            break
        default:
            break
        }
    }
    
    func completeTask(_ task: CSPTaskModel, completion: @escaping (Error?) -> ()) {
        CSPAPIManager.shared.completeTask(task) { error in
            completion(error)
        }
    }
    
    func saveTask(_ task: CSPTaskModel, completion: @escaping (Error?) -> ()) {
        CSPAPIManager.shared.saveTask(task) { error in
             completion(error)
        }
    }
    
    func getCircles(_ completion: @escaping ([CSPCircleModel]) -> ()) {
        CSPAPIManager.shared.getCircles { circles in
            completion(circles)
        }
    }
    
    func commitChanges(_ completion: @escaping (Error?) -> ()) {
        CSPAPIManager.shared.commitChanges(allTasks) { error in
            completion(error)
        }
        
        // MARK: - Analytics

        AnalyticsManager.logEvent(type: .TaskPositionChanged)
        // MARK: -
        
    }
    
}

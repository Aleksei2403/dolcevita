//
//  CSPJournalModel.swift
//  32dayz Family
//
//  Created by Pavel Maksimov on 9/12/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPJournalModel {
    
    var  posts: [AnyObject] = []
    
    func getCircles(_ completion: @escaping ([CSPCircleModel]) -> ()) {
        CSPAPIManager.shared.getCircles { circles in
            completion(circles)
        }
    }
    
    func reload() {
        posts = CSPDBManager.shared.getJournal()
    }
    
    func deletePost(_ post: CSPPostModel, completion: @escaping (Error?) -> ()) {
        CSPAPIManager.shared.deletePost(post) { error in
            completion(error)
        }
    }
   
    
    
    
}

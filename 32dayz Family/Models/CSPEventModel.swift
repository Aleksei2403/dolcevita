//
//  CSPEventModel.swift
//  32dayz Family
//
//  Created by mac on 27.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import Foundation
import UserNotifications
import CoreData

class CSPEventsModel {
    var circle = CSPSessionManager.currentCircle
  
    private var _events: [CSPEventModel] = []
    
    var allEvent: [CSPEventModel] {
        get {
            return _events
        }
       
    }
    
    func getCircles(_ completion: @escaping ([CSPCircleModel]) -> ()) {
        CSPAPIManager.shared.getCircles { circles in
            completion(circles)
        }
    }
    
    func  scheduleNotification(inSeconds seconds: TimeInterval, completion: (Bool) ->()) {
        
        //removeNotifications(withIdentifiers: ["MyUniqueIdentifier"])
        
        let date = Date(timeIntervalSinceNow: seconds)
        
        
        let content = UNMutableNotificationContent()
        content.title = "notification"
        content.body = "body"
        content.sound = UNNotificationSound.default
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.month, .day, .hour, .minute, .second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        let request = UNNotificationRequest(identifier: "MyUniqueIdentifier", content: content, trigger: trigger)
        
        //let center = UNUserNotificationCenter.current()
        AppDelegate.shared.center.add(request, withCompletionHandler: nil)
        
    }
    
    func removeNotifications(withIdentifiers identifiers: [String]) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: identifiers)
    }
    
    func reload() {
        CSPAPIManager.shared.getCircles { circles in
            
        }
    }
    
    
}

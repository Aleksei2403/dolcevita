//
//  CSPIAPManager.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/29/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation
import StoreKit

class CSPIAPManager: NSObject {
    
    static let shared = CSPIAPManager()

    private struct ProductID {
        static let oneMonth = "com.nesus.32dayzFamily.1month"
        static let threeMonths = "com.nesus.32dayzFamily.3months"
        static let twelveMonths = "com.nesus.32dayzFamily.12months"
    }

    fileprivate var fetchHandler: ((Error?) -> ())?
    fileprivate var purchaseHandler: ((Error?) -> ())?
    fileprivate var restoreHandler: ((Error?) -> ())?

    fileprivate var _products: [SKProduct]?
    public var products: [SKProduct] {
        get {
            return self._products == nil ? [] : self._products!.sorted { $0.price.compare($1.price) == .orderedDescending }
        }
        set {
            self._products = newValue
        }
    }

    public func getProducts(completion: @escaping (Error?) -> ()) {
        fetchHandler = completion
        if products.count == 3 {
            completion(nil)
        } else {
            fetchProducts(completion: completion)
        }
    }

    public func fetchProducts(completion: @escaping (Error?) -> ()) {
//        fetchHandler = completion
//        let productsRequest = SKProductsRequest(productIdentifiers: Set([ProductID.twelveMonths, ProductID.threeMonths, ProductID.oneMonth]))
//        productsRequest.delegate = self
//        productsRequest.start()
    }

    public func purchase(_ product: SKProduct, completion: @escaping (Error?) -> ()) {
        if SKPaymentQueue.canMakePayments() {
            purchaseHandler = completion
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
        } else {
            completion(nil)
        }
    }

    public func restore(completion: @escaping (Error?) -> ()) {
        restoreHandler = completion
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }

}

extension CSPIAPManager: SKProductsRequestDelegate {

    // MARK: - Fetch Products

    internal func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
        fetchHandler?(nil)
    }

    internal func request(_ request: SKRequest, didFailWithError error: Error) {
        fetchHandler?(error)
    }

}

extension CSPIAPManager: SKPaymentTransactionObserver {

    // MARK: - Purchase

    internal func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased, .restored:
                purchaseHandler?(nil)
                break
            case .failed:
                purchaseHandler?(NSError(domain: "Purchase failed", code: 0, userInfo: nil))
            case .deferred:
                break
            case .purchasing:
                break
            }
        }
    }

    // MARK: - Restore Purchase

    internal func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        restoreHandler?(nil)
    }

    internal func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        restoreHandler?(error)
    }

}

////
////  LocalNotifications.swift
////  32dayz Family
////
////  Created by mac on 27.05.21.
////  Copyright © 2021 Nesus UAB. All rights reserved.
////
//
//import Foundation
//import UserNotifications
//
//class LocalNotifications {
//
//    func createDate(weekday: Int, hour: Int, minute: Int, year: Int)->Date{
//
//        var components = DateComponents()
//        components.hour = hour
//        components.minute = minute
//        components.year = year
//        components.weekday = weekday // sunday = 1 ... saturday = 7
//        components.weekdayOrdinal = 10
//        components.timeZone = .current
//
//        let calendar = Calendar(identifier: .gregorian)
//        return calendar.date(from: components)!
//    }
//
//    //Schedule Notification with weekly bases.
//    func scheduleNotification(at date: Date, body: String, titles:String) {
//
//        let triggerWeekly = Calendar.current.dateComponents([.weekday,.hour,.minute,.second,], from: date)
//
//        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
//
//        let content = UNMutableNotificationContent()
//        content.title = titles
//        content.body = body
//        content.sound = UNNotificationSound.default
//        content.categoryIdentifier = "todoList"
//
//        let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: trigger)
//
//        //UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
//        UNUserNotificationCenter.current().add(request) {(error) in
//            if let error = error {
//                print(" We had an error: \(error)")
//            }
//        }
//    }
//}
//
import Foundation
import UserNotifications
class LocalNotificationRequeust {
    static let shared = LocalNotificationRequeust()
    
    func sendNotifications(timeIntrval: TimeInterval){
        
        let content = UNMutableNotificationContent()
        content.title = "Dolce Vita"
        content.body = "You haven't completed the task"
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeIntrval, repeats: false)
        let request = UNNotificationRequest(identifier: "notifications", content: content, trigger: trigger)
        AppDelegate.shared.center.add(request)
    }
    
}

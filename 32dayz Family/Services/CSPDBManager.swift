//
//  CSPDBManager.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/21/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//
import UIKit
import Foundation
import CoreData
import UserNotifications

var fetchComments = [CSPCommentModel]()

class CSPDBManager {
    
    static let shared = CSPDBManager()
    
    static let dbName = "32dayzFamilyDatabase"
    static let dbExtension = "momd"
    static let db = "32dayzFamilyDatabase.sqlite"
    
    static let kCurrentUserUID = "CSPCurrentUserUIDKey"
    static let kCurrentCircleUID = "CSPCurrentCircleUIDKey"
    
    struct Entity {
        static let User = "CSPUserModel"
        static let Circle = "CSPCircleModel"
        static let Post = "CSPPostModel"
        static let Task = "CSPTaskModel"
        static let List = "CSPListModel"
        static let Comments = "CSPCommentModel"
        static let Events = "CSPEventModel"
    }
    
    // MARK: - Public Api
    
    // MARK: - Session
    
    public var currentUser: CSPUserModel? {
        get {
            if let uid = UserDefaults.standard.string(forKey: CSPDBManager.kCurrentUserUID) {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.User)
                fetchRequest.predicate = NSPredicate(format: "uid like '\(uid)'")
                do {
                    let users = try managedObjectContext.fetch(fetchRequest) as! [CSPUserModel]
                    return users.first
                } catch {
                    print(error)
                }
            }
            return nil
        }
    }
    
    public func setUser(_ user: CSPUserModel) {
        UserDefaults.standard.set(user.uid!, forKey: CSPDBManager.kCurrentUserUID)
        UserDefaults.standard.synchronize()
    }
    
    public func signOut() {
        UserDefaults.standard.removeObject(forKey: CSPDBManager.kCurrentUserUID)
        UserDefaults.standard.removeObject(forKey: CSPDBManager.kCurrentCircleUID)
        UserDefaults.standard.synchronize()
    }
    
    public var currentCircle: CSPCircleModel? {
        get {
            if let uid = UserDefaults.standard.string(forKey: CSPDBManager.kCurrentCircleUID) {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.Circle)
                fetchRequest.predicate = NSPredicate(format: "uid like '\(uid)'")
                do {
                    let circles = try managedObjectContext.fetch(fetchRequest) as! [CSPCircleModel]
                    return circles.first
                } catch {
                    print(error)
                }
            }
            return nil
        }
    }
    
    public func setCircle(_ circle: CSPCircleModel) {
        UserDefaults.standard.set(circle.uid!, forKey: CSPDBManager.kCurrentCircleUID)
        UserDefaults.standard.synchronize()
    }
    
    public var currentInbox: CSPListModel? {
        get {
            return ((currentCircle?.lists?.array as? [CSPListModel])?.filter { $0.name == "Inbox" })?.first
        }
    }
    
    // MARK: -
    
    public func updateUser(_ user: CSPUserModel, _ dictionary: [String: Any]) -> CSPUserModel {
        user.notVerification = ((dictionary[DBKey.NotVerivication] as? String) != nil)
        user.uid = dictionary[DBKey.uid] as? String
        user.firstName = dictionary[DBKey.FirstName] as? String
        user.lastName = dictionary[DBKey.LastName] as? String
        user.phone = dictionary[DBKey.Phone] as? String
        user.photoURL = dictionary[DBKey.PhotoURL] as? String
        user.createdAt = Date(timeIntervalSince1970: dictionary[DBKey.CreatedAt] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate as Date
        user.goldUntill = Date(timeIntervalSince1970: dictionary[DBKey.Gold] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate as Date
        saveContext()
        return user
        
    }
    private func getUser(withUID uid: String) -> CSPUserModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.User)
        fetchRequest.predicate = NSPredicate(format: "uid like '\(uid)'")
        do {
            let users = try managedObjectContext.fetch(fetchRequest) as! [CSPUserModel]
            return users.first
        } catch {
            print(error)
        }
        return nil
    }
    
    public func saveUser(_ uid: String, _ dictionary: [String: Any]) -> CSPUserModel {
        var user = getUser(withUID: uid)
        if user == nil {
            let entity = NSEntityDescription.entity(forEntityName: Entity.User, in: managedObjectContext)
            user = CSPUserModel(entity: entity!, insertInto: managedObjectContext)
        }
        user!.notVerification = dictionary[DBKey.NotVerivication] as? Bool ?? false
        user!.uid = uid
        user!.firstName = dictionary[DBKey.FirstName] as? String
        user!.lastName = dictionary[DBKey.LastName] as? String
        user!.phone = dictionary[DBKey.Phone] as? String
        user!.photoURL = dictionary[DBKey.PhotoURL] as? String
        user!.createdAt = Date(timeIntervalSince1970: dictionary[DBKey.CreatedAt] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate as Date
        user!.goldUntill = Date(timeIntervalSince1970: dictionary[DBKey.Gold] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate as Date
        
        saveContext()
        return user!
    }
    
    public func setAsValidate( completion: @escaping () -> ()) -> CSPUserModel {
        var user = CSPSessionManager.currentUser
        if user == nil {
            let entity = NSEntityDescription.entity(forEntityName: Entity.User, in: managedObjectContext)
            user = CSPUserModel(entity: entity!, insertInto: managedObjectContext)
        }
        user?.notVerification = false
        saveContext()
        return user!
    }
    
    public func updatePhone(_ uid: String, _ phone: String) -> CSPUserModel {
        var user = getUser(withUID: uid)
        if user == nil {
            let entity = NSEntityDescription.entity(forEntityName: Entity.User, in: managedObjectContext)
            user = CSPUserModel(entity: entity!, insertInto: managedObjectContext)
        }
        user!.phone = phone
        saveContext()
        return user!
    }
    
    public func updateSubscription(_ uid: String, till ti: TimeInterval) -> CSPUserModel {
        var user = getUser(withUID: uid)
        if user == nil {
            let entity = NSEntityDescription.entity(forEntityName: Entity.User, in: managedObjectContext)
            user = CSPUserModel(entity: entity!, insertInto: managedObjectContext)
        }
        user!.goldUntill = NSDate(timeIntervalSinceReferenceDate: ti) as Date
        saveContext()
        return user!
    }
    
    public func updateName(_ uid: String, _ fn: String, _ ln: String) -> CSPUserModel {
        var user = getUser(withUID: uid)
        if user == nil {
            let entity = NSEntityDescription.entity(forEntityName: Entity.User, in: managedObjectContext)
            user = CSPUserModel(entity: entity!, insertInto: managedObjectContext)
        }
        user!.firstName = fn
        user!.lastName = ln
        saveContext()
        return user!
    }
    
    public func updatePhoto(_ uid: String, _ photoURL: String) -> CSPUserModel {
        var user = getUser(withUID: uid)
        if user == nil {
            let entity = NSEntityDescription.entity(forEntityName: Entity.User, in: managedObjectContext)
            user = CSPUserModel(entity: entity!, insertInto: managedObjectContext)
        }
        user!.photoURL = photoURL
        saveContext()
        return user!
    }
    
    public func saveCircle(_ uid: String, _ dictionary: [String: Any], _ author: CSPUserModel?, _ users: [CSPUserModel], _ lists: [CSPListModel] = [], _ posts: [CSPPostModel] = [], _ tasks: [CSPTaskModel] = [],_ events: [CSPEventModel] = [] ) -> CSPCircleModel {
        var circle = getCircle(withUID: uid)
        if circle == nil {
            circle = createNewCircle()
        }
        circle!.uid = uid
        circle!.name = dictionary[DBKey.Name] as? String
        circle!.author = author
        (circle!.members?.array as? [CSPUserModel])?.forEach { circle!.removeFromMembers($0) }
        (circle!.lists?.array as? [CSPListModel])?.forEach { circle!.removeFromLists($0) }
        (circle!.posts?.array as? [CSPPostModel])?.forEach { circle!.removeFromPosts($0) }
        (circle!.tasks?.array as? [CSPTaskModel])?.forEach { circle!.removeFromTasks($0) }
        (circle!.events?.array as? [CSPEventModel])?.forEach { circle!.removeFromEvents($0) }
        users.forEach { circle!.addToMembers($0) }
        lists.forEach { circle!.addToLists($0) }
        posts.forEach { circle!.addToPosts($0) }
        tasks.forEach { circle!.addToTasks($0) }
        events.forEach { circle!.addToEvents($0)}
        saveContext()
        return circle!
    }
    
    public func createNewCircle() -> CSPCircleModel {
        let entity = NSEntityDescription.entity(forEntityName: Entity.Circle, in: managedObjectContext)
        return CSPCircleModel(entity: entity!, insertInto: managedObjectContext)
    }
    
    public func deleteCircles(except circleUIDs: [String]) {
        for circle in getCircles() {
            if !circleUIDs.contains(circle.uid!) {
                managedObjectContext.delete(circle)
            }
        }
        saveContext()
    }
    
    public func deleteCircle(_ circle: CSPCircleModel) {
        managedObjectContext.delete(circle)
        saveContext()
    }
    
    public func deleteList(_ list: CSPListModel) {
        managedObjectContext.delete(list)
        saveContext()
    }
    
    func addUserInvited( _ user: CSPUserModel) {
        if let circle = currentCircle {
            let users = (circle.invitedMembers?.array as? [CSPUserModel])?.filter { $0.uid == user.uid }
            users?.forEach { circle.removeFromMembers($0) }
            circle.addToInvitedMembers(user)
            saveContext()
            
        }
    }
    
    func addComment(_ comment: CSPCommentModel) {
        if let circle = currentCircle {
            let comments = (circle.comments?.array as? [CSPCommentModel])?.filter { $0.uid == comment.postId }
            comments?.forEach { circle.removeFromComments($0) }
            circle.addToComments(comment)
            saveContext()
            
        }
    }
    
    func addUser(_ user: CSPUserModel) {
        if let circle = currentCircle {
            let users = (circle.members?.array as? [CSPUserModel])?.filter { $0.uid == user.uid }
            users?.forEach { circle.removeFromMembers($0) }
            circle.addToMembers(user)
            saveContext()
            
        }
    }
    
    func addList(_ list: CSPListModel) {
        if let circle = currentCircle {
            let lists = (circle.lists?.array as? [CSPListModel])?.filter { $0.uid == list.uid }
            lists?.forEach { circle.removeFromLists($0) }
            circle.addToLists(list)
            saveContext()
        }
    }
    
    func addPost(_ post: CSPPostModel) {
        if let circle = currentCircle {
            let posts = (circle.posts?.array as? [CSPPostModel])?.filter { $0.uid == post.uid }
            posts?.forEach { circle.removeFromPosts($0) }
            circle.addToPosts(post)
            saveContext()
        }
    }
    
    private func getComments(withUID uid: String) -> CSPCommentModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.Comments)
        fetchRequest.predicate = NSPredicate(format: "uid = %@", uid)
        do {
            let com = try managedObjectContext.fetch(fetchRequest) as! [CSPCommentModel]
            return com.first
        } catch {
            print(error)
        }
        return nil
    }

    public func saveComment(_ uid: String, _ dictionary: [Any], _ author: String){
        for item in dictionary {
            if let travelJson = item as? [String:Any] {
                if let author = travelJson["author"] as? String,
                   let authorImage = travelJson["authorImage"] as? String,
                   let circleId = travelJson["circleId"] as? String,
                   let content = travelJson["content"] as? String,
                   let postId = travelJson["postId"] as? String,
                   let uid = travelJson["uid"] as? String,
                   let createdAt = Date(timeIntervalSince1970: travelJson["created_at"] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as? NSDate {
                    var comment = getComments(withUID: uid)
                    if comment == nil {
                        comment = createComment()
                        comment!.content = content
                        comment!.authorImage = authorImage
                        comment!.uid = uid
                        comment!.author = author
                        comment!.circleId = circleId
                        comment!.postId = postId
                        comment?.createdAt = createdAt as Date

                        saveContext()
                    }
                }
            }
        }
    }
    
 
    
    func addEvent(_ event: CSPEventModel) {
        if let circle = currentCircle {
            let events = (circle.events?.array as? [CSPEventModel])?.filter { $0.uid == event.uid}
            events?.forEach { circle.removeFromEvents($0) }
            circle.addToEvents(event)
            saveContext()
        }
    }
    
    func addTask(_ task: CSPTaskModel) {
        if let circle = currentCircle {
            let tasks = (circle.tasks?.array as? [CSPTaskModel])?.filter { $0.uid == task.uid }
            tasks?.forEach { circle.removeFromTasks($0) }
            circle.addToTasks(task)
            saveContext()
        }
    }
    
    private func getCircle(withUID uid: String) -> CSPCircleModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.Circle)
        fetchRequest.predicate = NSPredicate(format: "uid like '\(uid)'")
        do {
            let circles = try managedObjectContext.fetch(fetchRequest) as! [CSPCircleModel]
            return circles.first
        } catch {
            print(error)
        }
        return nil
    }
    
    public func saveEvent(_ uid: String, _ dictionary: [String: Any], _ author: CSPUserModel, _ users: [CSPUserModel]) -> CSPEventModel {
        var event = getEvents(withUID: uid)
        if event == nil {
            event = createNewEvent()
        }
        event!.uid = uid
        event!.content = dictionary[DBKey.Content] as? String
        event!.location = dictionary[DBKey.Location] as? String
        event!.createdAt = Date(timeIntervalSince1970: dictionary[DBKey.CreatedAt] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate
        event!.author = author
        if let ti = dictionary[DBKey.Date] as? TimeInterval {
            event!.date = Date(timeIntervalSince1970: ti) as NSDate
        }
        event?.start = dictionary["start"] as? String
        event?.allDay = (dictionary["allDay"] as? Bool)!
        if  let createdAt = dictionary[DBKey.CreatedAt] as? TimeInterval {
            event?.createdAt = Date(timeIntervalSince1970: createdAt as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as? NSDate
        }
        if let reminder = dictionary["reminder"] as? Bool {
            event?.reminder = reminder
        }
        if let startTime = dictionary["startTime"]as? TimeInterval {
            event?.startTime = Date(timeIntervalSince1970: startTime as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as? NSDate
        }
        if let endsTime = dictionary["endsTime"] as? TimeInterval {
            event?.endsTime = Date(timeIntervalSince1970: endsTime as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as? Date as NSDate?
        }
        users.forEach { event!.addToMembers($0) }
        
        saveContext()
        return event!
    }
    
    private func getEvents(withUID uid: String) -> CSPEventModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.Events)
        fetchRequest.predicate = NSPredicate(format: "uid = %@", uid)
        do {
            let events = try managedObjectContext.fetch(fetchRequest) as! [CSPEventModel]
            return events.first
            
        } catch {
            print(error)
        }
        return nil
    }
    
    public func saveTask(_ uid: String, _ dictionary: [String: Any], _ author: CSPUserModel, _ users: [CSPUserModel], _ list: CSPListModel) -> CSPTaskModel {
        var task = getTask(withUID: uid)
        if task == nil {
            task = createNewTask()
        }
        task!.uid = uid
        task!.priority = dictionary[DBKey.Priority] as? String
        task!.content = dictionary[DBKey.Content] as? String
        task!.start = dictionary[DBKey.Start] as? String
        task!.createdAt = Date(timeIntervalSince1970: dictionary[DBKey.CreatedAt] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate
        task!.index = dictionary[DBKey.Index] as? Int16 ?? 0
        task!.author = author
        if let ti = dictionary[DBKey.Date] as? TimeInterval {
            task!.date = Date(timeIntervalSince1970: ti) as NSDate
        }
        users.forEach { task!.addToMembers($0) }
        task!.list = list
        
        saveContext()
        return task!
    }
    
    private func getTask(withUID uid: String) -> CSPTaskModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.Task)
        fetchRequest.predicate = NSPredicate(format: "uid like '\(uid)'")
        do {
            let tasks = try managedObjectContext.fetch(fetchRequest) as! [CSPTaskModel]
            return tasks.first
        } catch {
            print(error)
        }
        return nil
    }
    
    public func createNewTask() -> CSPTaskModel {
        let entity = NSEntityDescription.entity(forEntityName: Entity.Task, in: managedObjectContext)
        return CSPTaskModel(entity: entity!, insertInto: managedObjectContext)
    }
    
    public func deleteTask(_ task: CSPTaskModel) {
        managedObjectContext.delete(task)
        saveContext()
    }
    
    public func swapTasks(_ firstTask: CSPTaskModel, _ secondTask: CSPTaskModel) {
        let index = firstTask.index
        firstTask.index = secondTask.index
        secondTask.index = index
        saveContext()
    }
    
    public func createNewEvent() -> CSPEventModel {
        let entity = NSEntityDescription.entity(forEntityName: Entity.Events, in: managedObjectContext)
        return CSPEventModel(entity: entity!, insertInto: managedObjectContext)
    }
    
    public func deleteEvent(_ event: CSPEventModel) {
        
        managedObjectContext.delete(event)
        saveContext()
    }
    
    public func savePost(_ uid: String, _ dictionary: [String: Any], _ author: CSPUserModel, _ list: CSPListModel? = nil) -> CSPPostModel {
        var post = getPost(withUID: uid)
        if post == nil {
            post = createNewPost()
        }
        post!.uid = uid
        post!.content = dictionary[DBKey.Content] as? String
        post!.mediaURL = dictionary[DBKey.PhotoURL] as? String
        post!.mediaAspectRatio = dictionary[DBKey.PhotoAspectRatio] as? NSNumber ?? NSNumber(value: 1)
        post!.createdAt = Date(timeIntervalSince1970: dictionary[DBKey.CreatedAt] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate
        post!.author = author
        post!.type = dictionary[DBKey.PostType] as? Int16 ?? CSPPostType.Post.rawValue
        post!.list = list
        saveContext()
        return post!
    }
    
    
    private func getPost(withUID uid: String) -> CSPPostModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.Post)
        fetchRequest.predicate = NSPredicate(format: "uid like '\(uid)'")
        do {
            let posts = try managedObjectContext.fetch(fetchRequest) as! [CSPPostModel]
            return posts.first
        } catch {
            print(error)
        }
        return nil
    }
    
    public func createNewPost() -> CSPPostModel {
        let entity = NSEntityDescription.entity(forEntityName: Entity.Post, in: managedObjectContext)
        return CSPPostModel(entity: entity!, insertInto: managedObjectContext)
    }
    
    public func createComment() -> CSPCommentModel {
        let entity = NSEntityDescription.entity(forEntityName: Entity.Comments, in: managedObjectContext)
        return CSPCommentModel(entity: entity!, insertInto: managedObjectContext)
    }
    
    public func deletePost(_ post: CSPPostModel) {
        managedObjectContext.delete(post)
        saveContext()
    }
    
    
    public func saveList(_ uid: String, _ dictionary: [String: Any], _ tasks: [CSPTaskModel] = []) -> CSPListModel {
        var list = getList(withUID: uid)
        if list == nil {
            let entity = NSEntityDescription.entity(forEntityName: Entity.List, in: managedObjectContext)
            list = CSPListModel(entity: entity!, insertInto: managedObjectContext)
        }
        list!.uid = uid
        list!.name = dictionary[DBKey.Name] as? String
        list!.color = dictionary[DBKey.Color] as? String
        list!.circleUID = dictionary[DBKey.Circle] as? String
        list!.createdAt = Date(timeIntervalSince1970: dictionary[DBKey.CreatedAt] as? TimeInterval ?? Date.timeIntervalSinceReferenceDate) as NSDate
        var taskUIDs: [String] = []
        if let tasks = dictionary[DBKey.Tasks] as? NSDictionary {
            taskUIDs = (tasks.allKeys as! [String]).filter { tasks[$0] as? Bool == true }
        }
        list!.tasksCount = Int16(taskUIDs.count)
        saveContext()
        return list!
    }
    
    public func getList(withUID uid: String) -> CSPListModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.List)
        fetchRequest.predicate = NSPredicate(format: "uid like '\(uid)'")
        do {
            let lists = try managedObjectContext.fetch(fetchRequest) as! [CSPListModel]
            return lists.first
        } catch {
            print(error)
        }
        return nil
    }
    
    public func getLists() -> [CSPListModel] {
        return currentCircle?.listsArray ?? []
    }
    
    
    public func getEvents() -> [CSPEventModel] {
        return currentCircle?.eventsArray ?? []
    }
    
   
    public func getTasks() -> [CSPTaskModel] {
        return currentCircle?.tasksArray ?? []
    }
    
    public func getCircles() -> [CSPCircleModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.Circle)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do {
            let circles = try managedObjectContext.fetch(fetchRequest) as! [CSPCircleModel]
            return circles
        } catch {
            print(error)
        }
        return []
    }
    
    public func getJournal() -> [CSPPostModel] {
        return (currentCircle?.postsArray ?? []).sorted { firstPost, secondPost in
            guard let firstDate = firstPost.createdAt,
                let secondDate = secondPost.createdAt else {
                    return false
            }
            return firstDate.compare(secondDate as Date) == .orderedDescending
        }
    }
    
    public func getOverdueTasks() -> [CSPTaskModel] {
        return (currentCircle?.tasksArray.filter { $0.date?.isOverdue ?? false } ?? []).sorted { firstTask, secondTask in
            guard let firstDate = firstTask.createdAt,
                let secondDate = secondTask.createdAt else {
                    return false
            }
            return firstDate.compare(secondDate as Date) == .orderedDescending
        }
    }
    
    public func getTodayTasks() -> [CSPTaskModel] {
        return (currentCircle?.tasksArray.filter { $0.date?.isToday ?? false } ?? []).sorted { firstTask, secondTask in
            guard let firstDate = firstTask.createdAt,
                let secondDate = secondTask.createdAt else {
                    return false
            }
            return firstDate.compare(secondDate as Date) == .orderedDescending
        }
    }
    
    public func getNext7DaysTasks() -> [CSPTaskModel] {
        return (currentCircle?.tasksArray.filter { $0.date?.isNext7Days ?? false } ?? []).sorted { firstTask, secondTask in
            guard let firstDate = firstTask.date,
                let secondDate = secondTask.date else {
                    return false
            }
            return firstDate.compare(secondDate as Date) == .orderedAscending
        }
    }
    
    public func getSomedayTasks() -> [CSPTaskModel] {
        return (currentCircle?.tasksArray.filter { $0.date?.isSomeday ?? true } ?? []).sorted { firstTask, secondTask in
           guard let firstDate = firstTask.date,
               let secondDate = secondTask.date else {
                   return false
           }
           return firstDate.compare(secondDate as Date) == .orderedAscending
       }
    }
    
    public func getTasks(forList list: CSPListModel) -> [CSPTaskModel] {
        return (currentCircle?.tasksArray.filter { $0.list == list } ?? []).sorted { $0.index > $1.index }
    }
    
    
    // MARK: - Core Data Stack
    
    var managedObjectContext: NSManagedObjectContext {
        if _managedObjectContext == nil {
            let coordinator = persistentStoreCoordinator
            _managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            
            _managedObjectContext!.persistentStoreCoordinator = coordinator
        }
        return _managedObjectContext!
    }
    var _managedObjectContext: NSManagedObjectContext? = nil
    
    var managedObjectModel: NSManagedObjectModel {
        if _managedObjectModel == nil {
            if let modelURL = Bundle.main.url(forResource: CSPDBManager.dbName, withExtension: CSPDBManager.dbExtension) {
                _managedObjectModel = NSManagedObjectModel(contentsOf: modelURL)
            }
        }
        return _managedObjectModel!
    }
    var _managedObjectModel: NSManagedObjectModel? = nil
    
    var persistentStoreCoordinator: NSPersistentStoreCoordinator {
        if _persistentStoreCoordinator == nil {
            let storeURL = applicationDocumentsDirectory.appendingPathComponent(CSPDBManager.db)
            _persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
            do {
                try _persistentStoreCoordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
                print(storeURL)
            } catch {
                print("Unresolved error \(error), \(error.localizedDescription)")
                abort()
            }
        }
        return _persistentStoreCoordinator!
    }
    var _persistentStoreCoordinator: NSPersistentStoreCoordinator? = nil
    
    var applicationDocumentsDirectory: NSURL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last! as NSURL
    }
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                print("Unresolved error \(error), \(error.localizedDescription)")
                abort()
            }
        }
    }
    
}

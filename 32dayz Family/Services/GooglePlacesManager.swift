//
//  GooglePlacesManager.swift
//  32dayz Family
//
//  Created by mac on 21.09.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import Foundation
import GooglePlaces

struct Place {
    let name: String
    let identifier: String
}

final class GooglePlacesManager {
    static let shared = GooglePlacesManager()
    private let locationManager = CLLocationManager()
    private let client = GMSPlacesClient.shared()
    
    private init() {}
    
    enum PlacesError : Error {
        case failedToFind
    }
    
    
    
    public func findPlaces(query: String, completion: @escaping (Result<[Place], Error>) -> Void) {
        let filters = GMSAutocompleteFilter()
        filters.type = .geocode
        client.findAutocompletePredictions(fromQuery: query, filter: filters, sessionToken: nil) { results, error in
            guard let results = results , error == nil else {
                completion(.failure(PlacesError.failedToFind))
                return
            }
            
            let places: [Place] = results.compactMap({
                Place(name: $0.attributedFullText.string,
                      identifier: $0.placeID)
            })
            
            completion(.success(places))
        }
    }
}

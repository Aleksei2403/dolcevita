//
//  CSPSessionManager.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/21/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import Foundation

class CSPSessionManager {
    
    class var currentUser: CSPUserModel? {
        get {
            return CSPDBManager.shared.currentUser
        }
    }
    
    class func setCurrentUser(_ user: CSPUserModel) {
        CSPDBManager.shared.setUser(user)
    }
    
    class var currentCircle: CSPCircleModel? {
        get {
            return CSPDBManager.shared.currentCircle
        }
    }
    
    class func setCurrentCircle(_ circle: CSPCircleModel) {
        CSPDBManager.shared.setCircle(circle)
    }
    
    class var currentInbox: CSPListModel? {
        get {
            return CSPDBManager.shared.currentInbox
        }
    }
    
    class var isSubscribed: Bool {
        get {
//            #if DEBUG
//                print("user subscribed till \((currentUser?.goldUntill ?? NSDate()) as Date)")
//            #endif
//            return Date() < (currentUser?.goldUntill ?? NSDate()) as Date
            return true
        }
    }
    
    class func signOut() {
        CSPAPIManager.shared.signOut()
        CSPDBManager.shared.signOut()
    }
    
}

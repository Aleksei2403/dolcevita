//
//  CSPAPIManager.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/21/17.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import Foundation
import Firebase
import SystemConfiguration
import Alamofire
import AlamofireObjectMapper
import Contacts

struct DBKey {
    static let uid                      = "uid"
    static let Phone                    = "phone_number"
    static let FirstName                = "first_name"
    static let LastName                 = "last_name"
    static let CreatedAt                = "created_at"
    static let PhotoURL                 = "photo_url"
    static let Author                   = "author"
    static let Name                     = "name"
    static let Users                    = "users"
    static let Circles                  = "circles"
    static let Circle                   = "circle"
    static let Invites                  = "invites"
    static let Gold                     = "gold_untill"
    static let Title                    = "title"
    static let Content                  = "content"
    static let Posts                    = "posts"
    static let Tasks                    = "tasks"
    static let Lists                    = "lists"
    static let Color                    = "color"
    static let Date                     = "date"
    static let List                     = "list"
    static let PostType                 = "type"
    static let PhotoAspectRatio         = "aspect_ratio"
    static let Index                    = "index"
    static let AppTeamId                = "32dayzFamilyTeam"
    static let NotVerivication          = "not_verivication"
    static let Comments                 = "comments"
    static let PostId                   = "postId"
    static let StartTime                = "startTime"
    static let EndsTime                 = "endsTime"
    static let AllDay                   = "allDay"
    static let CircleId                 = "circleId"
    static let AuthorImage              = "authorImage"
    static let Events                   = "events"
    static let Start                    = "start"
    static let Reminder                 = "reminder"
    static let Location                 = "location"
    static let DueDate                  = "dueDate"
    static let Priority                 = "priority"
}

struct Table {
    static let Users        = "users"
    static let Circles      = "circles"
    static let Posts        = "posts"
    static let Tasks        = "tasks"
    static let Lists        = "lists"
    static let Colors       = "colors"
    static let Invites      = "invites"
    static let Comments     = "comments"
    static let Events       = "events"
}

struct Key {
    static let VerificationID = "authVerificationID"
}

class CSPAPIManager {
    
    static let shared = CSPAPIManager()
    
    var isConnected: Bool!
    
    init() {
        isConnected = isInternetConnectionAvailable()
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            self.isConnected = snapshot.value as? Bool ?? false
        })
    }
    
    // MARK: - Authorization
    
    public var currentUser: User? {
        get {
            return Auth.auth().currentUser
        }
    }
    
    public func verifyPhone(_ phone: String, completion: @escaping (Error?) -> ()) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { [weak self] verificationID, error in
            completion(error)
            if let verificationID = verificationID {
                UserDefaults.standard.set(verificationID, forKey: Key.VerificationID)
                UserDefaults.standard.synchronize()
                
            }
            
            // MARK: - Analytics
            
            self?.getUser(withPhoneNumber: phone) { user in
                if user == nil {
                    AnalyticsManager.logEvent(type: .OnboardingPhoneSubmitted)
                }
            }
            
            // MARK: -
            
        }
    }
    
    public func confirmCode(_ code: String, completion: @escaping (Error?) -> ()) {
        if let verificationID = verificationID {
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: code)
            Auth.auth().signIn(with: credential) { user, error in
                completion(error)
                if error == nil {
                    self.setAsValidate()
                    self.clearVerificationID()
                }
            }
        }
    }
    
    public func signOut() {
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print (signOutError.localizedDescription)
        }
    }
    
    public var verificationID: String? {
        get {
            return UserDefaults.standard.string(forKey: Key.VerificationID)
        }
    }
    
    public func clearVerificationID() {
        UserDefaults.standard.removeObject(forKey: Key.VerificationID)
        UserDefaults.standard.synchronize()
    }
    
    // MARK: - Database
    
    let databaseReference = Database.database().reference()
    let storageReference = Storage.storage().reference()
    
    private func saveImage(_ image: UIImage, completion: @escaping (_ imageUrl: URL?, _ error: Error?) -> ()) {
        let data = image.compressed().jpegData(compressionQuality: 1)!
        let imageReference = storageReference.child("photos/\(UInt64(Date.timeIntervalSinceReferenceDate * 1000000)).jpg")
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        imageReference.putData(data, metadata: metadata) { storageMetadata, error in
            imageReference.downloadURL { url, error in
                completion(url, error)
            }
        }
    }
    
    public func createInvitedAccount(account: (firstName: String, lastName: String, phone: String, id: String, circleUid: String), completion: @escaping (Error?) -> ()) {
        let updates = [
            DBKey.Phone: account.phone,
            DBKey.FirstName: account.firstName,
            DBKey.LastName: account.lastName,
            DBKey.CreatedAt: Date().timeIntervalSince1970,
            DBKey.Circle: account.circleUid,
            DBKey.NotVerivication: true 
            ] as [String: Any]
        
        let user = CSPDBManager.shared.saveUser(account.id, updates)
        let database = Database.database().reference()
        let child = database.child("users").child("\(user.uid)")
        child.setValue(updates) {(error , ref) in
            
        }
        
        CSPDBManager.shared.addUser(user)
        print("пользователь создается : \(account)")
        
    }
    public func createAccount(account: (firstName: String, lastName: String, photo: UIImage?), completion: @escaping (Error?) -> ()) {
        let currentUser = Auth.auth().currentUser!
        let userReference = databaseReference.child(Table.Users).child(currentUser.uid)
        var updates = [
            DBKey.Phone: currentUser.phoneNumber!,
            DBKey.FirstName: account.firstName,
            DBKey.LastName: account.lastName,
            DBKey.CreatedAt: Date().timeIntervalSince1970,
            DBKey.NotVerivication: false,
            DBKey.uid : currentUser.uid
            ] as [String: Any]
        let createAccount = {
            userReference.updateChildValues(updates) { error, _ in
                CSPAPIManager.shared.getUser(phoneNumber: currentUser.phoneNumber ?? "") { (user) in
                    if user != nil {
                        CSPDBManager.shared.updateUser(user!, updates)
                        CSPSessionManager.setCurrentUser(user!)
                    } else {
                        let user = CSPDBManager.shared.saveUser(currentUser.uid, updates)
                        CSPSessionManager.setCurrentUser(user)
                    }
                }
                
                completion(error)
                
                // MARK: - Analytics
                
                let parameter = Parameter(.photoAdded, updates[DBKey.PhotoURL] == nil ? .no : .yes)
                AnalyticsManager.logEvent(type: .OnboardingAccountCreated, parameters: [ "photoAdded" : parameter])
                
                // MARK: -
                
            }
        }
        if let photo = account.photo {
            saveImage(photo) { url, error in
                if url != nil {
                    updates[DBKey.PhotoURL] = url!.absoluteString
                } else {
                    completion(error)
                }
                createAccount()
                scheduleOnMainQueueAfter(delay: 1.0) {
                    NotificationCenter.default.post(name: Notification.Name.avatarChanged, object: nil)
                }
            }
        } else {
            createAccount()
        }
    }
    public func setAsValidate() {
            if let user = CSPSessionManager.currentUser {
                let userReference = databaseReference.child(Table.Users).child(user.uid!)
                let updates = [DBKey.NotVerivication: nil] as [String: Any]
                userReference.updateChildValues(updates) { error, _ in
                    let user = CSPDBManager.shared.setAsValidate() {
                        
                    }
                    CSPSessionManager.setCurrentUser(user)
                    
                }
            }
        }
    
    public func updatePhone(_ phone: String, completion: @escaping () -> ()) {
        if let user = CSPSessionManager.currentUser {
            let userReference = databaseReference.child(Table.Users).child(user.uid!)
            let updates = [DBKey.Phone: phone] as [String: Any]
            userReference.updateChildValues(updates) { error, _ in
                let user = CSPDBManager.shared.updatePhone(user.uid!, phone)
                CSPSessionManager.setCurrentUser(user)
                completion()
            }
        } else {
            completion()
        }
    }
    
    public func updateName(_ fn: String, _ ln: String, completion: @escaping () -> ()) {
        if let user = CSPSessionManager.currentUser {
            let userReference = databaseReference.child(Table.Users).child(user.uid!)
            let updates = [
                DBKey.FirstName: fn,
                DBKey.LastName: ln
                ] as [String: Any]
            userReference.updateChildValues(updates) { error, _ in
                let user = CSPDBManager.shared.updateName(user.uid!, fn, ln)
                CSPSessionManager.setCurrentUser(user)
                completion()
            }
        } else {
            completion()
        }
    }
    
    public func updatePhoto(_ image: UIImage, completion: @escaping () -> ()) {
        saveImage(image) { [weak self] url, error in
            guard let user = CSPSessionManager.currentUser,
                let url = url else {
                    completion()
                    return
            }
            let userReference = self?.databaseReference.child(Table.Users).child(user.uid!)
            let updates = [DBKey.PhotoURL: url.absoluteString] as [String: Any]
            userReference?.updateChildValues(updates) { error, _ in
                let user = CSPDBManager.shared.updatePhoto(user.uid!, url.absoluteString)
                CSPSessionManager.setCurrentUser(user)
                completion()
            }
            scheduleOnMainQueueAfter(delay: 1.0) {
                NotificationCenter.default.post(name: Notification.Name.avatarChanged, object: nil)
            }
        }
    }
    
    private func updateSubscription(till ti: TimeInterval, completion: @escaping () -> ()) {
        if let user = CSPSessionManager.currentUser {
            let userReference = databaseReference.child(Table.Users).child(user.uid!)
            let updates = [DBKey.Gold: ti] as [String: Any]
            userReference.updateChildValues(updates) { error, _ in
                let user = CSPDBManager.shared.updateSubscription(user.uid!, till: ti)
                CSPSessionManager.setCurrentUser(user)
                completion()
            }
        } else {
            completion()
        }
    }
    
    // MARK: - Circles
    
    public func createCircle(_ name: String, completion: @escaping (Error?) -> ()) {
        guard let key = databaseReference.child(Table.Circles).childByAutoId().key else {
            completion(nil)
            return
        }
        let circle = [
            DBKey.Author: currentUser!.uid,
            DBKey.Name: name,
            DBKey.Users: [currentUser!.uid: true]
        ] as [String : Any]
        let updates = ["/\(Table.Circles)/\(key)": circle,
                       "/\(Table.Users)/\(currentUser!.uid)/\(DBKey.Circles)/\(key)": true] as [String : Any]
        databaseReference.updateChildValues(updates) { [weak self] error, _ in
            let circle = CSPDBManager.shared.saveCircle(key, circle, CSPSessionManager.currentUser!, [CSPSessionManager.currentUser!])
            CSPSessionManager.setCurrentCircle(circle)
            self?.createList((_: "Inbox", _: nil)) { error in
                self?.addDefaultTask { _ in
                    self?.addDefaultPost { _ in
                        completion(error)
                    }
                }
                self?.addThreeTasks { _ in }
            }
            
            // MARK: - Analytics
            
            
            AnalyticsManager.logEvent(type: .CircleCreated)
            
            // MARK: -
        }
    }
    
    public func updateCircle(_ circle: CSPCircleModel, completion: @escaping (Error?) -> ()) {
//        var users: [String: Any] = [:]
//        circle.membersArray.forEach { users[$0.uid!] = true }
        let updates = [
            "/\(Table.Circles)/\(circle.uid!)/\(DBKey.Name)": circle.name ?? ""
//            ,
//            "/\(Table.Circles)/\(circle.uid!)/\(DBKey.Users)": users
            ] as [String : Any]
        databaseReference.updateChildValues(updates) { [weak self] error, _ in
            self?.getCircle(withUID: circle.uid!) { _ in
                completion(error)
            }
        }
    }
    
    public func deleteCircle(_ circle: CSPCircleModel, completion: @escaping (Error?) -> ()) {
        databaseReference.child(Table.Circles).child(circle.uid!).removeValue { error, _ in
            CSPDBManager.shared.deleteCircle(circle)
            completion(error)
        }
        circle.eventsArray.forEach {
            databaseReference.child(Table.Events).child($0.uid!).removeValue() }
        circle.postsArray.forEach { databaseReference.child(Table.Posts).child($0.uid!).removeValue() }
        circle.tasksArray.forEach { databaseReference.child(Table.Tasks).child($0.uid!).removeValue() }
        circle.listsArray.forEach { databaseReference.child(Table.Lists).child($0.uid!).removeValue() }
        circle.membersArray.forEach { user in
            let updates = ["/\(Table.Users)/\(user.uid!)/\(DBKey.Circles)/\(circle.uid!)": false] as [String: Any]
            databaseReference.updateChildValues(updates)
        }
    }
    
    public func deleteTasksModel(_ tasksModel: CSPTasksModel, completion: @escaping (Error?) -> ()) {
        guard let list = tasksModel.list, let uid = list.uid, let circleUID = list.circleUID else {
            completion(nil)
            return
        }
        
        tasksModel.allTasks.forEach {
            databaseReference.child(Table.Tasks).child($0.uid!).removeValue()
            databaseReference.child(Table.Circles).child(circleUID).child(DBKey.Tasks).child($0.uid!).removeValue()
        }
        databaseReference.child(Table.Circles).child(circleUID).child(DBKey.Lists).child(uid).removeValue()
        databaseReference.child(Table.Lists).child(uid).removeValue { error, _ in
           CSPDBManager.shared.deleteList(list)
           completion(error)
        }
    }
    
    public func getCircles(completion: @escaping ([CSPCircleModel]) -> ()) {
        if isConnected {
            databaseReference.child(Table.Users).child(currentUser!.uid).observeSingleEvent(of: .value, with: { [weak self] snapshot in
                if let circles = (snapshot.value as? NSDictionary)?[DBKey.Circles] as? NSDictionary {
                    let circleUIDs = (circles.allKeys as! [String]).filter { circles[$0] as? Bool == true }
                    CSPDBManager.shared.deleteCircles(except: circleUIDs)
                    self?.getCircles(withUIDs: circleUIDs) { circles in
                        completion(circles)
                    }
                } else {
                    completion([])
                }
            }) { error in
                completion([])
            }
        } else {
            completion([])
        }
    }
    
    private func getCircles(withUIDs uids: [String], completion: @escaping ([CSPCircleModel]) -> ()) {
        if uids.count == 0 {
            completion([])
        } else {
            var circles: [CSPCircleModel] = []
            for uid in uids {
                getCircle(withUID: uid) { circle in
                    if circle != nil {
                        circles.append(circle!)
                        if circles.count == uids.count {
                            completion(circles)
                        }
                    } else {
                        completion([])
                    }
                }
            }
        }
    }
    
    private func getCircle(withUID uid: String, completion: @escaping (CSPCircleModel?) -> ()) {
        databaseReference.child(Table.Circles).child(uid).observeSingleEvent(of: .value, with: { [weak self] snapshot in
            
            let authorUID: String = (snapshot.value as? [String: Any])?[DBKey.Author] as? String ?? ""
            var userUIDs: [String] = []
            var listUIDs: [String] = []
            var postUIDs: [String] = []
            var taskUIDs: [String] = []
            var eventsUIDs: [String] = []
            
            if let users = (snapshot.value as? NSDictionary)?[DBKey.Users] as? NSDictionary {
                userUIDs = (users.allKeys as! [String]).filter { users[$0] as? Bool == true }
            }
            if let lists = (snapshot.value as? NSDictionary)?[DBKey.Lists] as? NSDictionary {
                listUIDs = (lists.allKeys as! [String]).filter { lists[$0] as? Bool == true }
            }
            if let posts = (snapshot.value as? NSDictionary)?[DBKey.Posts] as? NSDictionary {
                postUIDs = (posts.allKeys as! [String]).filter { posts[$0] as? Bool == true }
            }
            if let tasks = (snapshot.value as? NSDictionary)?[DBKey.Tasks] as? NSDictionary {
                taskUIDs = (tasks.allKeys as! [String]).filter { tasks[$0] as? Bool == true }
            }
            
            if let events = (snapshot.value as? NSDictionary)?[DBKey.Events] as? NSDictionary {
                eventsUIDs = (events.allKeys as! [String]).filter { events[$0] as? Bool == true }
            }
            
            self?.getUser(withUID: authorUID) { author in
                self?.getUsers(withUIDs: userUIDs) { users in
                    self?.getLists(withUIDs: listUIDs) { lists in
                        self?.getPosts(withUIDs: postUIDs) { posts in
                            self?.getTasks(withUIDs: taskUIDs) { tasks in
                                self?.getEvents(withUIDs: eventsUIDs) { events in
                                    let circle = CSPDBManager.shared.saveCircle(uid, snapshot.value as! [String: Any], author, users, lists, posts, tasks, events)
                                    completion(circle)
                                }
                                
                            }
                        }
                    }
                }
            }
            
        }) { error in
            completion(nil)
        }
    }
    
    // MARK: - Create Circle Defaults
    
    private func addDefaultTask(completion: @escaping (Error?) -> ()) {
        let task = CSPDBManager.shared.createNewTask()
        task.uid = databaseReference.child(Table.Tasks).childByAutoId().key
        task.list = CSPSessionManager.currentInbox
        task.content = "Sign Up to DolceVita - the must-have organizer for families"
        saveTask(task) { [weak self] _ in
            self?.completeTask(task) { error in
                completion(error)
            }
        }
    }
    
    private func addDefaultPost(completion: @escaping (Error?) -> ()) {
        getTeamUser { [weak self] user in
            let post = CSPDBManager.shared.createNewPost()
            post.author = user
            post.content = "Thanks for signing up to DolceVita! You are on your way to discovering simple and effective organizer for families"
            post.type = CSPPostType.Post.rawValue
            self?.savePost(post, R.image.team_post()) { error in
                completion(error)
            }
        }
    }
    
    private func addThreeTasks(completion: @escaping (Error?) -> ()) {
        let today = CSPDBManager.shared.createNewTask()
        today.content = "Invite more family members and get more things done every day"
        today.date = NSDate.today()
        today.index = 0
        saveTask(today) { _ in }
        
        let tomorrow = CSPDBManager.shared.createNewTask()
        tomorrow.content = "Make post to capture the most important moment of the day"
        tomorrow.date = NSDate.tomorrow()
        tomorrow.index = 1
        saveTask(tomorrow) { _ in }
        
        let nextDay = CSPDBManager.shared.createNewTask()
        nextDay.content = "Add a due date on your tasks and never miss another important matter again"
        nextDay.date = NSDate.nextDayAfterTomorrow()
        nextDay.index = 2
        saveTask(nextDay, completion: completion)
    }
    
    // MARK: - Users
    
    public func getUser(completion: @escaping (CSPUserModel?) -> ()) {
        getUser(withUID: Auth.auth().currentUser!.uid, completion: completion)
    }
    
    public func getUsers(withUIDs uids: [String], completion: @escaping ([CSPUserModel]) -> ()) {
        if uids.count == 0 {
            completion([])
        } else {
            var users: [CSPUserModel] = []
            for uid in uids {
                getUser(withUID: uid) { user in
                    if user != nil {
                        users.append(user!)
                        if users.count == uids.count {
                            completion(users)
                        }
                    } else {
                        completion([])
                    }
                }
            }
        }
    }
    
    public func getUser(withUID uid: String, completion: @escaping (CSPUserModel?) -> ()) {
        databaseReference.child(Table.Users).child(uid).observeSingleEvent(of: .value, with: { snapshot in
            if let response = snapshot.value as? [String: Any] {
                let user = CSPDBManager.shared.saveUser(uid, response)
                completion(user)
            } else {
                completion(nil)
            }
        }) { error in
            completion(nil)
        }
    }
    public func getUser(phoneNumber: String, completion: @escaping (CSPUserModel?) -> ()) {
        databaseReference.child(Table.Users).queryOrdered(byChild: DBKey.Phone).queryEqual(toValue: phoneNumber).observeSingleEvent(of: .value, with: { snapshot in
            if let response = snapshot.value as? [String: Any], let key = response.keys.first, let data = response[key] as? [String: Any] {
                let user = CSPDBManager.shared.saveUser(key, data)
                completion(user)
            } else {
                completion(nil)
            }
        }) { error in
            completion(nil)
        }
    }
    
    public func getUser(withPhoneNumber phone: String, completion: @escaping (CSPUserModel?) -> ()) {
        databaseReference.child(Table.Users).queryOrdered(byChild: DBKey.Phone).queryEqual(toValue: phone).observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.childrenCount > 0 {
                completion(snapshot.children.map { CSPDBManager.shared.saveUser(($0 as! DataSnapshot).key,
                                                                                ($0 as! DataSnapshot).value as! [String: Any]) }.first)
            } else {
                completion(nil)
            }
        })
    }
    
    private func getTeamUser(completion: @escaping (CSPUserModel?) -> ()) {
        getUser(withUID: DBKey.AppTeamId) { user in
            completion(user)
        }
    }
    
    // MARK: - Lists
    
    public func createList(_ data: (name: String, color: CSPColorModel?), completion: @escaping (Error?) -> ()) {
        guard let key = databaseReference.child(Table.Lists).childByAutoId().key else {
            completion(nil)
            return
        }
        var list = [
            DBKey.Name: data.name,
            DBKey.Circle: CSPSessionManager.currentCircle!.uid!,
            DBKey.CreatedAt: Date().timeIntervalSince1970
            ] as [String: Any]
        if let color = data.color?.hex {
            list[DBKey.Color] = color
        }
        let updates = ["/\(Table.Lists)/\(key)": list,
                       "/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Lists)/\(key)": true] as [String : Any]
        databaseReference.updateChildValues(updates) { error, _ in
            let list = CSPDBManager.shared.saveList(key, list)
            CSPDBManager.shared.addList(list)
            completion(error)
        }
    }
    
    private func getLists(withUIDs uids: [String], completion: @escaping ([CSPListModel]) -> ()) {
        if uids.count == 0 {
            completion([])
        } else {
            var lists: [CSPListModel] = []
            for uid in uids {
                getList(withUID: uid) { list in
                    if list != nil {
                        lists.append(list!)
                        if lists.count == uids.count {
                            completion(lists)
                        }
                    } else {
                        completion([])
                    }
                }
            }
        }
    }
    
    public func getList(withUID uid: String, completion: @escaping (CSPListModel?) -> ()) {
        databaseReference.child(Table.Lists).child(uid).observeSingleEvent(of: .value, with: { snapshot in
            if let response = snapshot.value as? [String: Any] {
                let list = CSPDBManager.shared.saveList(uid, response)
                completion(list)
            } else {
                completion(nil)
            }
        }) { error in
            completion(nil)
        }
    }
    
    // MARK: - Posts
    
    private func getPosts(withUIDs uids: [String], completion: @escaping ([CSPPostModel]) -> ()) {
        if uids.count == 0 {
            completion([])
        } else {
            var posts: [CSPPostModel] = []
            for uid in uids {
                getPost(withUID: uid) { post in
                    if post != nil {
                        posts.append(post!)
                        if posts.count == uids.count {
                            completion(posts)
                        }
                    } else {
                        completion([])
                    }
                }
            }
        }
    }
    
   
    public func getPost(withUID uid: String, completion: @escaping (CSPPostModel?) -> ()) {
        databaseReference.child(Table.Posts).child(uid).observeSingleEvent(of: .value, with: { [weak self] snapshot in
            if let response = snapshot.value as? [String: Any] {
                if let authorUID = response[DBKey.Author] as? String {
                    self?.getUser(withUID: authorUID) { user in
                        if user != nil {
                            if let listUID = response[DBKey.List] as? String {
                                self?.getList(withUID: listUID) { list in
                                    if list != nil {
                                        let post = CSPDBManager.shared.savePost(uid, response, user!, list!)
                                        completion(post)
                                    } else {
                                        completion(nil)
                                    }
                                }
                            } else {
                                let post = CSPDBManager.shared.savePost(uid, response, user!)
                                completion(post)
                            }
                        } else {
                            completion(nil)
                        }
                    }
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }) { error in
            completion(nil)
        }
    }
    
    public func savePost(_ postModel: CSPPostModel, _ image: UIImage?, completion: @escaping (Error?) -> ()) {
        guard let key = postModel.uid ?? databaseReference.child(Table.Posts).childByAutoId().key else {
            completion(nil)
            return
        }
        var post = [
            DBKey.Author: postModel.author?.uid ?? currentUser!.uid,
            DBKey.CreatedAt: Date().timeIntervalSince1970,
            DBKey.Circle: CSPSessionManager.currentCircle!.uid!,
            DBKey.PostType: postModel.type
            ] as [String: Any]
        let savePost = {
            let updates = ["/\(Table.Posts)/\(key)": post,
                           "/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Posts)/\(key)": true] as [String : Any]
            self.databaseReference.updateChildValues(updates) { error, _ in
                let post = CSPDBManager.shared.savePost(key, post, postModel.author ?? CSPSessionManager.currentUser!, postModel.list)
                CSPDBManager.shared.addPost(post)
                completion(error)
            }
        }
        if let content = postModel.content {
            post[DBKey.Content] = content
        }
        if let listUID = postModel.list?.uid {
            post[DBKey.List] = listUID
        }
        if let image = image {
            saveImage(image) { url, _ in
                if url != nil {
                    post[DBKey.PhotoURL] = url!.absoluteString
                    post[DBKey.PhotoAspectRatio] = image.size.height / image.size.width
                }
                savePost()
            }
        } else {
            savePost()
        }
    }

        public func deleteComment(_ comment: CSPCommentModel, completion: @escaping (Error?) -> ()) {
            databaseReference.child(Table.Comments).child(comment.uid!).removeValue { error, _ in
    
                completion(error)
            }
            let updates = ["/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Comments)/\(comment.uid!)": false] as [String: Any]
            databaseReference.updateChildValues(updates) { _, _ in }
        }
    
    public func deletePost(_ post: CSPPostModel, completion: @escaping (Error?) -> ()) {
        databaseReference.child(Table.Posts).child(post.uid
        ?? "").removeValue { error, _ in
            CSPDBManager.shared.deletePost(post)
            completion(error)
            // MARK: - Analytics
            AnalyticsManager.logEvent(type: .PostDeleted)

            // MARK: -
        }
        let updates = ["/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Posts)/\(post.uid!)": false] as [String: Any]
        databaseReference.updateChildValues(updates) { _, _ in }
    }
    // MARK: - Events
    
    public func saveEvent(_ eventModel: CSPEventModel, completion: @escaping (Error?) -> ()) {
        guard let key = eventModel.uid ?? databaseReference.child(Table.Events).childByAutoId().key else {
            completion(nil)
            return
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        var event = [
            DBKey.Author: currentUser!.uid,
            DBKey.Content: eventModel.content!,
            DBKey.CreatedAt: Date().timeIntervalSince1970,
            DBKey.Circle: CSPSessionManager.currentCircle!.uid!,
            DBKey.AllDay: eventModel.allDay,
            DBKey.Reminder: eventModel.reminder,
            DBKey.uid: key,
            DBKey.Location : eventModel.location
            ] as [String: Any]
        
       
        if let users = eventModel.members?.array as? [CSPUserModel] {
            event[DBKey.Users] = users.map { $0.uid }
        }
        if eventModel.date != nil {
            event[DBKey.Date] = (eventModel.date! as Date).timeIntervalSince1970
        }
        
        if eventModel.start != nil {
            event[DBKey.Start] = dateFormatter.string(from: eventModel.date! as Date)
        }
        if eventModel.startTime != nil {
            event[DBKey.StartTime] = (eventModel.startTime! as Date).timeIntervalSince1970
        }
        if eventModel.endsTime != nil {
            event[DBKey.EndsTime] = (eventModel.endsTime! as Date).timeIntervalSince1970
        }
        if eventModel.allDay != nil {
            event[DBKey.AllDay] = eventModel.allDay as Bool
        }
        if eventModel.reminder != nil {
            event[DBKey.Reminder] = eventModel.reminder as Bool
        }
        let updates = ["/\(Table.Events)/\(key)": event,
                       
                       "/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Events)/\(key)": true] as [String: Any]
        self.databaseReference.updateChildValues(updates) { error, _ in
            let event = CSPDBManager.shared.saveEvent(key, event, CSPSessionManager.currentUser!, eventModel.members?.array as? [CSPUserModel] ?? [])
                     CSPDBManager.shared.addEvent(event)
                     completion(error)
        }
    }
    
    public func getEvent(withUID uid: String, completion: @escaping (CSPEventModel?) -> ()) {
        databaseReference.child(Table.Events).child(uid).observeSingleEvent(of: .value, with: { [weak self] snapshot in
            if let response = snapshot.value as? [String: Any] {
                if let authorUID = response[DBKey.Author] as? String {
                    self?.getUser(withUID: authorUID) { user in
                        if user != nil {
                            let memberUIDs: [String] = response[DBKey.Users] as? [String] ?? []
                            self?.getUsers(withUIDs: memberUIDs) { users in
                                let event = CSPDBManager.shared.saveEvent(uid, response, user!, users)
                                completion(event)
                            }
                        } else {
                            completion(nil)
                        }
                    }
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }) { error in
            completion(nil)
        }
    }
    
    public func downloadComments(for postId: String, onComplete: (([Any])-> ())?) {
        let database = Database.database().reference()
        database
            .child("comments")
            .queryOrdered(byChild: "postId")
            .queryEqual(toValue: postId)
            .observeSingleEvent(of: .value) { response in
            if let value = response.value as? [String:Any] {
                onComplete?(Array(value.values))
            }
        }
    }
    
    private func getEvents(withUIDs uids: [String], completion: @escaping ([CSPEventModel]) -> ()) {
        if uids.count == 0 {
            completion([])
        } else {
            var events: [CSPEventModel] = []
            for uid in uids {
                getEvent(withUID: uid) { event in
                    if event != nil {
                        events.append(event!)
                        if events.count == uids.count {
                            completion(events)
                        }
                    } else {
                        completion([])
                    }
                }
            }
        }
    }
    
    public func deleteEvent(_ event: CSPEventModel, completion: @escaping (Error?) -> ()) {
        databaseReference.child(Table.Events).child(event.uid ?? "").removeValue { error, _ in
            CSPDBManager.shared.deleteEvent(event)
            completion(error)
        }
        let updates = ["/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Events)/\(event.uid!)": false] as [String: Any]
        databaseReference.updateChildValues(updates) { _, _ in }
    }
    

    // MARK: - Tasks
    
    private func getTasks(withUIDs uids: [String], completion: @escaping ([CSPTaskModel]) -> ()) {
        if uids.count == 0 {
            completion([])
        } else {
            var tasks: [CSPTaskModel] = []
            for uid in uids {
                getTask(withUID: uid) { task in
                    if task != nil {
                        tasks.append(task!)
                        if tasks.count == uids.count {
                            completion(tasks)
                        }
                    } else {
                        completion([])
                    }
                }
            }
        }
    }
    
    public func getTask(withUID uid: String, completion: @escaping (CSPTaskModel?) -> ()) {
        databaseReference.child(Table.Tasks).child(uid).observeSingleEvent(of: .value, with: { [weak self] snapshot in
            if let response = snapshot.value as? [String: Any] {
                if let authorUID = response[DBKey.Author] as? String {
                    self?.getUser(withUID: authorUID) { user in
                        if user != nil {
                            if let listUID = response[DBKey.List] as? String {
                                self?.getList(withUID: listUID) { list in
                                    if list != nil {
                                        let memberUIDs: [String] = response[DBKey.Users] as? [String] ?? []
                                        self?.getUsers(withUIDs: memberUIDs) { users in
                                            let task = CSPDBManager.shared.saveTask(uid, response, user!, users, list!)
                                            completion(task)
                                        }
                                    } else {
                                        completion(nil)
                                    }
                                }
                            } else {
                                completion(nil)
                            }
                        } else {
                            completion(nil)
                        }
                    }
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }) { error in
            completion(nil)
        }
    }
    
    public func saveTask(_ taskModel: CSPTaskModel, completion: @escaping (Error?) -> ()) {
        guard let key = taskModel.uid ?? databaseReference.child(Table.Tasks).childByAutoId().key else {
            completion(nil)
            return
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        var task = [
            DBKey.Author: taskModel.author?.uid ?? currentUser!.uid,
            DBKey.Content: taskModel.content!,
            DBKey.CreatedAt: Date().timeIntervalSince1970,
            DBKey.Circle: CSPSessionManager.currentCircle!.uid!,
            DBKey.Index: taskModel.index
            ] as [String: Any]
        if let users = taskModel.members?.array as? [CSPUserModel] {
            task[DBKey.Users] = users.map { $0.uid }
        }
        if taskModel.date != nil {
            task[DBKey.Date] = (taskModel.date! as Date).timeIntervalSince1970
        }
        
        if taskModel.priority != nil {
            task[DBKey.Priority] = taskModel.priority
        }
        
        if taskModel.start != nil {
            task[DBKey.Start] = dateFormatter.string(from: taskModel.date! as Date)
        }
        var taskList: CSPListModel?
        if taskModel.list != nil {
            taskList = taskModel.list!
            task[DBKey.List] = taskModel.list!.uid ?? ""
        } else if let list = CSPSessionManager.currentInbox {
            taskList = list
            task[DBKey.List] = list.uid ?? ""
        }
        if taskList != nil {
            getList(withUID: taskList!.uid!) { [weak self] list in
                if taskModel.uid == nil {
                    task[DBKey.Index] = list?.tasksCount ?? 0
                }
                let updates = ["/\(Table.Tasks)/\(key)": task,
                               "/\(Table.Lists)/\(taskList!.uid!)/\(DBKey.Tasks)/\(key)": true,
                               "/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Tasks)/\(key)": true] as [String: Any]
                self?.databaseReference.updateChildValues(updates) { error, _ in
                    let task = CSPDBManager.shared.saveTask(key, task, CSPSessionManager.currentUser!, taskModel.members?.array as? [CSPUserModel] ?? [], taskList!)
                    CSPDBManager.shared.addTask(task)
                    completion(error)
                }
            }
        } else {
            completion(nil)
        }
    }
    

    public func saveComment(content:String, uid:String, authorImage: String, completion: @escaping (Error?) -> ()) {
        guard let key = databaseReference.child(Table.Comments).childByAutoId().key else {
            completion(nil)
            return
        }
        
        let circle = CSPSessionManager.currentCircle
        let comment = [
            DBKey.CircleId: circle?.uid,
            DBKey.Author: circle?.author?.fullName,
            DBKey.Content: content,
            DBKey.CreatedAt: Date().timeIntervalSince1970,
            DBKey.PostId: uid,
            DBKey.AuthorImage : authorImage,
            DBKey.uid: key
        ] as! [String: Any]
        
        let updates = ["/\(Table.Comments)/\(key)": comment,
                       "/\(DBKey.Author)/\(currentUser!.uid)/\(DBKey.uid)/\(key)": true] as [String : Any]
        databaseReference.updateChildValues(updates) { error, _ in
            //let comment = CSPDBManager.shared.saveComment(uid, comment, self.currentUser!.uid)
            completion(error)
            
        }
    }

    public func deleteTask(_ task: CSPTaskModel, completion: @escaping (Error?) -> ()) {
        databaseReference.child(Table.Tasks).child(task.uid!).removeValue { error, _ in
            CSPDBManager.shared.deleteTask(task)
            completion(error)
        }
        let updates = ["/\(Table.Circles)/\(CSPSessionManager.currentCircle!.uid!)/\(DBKey.Tasks)/\(task.uid!)": false] as [String: Any]
        databaseReference.updateChildValues(updates) { _, _ in }
    }
    
    public func completeTask(_ task: CSPTaskModel, completion: @escaping (Error?) -> ()) {
        let post = CSPDBManager.shared.createNewPost()
        post.content = task.content
        post.list = task.list
        post.type = CSPPostType.CompletedTask.rawValue
        savePost(post, nil) { [weak self] _ in
            self?.deleteTask(task, completion: completion)
        }
    }
    
    public func saveIndexesFor(firstTask: CSPTaskModel, secondTask: CSPTaskModel) {
        guard let firstUID = firstTask.uid, let secondUID = secondTask.uid else { return }
        databaseReference.child(Table.Tasks).child(firstUID).child(DBKey.Index).setValue(firstTask.index)
        databaseReference.child(Table.Tasks).child(secondUID).child(DBKey.Index).setValue(secondTask.index)
    }
    
    public func commitChanges(_ tasks: [CSPTaskModel], completion: @escaping (Error?) -> ()) {
        var updates: [String: Any] = [:]
        tasks.forEach { task in
            updates["/\(Table.Tasks)/\(task.uid!)/\(DBKey.Index)"] = task.index
        }
        databaseReference.updateChildValues(updates) { error, _ in
            completion(error)
        }
    }
    

    // MARK: - Colors
    
    public func getColors(completion: @escaping ([CSPColorModel]) -> ()) {
        databaseReference.child(Table.Colors).observeSingleEvent(of: .value, with: { snapshot in
            if let response = snapshot.value as? NSDictionary {
                completion(response.allKeys.map { CSPColorModel($0 as! String, response[$0] as! String) })
            } else {
                completion([])
            }
        }) { error in
            completion([])
        }
    }
    
    // MARK: - Ivites
    
    public func invite(phone: String, toCircle uid: String, completion: @escaping (CSPUserModel?, Error?) -> ()) {
        getUser(withPhoneNumber: phone) { [weak self] user in
            if let key = self?.databaseReference.child(Table.Invites).childByAutoId().key {
                let updates = ["/\(Table.Invites)/\(key)": [DBKey.Phone: phone, DBKey.Circle: uid],
                               "/\(Table.Circles)/\(uid)/\(DBKey.Invites)/\(key)": true] as [String : Any]
                
                let saveChanges: (CSPUserModel?) -> () = { user in
                    self?.databaseReference.updateChildValues(updates) { error, _ in
                       completion(user, error)
                    }
                }
                if let userUID = user?.uid {
                    self?.addUser(userUID, toCircle: uid) { _ in
                        saveChanges(user)
                    }
                } else {
                    saveChanges(nil)
                }
            }
        }
    }
    
    public func invite(user: CSPUserModel, toCircle uid: String, completion: @escaping (CSPUserModel?, Error?) -> ()) {
        if let key = databaseReference.child(Table.Invites).childByAutoId().key, let userUID = user.uid, let phone = user.phone {
            let updates = ["/\(Table.Invites)/\(key)": [DBKey.Phone: phone, DBKey.Circle: uid],
                           "/\(Table.Circles)/\(uid)/\(DBKey.Invites)/\(key)": true] as [String : Any]

            addUser(userUID, toCircle: uid) { [weak self] _ in
                self?.databaseReference.updateChildValues(updates) { error, _ in
                    completion(user, error)
                }
            }
        }
    }
    
    public func removeInvite(user: CSPUserModel, toCircle uid: String, completion: @escaping (Error?) -> ()) {
        guard let userUID = user.uid, let phoneNumber = user.phone else {
            completion(nil)
            return
        }
        databaseReference.child(Table.Invites).queryOrdered(byChild: DBKey.Phone).queryEqual(toValue: phoneNumber).observeSingleEvent(of: .value, with: { snapshot in
            if let response = snapshot.value as? [String: Any] {
                response.keys.forEach { inviteUID in
                    self.databaseReference.child(Table.Invites).child(inviteUID).removeValue()
                    self.databaseReference.child(Table.Circles).child(uid).child(Table.Invites).child(inviteUID).removeValue()
                }
            } else {
                completion(nil)
            }
        }) { error in
            completion(nil)
        }
        databaseReference.child(Table.Circles).child(uid).child(Table.Users).child(userUID).removeValue()
        databaseReference.child(Table.Users).child(userUID).child(Table.Circles).child(uid).removeValue { error, _ in
            completion(error)
        }
    }
    
    public func getInvites(forCircle uid: String, completion: @escaping ([CSPInviteModel]) -> ()) {
        databaseReference.child(Table.Invites).queryOrdered(byChild: DBKey.Circle).queryEqual(toValue: uid).observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.childrenCount > 0 {
                completion(snapshot.children.map { CSPInviteModel(($0 as! DataSnapshot).key,
                                                                  ($0 as! DataSnapshot).value as! NSDictionary) })
            } else {
                completion([])
            }
        })
    }
    
    public func getInvites(forPhone phone: String, completion: @escaping ([CSPInviteModel]) -> ()) {
        databaseReference.child(Table.Invites).queryOrdered(byChild: DBKey.Phone).queryEqual(toValue: phone).observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.childrenCount > 0 {
                completion(snapshot.children.map { CSPInviteModel(($0 as! DataSnapshot).key,
                                                                  ($0 as! DataSnapshot).value as! NSDictionary) })
            } else {
                completion([])
            }
        })
    }
    
    public func applyInvites(forPhone phone: String, completion: @escaping () -> ()) {
        getInvites(forPhone: phone) { [weak self] invites in
            if invites.count > 0 {
                var index = 0
                invites.forEach {
                    self?.addUser(self?.currentUser?.uid ?? "", toCircle: $0.circleUID) { _ in
                        index += 1
                        if index == invites.count {
                            completion()
                        }
                    }
                }
            } else {
                completion()
            }
        }
    }
    
    public func addUser(_ userUID: String, toCircle circleUID: String, completion: @escaping (Error?) -> ()) {
        guard let key = databaseReference.child(Table.Circles).child(circleUID).key else {
            completion(nil)
            return
        }
        let updates = ["/\(Table.Circles)/\(key)/\(DBKey.Users)/\(userUID)": true,
                       "/\(Table.Users)/\(userUID)/\(DBKey.Circles)/\(key)": true] as [String : Any]
        databaseReference.updateChildValues(updates) { error, _ in
            completion(error)
        }
    }
    
    // MARK: - Internet Connection Test
    
    private func isInternetConnectionAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    // MARK: - IAP
    
    struct IAP {
        #if DEBUG
            static let VerifyReceipt = "https://sandbox.itunes.apple.com/verifyReceipt"
        #else
            static let VerifyReceipt = "https://buy.itunes.apple.com/verifyReceipt"
        #endif
        static let Secret = "5a0ba36bd38c42eda3b7d06420cad408"
    }
    
    public func validateSubscription(completion: @escaping () -> ()) {
//        if let receiptURL = Bundle.main.appStoreReceiptURL {
//            do {
//                let receiptData = try Data(contentsOf: receiptURL)
//                let requestContents = ["receipt-data": receiptData.base64EncodedString(),
//                                       "password": IAP.Secret]
//                
//                AF.request(IAP.VerifyReceipt, method: .post, parameters: requestContents, encoding: JSONEncoding.default, headers: nil).validate().responseObject { [weak self] (response: AFDataResponse<CSPIAPValidationModel>) in
//                    if let subscriptionExpirationDate = response.value?.latestReceiptInfo?.last?.expiresDate {
//                        self?.updateSubscription(till: subscriptionExpirationDate) {  }
//                    }
//                    completion()
//                }
//            } catch {
//                completion()
//                print(error.localizedDescription)
//            }
//        }
    }
    
}

//
//  CSPInvitePresenter.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPInvitePresenter: NSObject {
    
    @IBOutlet weak var constraintTopInviteButton: NSLayoutConstraint!
    
    func setupConstraints() {
        constraintTopInviteButton.constant = DDV(36, 36, 36, 66)
    }
    
}

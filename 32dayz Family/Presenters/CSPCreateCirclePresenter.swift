//
//  CSPCreateCirclePresenter.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCreateCirclePresenter: NSObject {
    
    @IBOutlet weak var constraintCenterFamilyCircleNameTextField: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomCreateAndContinueButton: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightErrorStackView: NSLayoutConstraint!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var errorStackView: UIStackView!
    
    func setupConstraints() {
        constraintBottomCreateAndContinueButton.constant = DDV(231, 231, 231, 312)
        constraintCenterFamilyCircleNameTextField.constant = DDV(-100, -100, -100, -160)
    }
    
    func showError(_ view: UIView) {
        constraintHeightErrorStackView.constant = 30
        infoLabel.alpha = 0
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.errorStackView.alpha = 1
        }
    }
    
    func hideError(_ view: UIView) {
        constraintHeightErrorStackView.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            view.layoutIfNeeded()
            self.errorStackView.alpha = 0
        }) { _ in
            self.infoLabel.alpha = 1
        }
    }
    
}

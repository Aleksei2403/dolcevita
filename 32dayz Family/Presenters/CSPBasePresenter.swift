//
//  CSPBasePresenter.swift
//  FamilyOrganizer
//
//  Created by User on 03.11.16.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import UIKit

class CSPBasePresenter: NSObject {
    
    static let device = CSPDevice()
    static let defaultDevice = CSPDevice.iPhone6
    static let ratio: CGFloat = device.screenSizeInPoints.width / CGFloat(defaultDevice.screenSizeInPoints.width)
    
    class func updateAllSubviews(ofView view: UIView) {
        
        if ratio != 1 {
            if let label = view as? UILabel {
                if !label.isResizingDisable {
                    label.font = label.font.withSize(label.font.pointSize * ratio)
                }
            }
            
            if let textField = view as? UITextField {
                if textField.font != nil {
                    textField.font = textField.font!.withSize(textField.font!.pointSize * ratio)
                }
            }
            
            if let button = view as? UIButton {
                if button.titleLabel?.font != nil {
                    button.titleLabel!.font = button.titleLabel!.font!
                }
            }
            
            for constraint in view.constraints {
                if !constraint.isResizingDisable {
                    constraint.constant *= ratio
                }
            }
            
            let subviews = view.subviews
            if subviews.count == 0 {
                return
            }
            for subview in subviews {
                if !subview.isResizingDisable {
                    updateAllSubviews(ofView: subview)
                }
            }
        }
    }
    
    class func convertToActualDeviceSize(_ value: CGFloat) -> CGFloat {
        return value * ratio
    }
    
    class func getDeviceDependValue(forIphone5 iphone5: CGFloat, iphone6: CGFloat, iphone6Plus: CGFloat) -> CGFloat {
        switch device {
        case .iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE:
            return iphone5
        case .iPhone6, .iPhone6s, .iPhone7:
            return iphone6
        case .iPhone6Plus, .iPhone6sPlus, .iPhone7Plus:
            return iphone6Plus
        case .simulator(let model):
            switch model {
            case .iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE:
                return iphone5
            case .iPhone6, .iPhone6s, .iPhone7:
                return iphone6
            case .iPhone6Plus, .iPhone6sPlus, .iPhone7Plus:
                return iphone6Plus
            default:
                return 0
            }
        case .unknown( _):
            return 0
        }
    }
}

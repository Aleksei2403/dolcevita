//
//  CSPPhoneFailurePresenter.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPPhoneFailurePresenter: NSObject {
    
    @IBOutlet weak var constraintBottomInfoLabel: NSLayoutConstraint!
    
    func setupConstraints() {
        constraintBottomInfoLabel.constant = DDV(150, 150, 240, 240)
    }
    
}

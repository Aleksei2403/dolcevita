//
//  CSPCreateAccountPresenter.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCreateAccountPresenter: NSObject {
    
    @IBOutlet weak var constraintBottomScrollView: NSLayoutConstraint!
    
    @IBOutlet weak var constraintFirstNameErrorHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintLastNameErrorHeight: NSLayoutConstraint!
    
    @IBOutlet weak var firstNameErrorStackView: UIStackView!
    @IBOutlet weak var lastNameErrorStackView: UIStackView!
    
    func showKeyboard(_ height: CGFloat) {
        constraintBottomScrollView.constant = height
    }
    
    func hideKeyboard() {
        constraintBottomScrollView.constant = 0
    }
    
    func showFirstNameError(_ view: UIView) {
        constraintFirstNameErrorHeight.constant = 30
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.firstNameErrorStackView.alpha = 1
        }
    }
    
    func hideFirstNameError(_ view: UIView) {
        constraintFirstNameErrorHeight.constant = 0
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.firstNameErrorStackView.alpha = 0
        }
    }
    
    func showLastNameError(_ view: UIView) {
        constraintLastNameErrorHeight.constant = 30
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.lastNameErrorStackView.alpha = 1
        }
    }
    
    func hideLastNameError(_ view: UIView) {
        constraintLastNameErrorHeight.constant = 0
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.lastNameErrorStackView.alpha = 0
        }
    }
    
}

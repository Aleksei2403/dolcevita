//
//  CSPEnterPhonePresenter.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPEnterPhonePresenter: NSObject {
    
    @IBOutlet weak var constraintTopErrorStackView: NSLayoutConstraint!
    @IBOutlet weak var constraintTopSendCodeButton: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomSendCodeButton: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomInfoLabel: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightErrorStackView: NSLayoutConstraint!
    
    @IBOutlet weak var errorStackView: UIStackView!
    
    public func setupConstraints() {
        constraintTopErrorStackView.constant = DDV(5, 5, 10, 10)
        constraintTopSendCodeButton.constant = DDV(39, 39, 49, 79)
        constraintBottomInfoLabel.constant = DDV(232, 232, 247, 247)
        constraintBottomSendCodeButton.constant = DDV(282, 282, 312, 332)
    }
    
    func showError(_ view: UIView) {
        constraintHeightErrorStackView.constant = 30
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.errorStackView.alpha = 1
        }
    }
    
    func hideError(_ view: UIView) {
        constraintHeightErrorStackView.constant = 0
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.errorStackView.alpha = 0
        }
    }
    
}

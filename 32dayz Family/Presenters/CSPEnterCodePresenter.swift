//
//  CSPEnterCodePresenter.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPEnterCodePresenter: NSObject {
    
    @IBOutlet weak var constraintBottomContinueButton: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomCodeTextField: NSLayoutConstraint!
    @IBOutlet weak var constraintTopInfoLabel: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomResendCodeButton: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightErrorStackView: NSLayoutConstraint!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var errorStackView: UIStackView!
    
    func setupConstraints() {
        constraintBottomContinueButton.constant = DDV(282, 282, 312, 332)
        constraintBottomCodeTextField.constant = DDV(421, 421, 461, 501)
        constraintTopInfoLabel.constant = DDV(5, 5, 10, 10)
        constraintBottomResendCodeButton.constant = DDV(230, 230, 230, 260)
    }
    
    func showError(_ view: UIView) {
        constraintHeightErrorStackView.constant = 30
        infoLabel.alpha = 0
        UIView.animate(withDuration: 0.5) { 
            view.layoutIfNeeded()
            self.errorStackView.alpha = 1
        }
    }
    
    func hideError(_ view: UIView) {
        constraintHeightErrorStackView.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            view.layoutIfNeeded()
            self.errorStackView.alpha = 0
        }) { _ in
            self.infoLabel.alpha = 1
        }
    }
    
}

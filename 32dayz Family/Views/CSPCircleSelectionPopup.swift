//
//  CSPCircleSelectionPopup.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/18/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCircleSelectionPopup: UIView {

    class func instanceFromNib() -> CSPCircleSelectionPopup {
        let view = R.nib.cspCircleSelectionPopup().instantiate(withOwner: nil, options: nil).first as! CSPCircleSelectionPopup
        view.frame = UIScreen.main.bounds
        return view
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var circles: [CSPCircleModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var currentCircle: CSPCircleModel?
    
    var onCircleSelection: ((CSPCircleModel) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(R.nib.cspCircleSelectionCell)
    }
    
    @IBAction func didTapBackground(_ sender: UIButton?) {
        removeFromSuperview()
    }
    
}

extension CSPCircleSelectionPopup: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return circles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspCircleSelectionCell_id.identifier, for: indexPath) as! CSPCircleSelectionCell
        let circle = circles[indexPath.row]
        cell.config(forCircle: circle, isCurrent: circle.uid == currentCircle?.uid)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onCircleSelection?(circles[indexPath.row])
        didTapBackground(nil)
    }
    
}

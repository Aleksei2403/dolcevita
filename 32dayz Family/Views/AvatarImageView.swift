//
//  AvatarImageView.swift
//  32dayz Family
//
//  Created by Valik Kuchinsky on 3/12/20.
//  Copyright © 2020 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

extension Notification.Name {
    static let avatarChanged = Notification.Name(rawValue: "AvatarWasChanged")
}

class AvatarImageView: UIImageView {

    fileprivate var user: CSPUserModel?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func set(user: CSPUserModel) {
        self.user = user
        updateImage()
        NotificationCenter.default.addObserver(self, selector: #selector(AvatarImageView.updateImage), name: Notification.Name.avatarChanged, object: nil)
    }
    
    @objc func updateImage() {
        if let photoURL = user?.photoURL {
           Nuke.loadImage(with: URL(string: photoURL)!, into: self)
        } else {
            image = nil
        }
    }

}

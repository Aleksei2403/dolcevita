//
//  CSPSettingsHeaderView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPSettingsHeaderView: UIView {

    @IBOutlet weak var mainLabel: UILabel!
    
    class func instanceFromNib() -> CSPSettingsHeaderView {
        return R.nib.cspSettingsHeaderView().instantiate(withOwner: nil, options: nil).first as! CSPSettingsHeaderView
    }
    
    public func config(withTitle title: String?) {
        mainLabel.text = title?.uppercased()
    }

}

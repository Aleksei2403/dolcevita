//
//  CSPTaskEditorView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/31/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

@IBDesignable
class CSPTaskEditorView: UIView {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var views: [UIView]!
    @IBOutlet weak var addMembersLabel: UILabel!
    @IBOutlet weak var addDueDateLabel: UILabel!
    @IBOutlet weak var chooseListLabel: UILabel!
    @IBOutlet weak var choosePriorityLabel: UILabel!
    
    let datePickerView = CSPDatePickerView.instanceFromNib()
    
    var task: CSPTaskModel!
    var onAddMembersClick: (() -> ())?
    var onAddDueDateClick: (() -> ())?
    var onChooseListClick: (() -> ())?
    var onChoosePriorityClick: (() -> ())?
    
    lazy var df: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(R.nib.cspTaskMemberCell)
        datePickerView.onDatePick = { [weak self] date in
            if let task = self?.task {
                task.date = date as NSDate
                self?.setup(withTask: task)
            }
            
            // MARK: - Analytics
            
            AnalyticsManager.logEvent(type: .TaskDueDateAdded)
            // MARK: -
            
        }
    }
    
    func setup(withTask task: CSPTaskModel) {
        self.task = task
        addMembersLabel.isHidden = taskMembersCount > 0
        collectionView.isHidden = taskMembersCount == 0
        collectionView.reloadData()
        addDueDateLabel.text = task.date == nil ? "Add Due Date" : df.string(from: task.date! as Date)
        chooseListLabel.text = task.list?.name ?? "Choose List"
        choosePriorityLabel.text = task.priority ?? "Choose Priority"
    }
    
    @IBAction func highlight(_ sender: UIButton) {
        guard let view = (views.filter { $0.tag == sender.tag }).first else {
            return
        }
        view.backgroundColor = UIColor(0xf9f9f9)
    }
    
    @IBAction func unhighlight(_ sender: UIButton) {
        guard let view = (views.filter { $0.tag == sender.tag }).first else {
            return
        }
        view.backgroundColor = UIColor(0xfcfcfc)
    }
    
    @IBAction func clickedAddMembersButton(_ sender: UIButton) {
        unhighlight(sender)
        onAddMembersClick?()
    }
    
    @IBAction func clickedAddDueDateButton(_ sender: UIButton) {
        unhighlight(sender)
        onAddDueDateClick?()
    }
    
    @IBAction func clickedChoosePriorityButton(_ sender: UIButton) {
        unhighlight(sender)
        onChoosePriorityClick?()
        
    }
    @IBAction func clickedChooseListButton(_ sender: UIButton) {
        unhighlight(sender)
        onChooseListClick?()
    }
    
}

extension CSPTaskEditorView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    private var maxItems: Int {
        get {
            return Int(collectionView.frame.width / (47.5)) //pm cell width (40) + spacing (7.5)
        }
    }
    
    fileprivate var taskMembersCount: Int {
        get {
            return task.members?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return taskMembersCount > maxItems ? maxItems : taskMembersCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspTaskMemberCell_id.identifier, for: indexPath) as! CSPTaskMemberCell
        if indexPath.item == maxItems - 1 && taskMembersCount > maxItems {
            cell.config(forUsers: taskMembersCount - maxItems + 1)
        } else {
            cell.config(forUser: (task.members!.array as! [CSPUserModel])[indexPath.item])
        }
        return cell
    }
    
}

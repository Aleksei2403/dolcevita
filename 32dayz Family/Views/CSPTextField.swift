//
//  CSPTextField.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        tintColor = .white
        if placeholder != nil {
            let placeholderString = NSAttributedString(string: placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor(0xace0f5)])
            attributedPlaceholder = placeholderString
        }
    }
    
}

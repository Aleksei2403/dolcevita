//
//  CSPDatePickerView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/2/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPDatePickerView: UIView {

    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var bgButton: UIButton!
    
    
    var onDatePick: ((Date) -> ())?
    
    class func instanceFromNib() -> CSPDatePickerView {
        return R.nib.cspDatePickerView().instantiate(withOwner: nil, options: nil).first as! CSPDatePickerView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = UIScreen.main.bounds
        bgButton.alpha = 0
        datePickerView.transform = CGAffineTransform(translationX: 0, y: datePickerView.frame.height)
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.backgroundColor = .white
        } else {
            
        }
        datePicker.backgroundColor = .white
        
    }
    
    public func show() {
        if datePickerView.transform != .identity {
            UIApplication.shared.keyWindow?.addSubview(self)
            UIView.animate(withDuration: 0.3) {
                self.bgButton.alpha = 1
                self.datePickerView.transform = .identity
            }
        }
    }
    
    private func hide() {
        if datePickerView.transform == .identity {
            UIView.animate(withDuration: 0.3, animations: {
                self.bgButton.alpha = 0
                self.datePickerView.transform = CGAffineTransform(translationX: 0, y: self.datePickerView.frame.height)
            }) { _ in
                self.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func clickedCancelButton(_ sender: UIButton) {
        hide()
    }

    @IBAction func clickedSaveButton(_ sender: UIButton) {
        onDatePick?(datePicker.date)
        hide()
    }
    
}

//
//  CSPButton.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPButton: UIButton {
    
    @IBInspectable var imageBackgroundColor: UIColor = UIColor.blue {
        didSet {
            prepareBackgroundImage()
        }
    }
    
    @IBInspectable var fontSize: CGFloat = 21 {
        didSet {
            titleLabel?.font = R.font.muliBold(size: fontSize)
        }
    }
    
    @IBInspectable var backgroundNeeded: Bool = false {
        didSet {
            prepareBackgroundImage()
        }
    }
    
    private var titleStorage: String?
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView(style: .white)
        ai.startAnimating()
        ai.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
        return ai
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tintColor = .white
        setTitleColor(.white, for: .normal)
        titleLabel?.shadowOffset = CGSize(width: 0, height: 1)
        setTitleShadowColor(UIColor(0x1c8b81), for: .normal)
        titleEdgeInsets = UIEdgeInsets(top: -4, left: 0, bottom: 0, right: 0)
        imageEdgeInsets = UIEdgeInsets(top: -4, left: 0, bottom: 0, right: 0)
    }
    
    public func showLoading() {
        titleStorage = title(for: .normal)
        setTitle(nil, for: .normal)
        addSubview(activityIndicator)
        layoutIfNeeded()
        isEnabled = false
    }
    
    var completionBlock: (() -> ())?
    
    public func hideLoading(success: Bool, completion: @escaping () -> ()) {
        activityIndicator.removeFromSuperview()
        isEnabled = true
        completionBlock = completion
        if success {
            setTitle(nil, for: .normal)
            setImage(R.image.tick_white(), for: .normal)
            perform(#selector(endLoading), with: nil, afterDelay: 1)
        } else {
            endLoading()
        }
    }
    
    private func prepareBackgroundImage() {
        if backgroundNeeded {
            if self.bounds.width <= 80 {
                setBackgroundImage(R.image.button_small(), for: .normal)
                setBackgroundImage(R.image.button_small_pressed(), for: .disabled)
            } else {
                setBackgroundImage(R.image.button(), for: .normal)
                setBackgroundImage(R.image.button_pressed(), for: .disabled)
            }
        }
    }
    
    @objc private func endLoading() {
        setTitle(titleStorage, for: .normal)
        setImage(nil, for: .normal)
        completionBlock?()
        completionBlock = nil
    }

}

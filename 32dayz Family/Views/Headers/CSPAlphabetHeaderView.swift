//
//  CSPAlphabetHeaderView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPAlphabetHeaderView: UIView {
    
    class func instanceFromNib() -> CSPAlphabetHeaderView {
        return R.nib.cspAlphabetHeaderView().instantiate(withOwner: nil, options: nil).first as! CSPAlphabetHeaderView
    }

    @IBOutlet weak var letterLabel: UILabel!
    
    public func config(forLetter letter: String) {
        letterLabel.text = letter.uppercased()
    }

}

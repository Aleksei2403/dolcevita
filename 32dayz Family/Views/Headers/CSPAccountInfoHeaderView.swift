//
//  CSPAccountInfoHeaderView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/21/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPAccountInfoHeaderView: UIView {

    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var userPhotoImageView: AvatarImageView!
    @IBOutlet weak var addPhotoButton: UIButton!
    
    var onAddPhotoClick: (() -> ())?
    
    class func instanceFromNib() -> CSPAccountInfoHeaderView {
         return UIKit.UINib(resource: R.nib.cspAccountInfoHeaderView).instantiate(withOwner: nil, options: nil  ).first as! CSPAccountInfoHeaderView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        config()
    }
    
    func config() {
        guard let user = CSPSessionManager.currentUser else { return }
        if user.photoURL != nil {
            userInitialsView.isHidden = true
            userPhotoImageView.set(user: user)
        } else {
            userPhotoImageView.image = nil
            userInitialsView.isHidden = false
            userInitialsLabel.text = user.initials
        }
    }
    
    func config(withPickedImage img: UIImage?) {
        userInitialsView.isHidden = true
        userPhotoImageView.image = img
    }
    
    // MARK: - Actions
    
    @IBAction func clickedAddPhotoButton(_ sender: UIButton) {
        onAddPhotoClick?()
    }

}

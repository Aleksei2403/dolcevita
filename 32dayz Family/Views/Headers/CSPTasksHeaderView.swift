//
//  CSPTasksHeaderView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/8/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPTasksHeaderView: UIView {
    
    @IBOutlet weak var mainLabel: UILabel!
    
    lazy var weekDayFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "EEE"
        return df
    }()
    
    lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "MMM dd"
        return df
    }()
    
    class func instanceFromNib() -> CSPTasksHeaderView {
        return R.nib.cspTasksHeaderView().instantiate(withOwner: nil, options: nil).first as! CSPTasksHeaderView
    }
    
    func config(forDate date: NSDate?) {
        guard let date = date else {

            let attrString = NSMutableAttributedString(string: "Other")
            mainLabel.attributedText = attrString
            return
        }
        if date.isToday {
            let today = "Today \(weekDayFormatter.string(from: date as Date))"
            let date = dateFormatter.string(from: date as Date)
            let attrString = NSMutableAttributedString(string: today + " " + date)
            attrString.addAttribute(NSAttributedString.Key.foregroundColor,
                                    value: UIColor.black,
                                    range: NSMakeRange(0, today.count))
            attrString.addAttribute(NSAttributedString.Key.font,
                                    value: R.font.muliBold(size: 16)!,
                                    range: NSMakeRange(0, today.count))
            mainLabel.attributedText = attrString
        } else if date.isTomorrow {
            let tomorrow = "Tomorrow \(weekDayFormatter.string(from: date as Date))"
            let date = dateFormatter.string(from: date as Date)
            let attrString = NSMutableAttributedString(string: tomorrow + " " + date)
            attrString.addAttribute(NSAttributedString.Key.foregroundColor,
                                    value: UIColor.black,
                                    range: NSMakeRange(0, tomorrow.count))
            attrString.addAttribute(NSAttributedString.Key.font,
                                    value: R.font.muliBold(size: 16)!,
                                    range: NSMakeRange(0, tomorrow.count))
            mainLabel.attributedText = attrString
        } else {
            let day = weekDayFormatter.string(from: date as Date)
            let date = dateFormatter.string(from: date as Date)
            let attrString = NSMutableAttributedString(string: day + " " + date)
            attrString.addAttribute(NSAttributedString.Key.foregroundColor,
                                    value: UIColor.black,
                                    range: NSMakeRange(0, day.count))
            attrString.addAttribute(NSAttributedString.Key.font,
                                    value: R.font.muliBold(size: 16)!,
                                    range: NSMakeRange(0, day.count))
            mainLabel.attributedText = attrString
        }
    }
    
    func configForOverdue() {
        let date = "Overdue Tasks"
        let attrString = NSMutableAttributedString(string: date, attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: R.font.muliBold(size: 16)!
            ])
        mainLabel.attributedText = attrString
    }

}

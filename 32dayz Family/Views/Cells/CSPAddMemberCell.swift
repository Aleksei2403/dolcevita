//
//  CSPAddMemberCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/1/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPAddMemberCell: UITableViewCell {

    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var user: CSPUserModel?
    
    var onSelectUser: ((CSPUserModel) -> ())?
    var onDeselectUser: ((CSPUserModel) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func config(forUser user: CSPUserModel, isSelected: Bool) {
        self.user = user
        if let url = user.photoURL {
            userPhotoImageView.isHidden = false
            userInitialsView.isHidden = true
            Nuke.loadImage(with: URL(string: url)!, into: userPhotoImageView)
        } else {
            userPhotoImageView.isHidden = true
            userInitialsView.isHidden = false
            userInitialsLabel.text = user.initials
        }
        userNameLabel.text = user.fullName
        addButton.isSelected = isSelected
    }

    @IBAction func clickedAddButton(_ sender: UIButton) {
        guard let user = user else { return }
        if sender.isSelected {
            onDeselectUser?(user)
        } else {
            onSelectUser?(user)
        }
    }
    
}

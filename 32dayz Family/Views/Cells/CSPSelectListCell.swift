//
//  CSPSelectListCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/1/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPSelectListCell: UITableViewCell {
    
    @IBOutlet weak var listImageView: UIImageView!
    @IBOutlet weak var listColorView: UIView!
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var listSelectionImageView: UIImageView!
    
    public func config(forList list: CSPListModel, isSelected: Bool) {
        if let color = list.color {
            listImageView.isHidden = true
            listColorView.isHidden = false
            listColorView.backgroundColor = .getUIColor(withHex: color)
        } else {
            listImageView.isHidden = false
            listColorView.isHidden = true
        }
        listNameLabel.text = list.name
        listSelectionImageView.isHidden = !isSelected
    }

}

//
//  CSPMonthCalendarCell.swift
//  32dayz Family
//
//  Created by mac on 8.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CSPMonthCalendarCell: JTACDayCell, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var contentViewBackGroundColor: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var borderAndWidthView: UIView!
    @IBOutlet weak var viewDateInTableView: UIView!
    @IBOutlet weak var labelMonth: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let model = CSPMenuModel()
    var models = CSPEventsModel()
    var dates = [Date]()
    var events:[AnyObject] = []
    var cellState = ""
    var eventNames : [AnyObject] = []
    var eventNamesCount: Int {
       get {
        return eventNames.count
       }
   }
    override func awakeFromNib() {
        super.awakeFromNib()
        labelMonth.font = R.font.latoRegular(size: 12)
        labelMonth.textColor = .turquoiseBlue
        dateLabel.font = R.font.latoRegular(size: 12)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        borderAndWidthView.layer.borderWidth = 1
        borderAndWidthView.layer.borderColor = #colorLiteral(red: 0.9182453156, green: 0.9182668328, blue: 0.9182552695, alpha: 0.6980392157)
       
        
    }
   
    
    override func prepareForReuse() {
        self.tableView.backgroundColor = .white
        self.viewDateInTableView.backgroundColor = .white
        self.tableView.isHidden = true
    
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventNamesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CSPMonthTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "CSPMonthTableViewCell_id") as! CSPMonthTableViewCell
        
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        let formatter = DateFormatter()
        formatter.dateFormat = "d"
//        
//        for event in events {
////            if let date = event.date {
//            let date = formatter.string(from: event.date as Date)
////            if let content = event.content {
//                if cellState ==  date {
//                    cell.nameTaskLabel.text = event.content!
//               // }
//           // }
//            }
//        }
        cell.nameTaskLabel.text = "\(events[indexPath.row].content!)"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("hello")
    }
    

}

  
   
    
    


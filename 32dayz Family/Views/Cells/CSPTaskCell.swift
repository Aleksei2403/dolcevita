//
//  CSPTaskCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/7/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class CSPTaskCell: MGSwipeTableCell {
    
    @IBOutlet weak var priorityImageTask: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var listImageView: UIImageView!
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateColorView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var dateView: UIView!
    
    @IBOutlet weak var constraintWidthCollectionView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightBottomView: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomContentLabel: NSLayoutConstraint!
    
    var task: CSPTaskModel?
    var onPriorityClick: ((CSPTaskModel) -> ())?
    var onDoneClick: ((CSPTaskModel) -> ())?
    var onCalendarClick: ((CSPTaskModel) -> ())?
    var onEditClick: ((CSPTaskModel) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(R.nib.cspTaskMemberCell)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            backgroundColor = UIColor(0xf0f0f0)
        } else {
            backgroundColor = .white
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = UIColor(0xf0f0f0)
        } else {
            backgroundColor = .white
        }
    }
    
    func config(withTask task: CSPTaskModel, mode: MenuItemType, isExpanded: Bool) {
        self.task = task
        let prioritys = ["None":#imageLiteral(resourceName: "Vector red"), "Low":#imageLiteral(resourceName: "Vector yellow"),"Normal":#imageLiteral(resourceName: "Vector blue"),"High":#imageLiteral(resourceName: "Vector green"),"Highest":#imageLiteral(resourceName: "Flag grey")]
        constraintBottomContentLabel.constant = 15
        if task.membersArray.count == 0 {
            constraintWidthCollectionView.constant = 0
            constraintHeightCollectionView.constant = 0
        } else {
            constraintBottomContentLabel.constant = 60
            constraintHeightCollectionView.constant = 30
            let count = task.membersArray.count > 3 ? 3 : task.membersArray.count
            constraintWidthCollectionView.constant = CGFloat(count * 30 + (count - 1) * 5)
        }
        for priority in prioritys {
            if task.priority == priority.key {
                priorityImageTask.image = priority.value
            }
        }
        collectionView.reloadData()
        contentLabel.text = task.content
        constraintHeightBottomView.constant = isExpanded ? 44 : 0
        switch mode {
        case .Overdue, .Next7Days:
            listView.isHidden = false
            dateView.isHidden = false
            constraintBottomContentLabel.constant = 60
            configListName()
            configTaskDate()
            break
        case .Someday:
            configListName()
            configTaskDate()
        case .Today:
            listView.isHidden = false
            dateView.isHidden = true
            constraintBottomContentLabel.constant = max(30, constraintBottomContentLabel.constant)
            configListName()
            break
        case .Inbox, .CustomList:
            listView.isHidden = true
            dateView.isHidden = task.date == nil
            if task.date != nil {
                constraintBottomContentLabel.constant = max(30, constraintBottomContentLabel.constant)
            }
            configTaskDate()
            break
        default:
            break
        }
    }
    private func configListName() {
        let listName = task?.list?.name
        listNameLabel.text = listName
        if listName != "Inbox" {
            if let hex = task?.list?.color {
                let color = UIColor.getUIColor(withHex: hex)
                listImageView.image = UIImage(color: color)
            }
        } else {
            listImageView.image = R.image.inboxMark()
        }
    }
    
    private func configTaskDate() {
        if task?.date != nil {
            dateLabel.text = task!.date!.stringDescription
            dateColorView.backgroundColor = task!.date!.isOverdue ? UIColor(0xf76e9e) : UIColor(0x48b70b)
        }
    }
    
    func expand() {
        constraintHeightBottomView.constant = 44
        UIView.animate(withDuration: 0.3) {
            self.contentView.layoutIfNeeded()
        }
    }
    
    func collapse() {
        constraintHeightBottomView.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.contentView.layoutIfNeeded()
        }
    }
    
    // MARK: - Actions
    
    @IBAction func clickedDoneButton(_ sender: UIButton) {
        setSwipeOffset(frame.width, animated: true) { _ in
            if self.task != nil {
                self.onDoneClick?(self.task!)
            }
        }
    }
    
    @IBAction func clickedPriorityTaskButton(_ sender: UIButton) {
        if self.task != nil {
            onPriorityClick?(self.task!)
        }
        
    }
    @IBAction func clickedCalendarButton(_ sender: UIButton) {
        if self.task != nil {
            onCalendarClick?(self.task!)
        }
    }
    
    @IBAction func clickedEditButton(_ sender: UIButton) {
        if self.task != nil {
            onEditClick?(self.task!)
        }
    }
    
}

extension CSPTaskCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    fileprivate var taskMembersCount: Int {
        get {
            return task?.members?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return taskMembersCount > 3 ? 3 : taskMembersCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspTaskMemberCell_id.identifier, for: indexPath) as! CSPTaskMemberCell
        cell.transform = CGAffineTransform(scaleX: -1, y: 1)
        if indexPath.item == 2 && taskMembersCount > 3 {
            cell.config(forUsers: taskMembersCount - 2)
        } else {
            cell.config(forUser: task!.membersArray[indexPath.item])
        }
        return cell
    }
    
}


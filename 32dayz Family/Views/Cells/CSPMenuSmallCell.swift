//
//  CSPMenuSmallCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPMenuSmallCell: UITableViewCell {

    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var badgeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            backgroundColor = UIColor(0x60d0f2)
        } else {
            backgroundColor = .clear
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = UIColor(0x60d0f2)
        } else {
            backgroundColor = .clear
        }
    }
    
    public func config(forMenuSmallItem item: CSPMenuSmallItem) {
        mainLabel.text = item.title
        badgeLabel.text = String(item.counter)
        badgeLabel.isHidden = item.counter == 0
        typeImageView.isHidden = item.color != nil
        colorView.isHidden = item.color == nil
        typeImageView.image = R.image.ellipse()
        colorView.backgroundColor = item.color
    }

}

//
//  CSPCircleCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/19/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPCircleCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorInitialsLabel: UILabel!
    @IBOutlet weak var authorInitialsView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var membersCountLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            bgImageView.image = isSelected ? R.image.circle_bg_pressed() : R.image.circle_bg()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            bgImageView.image = isHighlighted ? R.image.circle_bg_pressed() : R.image.circle_bg()
        }
    }
    
    func config(forCircle circle: CSPCircleModel) {
        nameLabel.text = circle.name
        authorImageView.image = nil
        if let membersCount  = circle.members?.count {
            membersCountLabel.text = "\(membersCount) \(membersCount == 1 ? "member" : "members")"
            if let author = circle.author {
                if let url = author.photoURL {
                    authorInitialsView.isHidden = true
                    Nuke.loadImage(with: URL(string: url)!, into: authorImageView)
                } else {
                    authorImageView.image = nil
                    authorInitialsView.isHidden = false
                    authorInitialsLabel.text = author.initials
                }
            }
        } else {
            authorInitialsView.isHidden = true
            authorImageView.image = nil
            membersCountLabel.text = nil
        }
        
    }
    
}

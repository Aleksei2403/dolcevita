//
//  CSPInvitedMembersCellTableViewCell.swift
//  32dayz Family
//
//  Created by mac on 17.05.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke
import MessageUI

protocol CSPInvitedMembersCellTableViewCellDelegate {
    func didButtonPressed(phone: String)
}
class CSPInvitedMembersCellTableViewCell: UITableViewCell {

    @IBOutlet weak var invitedAgainButton: UIButton!
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var moreAmountLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    var delegate: CSPInvitedMembersCellTableViewCellDelegate?
    var sendAgainInvaite: (() -> ())?
    var onUserInvite: ((CSPUserModel) -> ())?
    var userInvitePhone: String = ""
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        invitedAgainButton.layer.cornerRadius = 6
        userInitialsView.layer.cornerRadius = 23
    }
    
    public func config(forUser user: CSPUserModel) {
        if let url = user.photoURL {
            userPhotoImageView.isHidden = false
            userInitialsView.isHidden = true
            Nuke.loadImage(with: URL(string: url)!, into: userPhotoImageView)
        } else {
            userPhotoImageView.isHidden = true
            userInitialsView.isHidden = false
            userInitialsLabel.isHidden = false
            moreAmountLabel.isHidden = true
            moreLabel.isHidden = true
            userInitialsLabel.text = user.initials
        }
    }
    
    public func config(forUsers count: Int) {
        userPhotoImageView.isHidden = true
        userInitialsView.isHidden = false
        userInitialsLabel.isHidden = true
        moreAmountLabel.isHidden = false
        moreLabel.isHidden = false
        moreAmountLabel.text = "\(count)"
    }
    
    @IBAction func inviteAgainClicked(_ sender: Any) {
        print("invite sended")
        delegate?.didButtonPressed(phone: userInvitePhone)
        self.invitedAgainButton.isHidden = true
    }
    
}

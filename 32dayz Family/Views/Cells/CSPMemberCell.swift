//
//  CSPMemberCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/17/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Contacts

class CSPMemberCell: UITableViewCell {
    
    var onInviteClick: (() -> ())?

    @IBOutlet weak var inviteButton: CSPButton!
    @IBOutlet weak var imageDataView: UIImageView!
    @IBOutlet weak var initialsView: UIView!
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func config(forContact contact: CNContact, isInvited: Bool) {
        let fullName = CNContactFormatter.string(from: contact, style: .fullName)
        fullNameLabel.text = fullName
//        if let components = fullName?.components(separatedBy: " ") {
//            initialsLabel.text = components.map { String($0[$0.startIndex]) }.joined()
//        } else {
//            initialsLabel.text = nil
//        }
        imageDataView.isHidden = contact.imageData == nil
        initialsView.isHidden = contact.imageData != nil
        imageDataView.image = contact.imageData == nil ? nil : UIImage(data: contact.imageData!)
        if isInvited {
            inviteButton?.setTitle("Accepted", for: .normal)
            inviteButton?.isEnabled = false
            inviteButton?.alpha = 0.5
        } else {
            inviteButton?.setTitle("Invite", for: .normal)
            inviteButton?.isEnabled = true
            inviteButton?.alpha = 1.0
        }
    }

    @IBAction func clickedInviteButton(_ sender: UIButton) {
        onInviteClick?()
    }
    
}

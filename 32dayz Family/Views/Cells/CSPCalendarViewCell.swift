////
/////
//  CSPEventTableViewCell.swift
//  32dayz Family
//
//  Created by mac on 3.08.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCalendarViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, cspCalendarViewControllerDelegate {
   
    
    @IBOutlet weak var listColorView: UIView!
    @IBOutlet weak var imageListColor: UIImageView!
    @IBOutlet weak var noEventsScheduledLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameTask: UILabel!
    @IBOutlet weak var allDayLabel: UILabel!
    @IBOutlet weak var labelFinishEvent: UILabel!
    @IBOutlet weak var labelStartEvent: UILabel!
    
    let alignedFlowLayout = UICollectionViewFlowLayout.self
    var event: CSPEventModel?
    
    var eventMembersCount: Int {
        get {
            if let event = self.event {
               
                return event.members?.count ?? 0
            } else {
                return 0
            }
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        if let event = self.event {
            for member in event.membersArray {
                print(member.fullName)
            }
        }
        
        collectionView.register(R.nib.cspTaskMemberCell)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
        collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        collectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        noEventsScheduledLabel.font = R.font.latoRegular(size: 14)
        allDayLabel.font = R.font.muliLight(size: 14)
        nameTask.font = R.font.muliLight(size: 14)
        labelStartEvent.font = R.font.muliLight(size: 14)
        labelFinishEvent.font = R.font.muliLight(size: 14)
    }
    
    override func prepareForReuse() {
        self.labelFinishEvent.text = ""
        self.labelStartEvent.text = ""
        self.nameTask.text = ""
        self.allDayLabel.text = ""
        self.noEventsScheduledLabel.text = "No events scheduled"
        
        super.prepareForReuse()
        
    }
    
    public func config(forList list: CSPListModel) {
        if let color = list.color {
            imageListColor.isHidden = true
            listColorView.isHidden = false
            listColorView.backgroundColor = .getUIColor(withHex: color)
        } else {
            imageListColor.isHidden = false
            listColorView.isHidden = true
        }
     
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
         super.setSelected(selected, animated: animated)

         if selected {
             contentView.backgroundColor = UIColor.white
         } else {
             contentView.backgroundColor = UIColor.white
         }
     }
    
   
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return eventMembersCount > 3 ? 3 : eventMembersCount
        }
    
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspTaskMemberCell_id.identifier, for: indexPath) as! CSPTaskMemberCell
            if indexPath.item == 2 && eventMembersCount > 3 {
                cell.config(forUsers: eventMembersCount - 2)
            } else {
                cell.config(forUser: event!.membersArray[indexPath.item])
            }
            return cell
        }
   
    func cspCalendarViewControllerDelegate(wiyh event: CSPEventModel) {
        self.event = event
    }
    
}


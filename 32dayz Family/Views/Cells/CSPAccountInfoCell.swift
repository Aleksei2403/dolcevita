//
//  CSPAccountInfoCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/21/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPAccountInfoCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var editButton: UIButton!
    
    var type: CSPAccountInfoCellType?
    var onEditPhoneClick: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func config(forType type: CSPAccountInfoCellType) {
        self.type = type
        textField.isUserInteractionEnabled = false
        switch type {
        case .FirstName:
            textField.text = CSPSessionManager.currentUser?.firstName
            break
        case .LastName:
            textField.text = CSPSessionManager.currentUser?.lastName
            break
        case .Phone:
            textField.text = CSPSessionManager.currentUser?.phone
            break
        }
    }

    // MARK: - Actions
    
    @IBAction func clickedEditButton(_ sender: UIButton) {
        if let type = type {
            switch type {
            case .FirstName, .LastName:
                textField.isUserInteractionEnabled = true
                textField.becomeFirstResponder()
                break
            case .Phone:
                onEditPhoneClick?()
                break
            }
        }
    }
    
    // MARK: - TextField
    
    @IBAction func didEndEditing(_ sender: UITextField) {
        textField.isUserInteractionEnabled = false
    }
    
}

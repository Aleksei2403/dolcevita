//
//  CSPPostCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/7/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPPostCell: UITableViewCell {
    
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var postImagePlaceholderView: UIView!
    @IBOutlet weak var postImageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postContentLabel: UILabel!
    @IBOutlet weak var listColorImageView: UIImageView!
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var constraintHeightPostImagePlaceholderView: NSLayoutConstraint!
    
    var post: CSPPostModel?
    var onMoreClick: ((CSPPostModel) -> ())?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            backgroundColor = UIColor(0xF0F0F0)
        } else {
            backgroundColor = .white
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = UIColor(0xF0F0F0)
        } else {
            backgroundColor = .white
        }
    }
    
    public func config(forPost post: CSPPostModel) {
        self.post = post
        commentsLabel.font = UIFont(name: R.font.latoRegular.fontName, size: 11)
        commentsLabel.textColor = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
        if let url = post.author?.photoURL {
            userPhotoImageView.isHidden = false
            userInitialsView.isHidden = true
            Nuke.loadImage(with: URL(string: url)!, into: userPhotoImageView)
        } else {
            userPhotoImageView.isHidden = true
            userInitialsView.isHidden = false
            userInitialsLabel.text = post.author?.initials
        }
        
        userNameLabel.text = post.author?.fullName
        
        postImageView.image = nil
        if let mediaURL = post.mediaURL {
            postImageActivityIndicator.startAnimating()
            postImagePlaceholderView.isHidden = false
            constraintHeightPostImagePlaceholderView.constant = UIScreen.main.bounds.width * CGFloat(post.mediaAspectRatio?.doubleValue ?? 1)
            Nuke.loadImage(with: URL(string: mediaURL)!, into: postImageView)
        } else {
            postImagePlaceholderView.isHidden = true
        }
        
        switch post.postType {
        case .Post:
            let attributedString =  NSMutableAttributedString(string: post.content ?? "")
            postContentLabel.attributedText = attributedString
            postContentLabel.text = post.content
            listColorImageView.isHidden = true
            listNameLabel.isHidden = true
            moreButton.isHidden = post.author?.uid != CSPSessionManager.currentUser?.uid
            let postedString = "Posted"
            let dateString = "\(postedString) \(post.createdAt!.stringRepresentation())"
            postDateLabel.text = dateString
            break
        case .CompletedTask:
            let completedString = "Completed a Task"
            let dateString = "\(completedString) \(post.createdAt!.stringRepresentation())"
            postDateLabel.text = dateString
            let attributedString =  NSMutableAttributedString(string: post.content ?? "")
            attributedString.addAttribute(NSAttributedString.Key.baselineOffset, value: 0, range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.font, value: R.font.openSansLightItalic(size: 15)!, range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(0x8b8b8b), range: NSMakeRange(0, attributedString.length))
            postContentLabel.attributedText = attributedString
            listColorImageView.isHidden = false
            listNameLabel.isHidden = false
            moreButton.isHidden = false
            let listName = post.list?.name
            listNameLabel.text = listName
            if listName != "Inbox" {
                if let hex = post.list?.color {
                    let color = UIColor.getUIColor(withHex: hex)
                    listColorImageView.image = UIImage(color: color)
                }
            } else {
                listColorImageView.image = R.image.inboxMark()
            }
            break
        }
        
    }
    // MARK: - Actions
    
    @IBAction func clickedMoreButton(_ sender: UIButton) {
        if post != nil {
            onMoreClick?(post!)
        }
    }
    
}


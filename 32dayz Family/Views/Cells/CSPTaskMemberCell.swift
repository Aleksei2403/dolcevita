//
//  CSPTaskMemberCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/1/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPTaskMemberCell: UICollectionViewCell {

    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var moreAmountLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = frame.width / 2
        clipsToBounds = true
    }
    
    public func config(forUser user: CSPUserModel) {
        if let url = user.photoURL {
            userPhotoImageView.isHidden = false
            userInitialsView.isHidden = true
            Nuke.loadImage(with: URL(string: url)!, into: userPhotoImageView)
        } else {
            userPhotoImageView.isHidden = true
            userInitialsView.isHidden = false
            userInitialsLabel.isHidden = false
            moreAmountLabel.isHidden = true
            moreLabel.isHidden = true
            userInitialsLabel.text = user.initials
        }
    }
    
    public func config(forUsers count: Int) {
        userPhotoImageView.isHidden = true
        userInitialsView.isHidden = false
        userInitialsLabel.isHidden = true
        moreAmountLabel.isHidden = false
        moreLabel.isHidden = false
        moreAmountLabel.text = "\(count)"
    }

}

//
//  CSPCircleInvitedMembersCell.swift
//  32dayz Family
//
//  Created by mac on 17.05.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

protocol CSPCircleInvitedMembersCellDelegate {
    func sendAgainInvaite( phone: String)
}

class CSPCircleInvitedMembersCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource, CSPInvitedMembersCellTableViewCellDelegate{
    
    var phoneInvite: String = ""
    var model = CSPManageCircleModel()
    var members: [CSPUserModel] = []
    var onCellSelection: (() -> ())?
    var delegate: CSPCircleInvitedMembersCellDelegate?
    var sendAgainInvite: ((String?) -> ())?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.register(R.nib.cspInvitedMembersCellTableViewCell)
    }
    
    
    
    public func configinvitedMembers(phone: String, withInvitedMembers invitedMembers:[CSPUserModel], isEditable: Bool) {
        self.members = invitedMembers
        self.phoneInvite = phone
        tableView.reloadData()
    }
    
    private var maxItems: Int {
        get {
            return Int((tableView.frame.width - 50) / (50)) //pm cell width (45) + spacing (5)
        }
    }
    
    fileprivate var circleinvitedMembersCount: Int {
        get {
            return members.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return circleinvitedMembersCount
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cspInvitedMembersCellTableViewCell_id, for: indexPath) as! CSPInvitedMembersCellTableViewCell
        let hh2 = (Calendar.current.component(.hour, from: Date()))
        cell.userInvitePhone = members[indexPath.item].phone ?? ""
        cell.delegate = self
        for i in members {
            if i.notVerification == true && hh2 >= 9 {
                cell.invitedAgainButton.isHidden = false
            } 
        }
   
       if indexPath.item == maxItems - 1 && circleinvitedMembersCount > maxItems {
            cell.config(forUsers: circleinvitedMembersCount - maxItems + 1)
       } else {
            cell.config(forUser: members[indexPath.item])
        }
       
        return cell
    
    }
    
    func didButtonPressed(phone: String) {
        phoneInvite = phone
        sendAgainInvite?(phone)
}
}

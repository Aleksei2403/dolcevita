//
//  CSPListColorCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/3/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPListColorCell: UICollectionViewCell {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var selectionImageView: UIImageView!
    
    public func config(withColor color: CSPColorModel, isSelected: Bool) {
        colorView.backgroundColor = color.value
        selectionImageView.isHidden = !isSelected
    }
    
}

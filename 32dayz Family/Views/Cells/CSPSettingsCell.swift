//
//  CSPSettingsCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPSettingsCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            backgroundColor = .white
        } else {
            backgroundColor = UIColor(0xfcfcfc)
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = .white
        } else {
            backgroundColor = UIColor(0xfcfcfc)
        }
    }
    
    public func config(forSettingsItem item: CSPSettingsItem) {
        mainLabel.text = item.title
        activeLabel.isHidden = !item.isSelected
//        arrowImageView.isHidden = item.isSelected && !item.isAuthor
    }

}

//
//  CSPCountryCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/13/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCountryCell: UITableViewCell {

    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func config(forCountry country: CSPCountry) {
        countryNameLabel.text = country.name
        countryCodeLabel.text = "+" + String(country.code)
    }
    
    public func configSelectedState() {
        checkImageView.isHidden = false
    }
    
    public func configUnselectedState() {
        checkImageView.isHidden = true
    }

}

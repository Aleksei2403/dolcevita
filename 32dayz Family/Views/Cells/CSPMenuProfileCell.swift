//
//  CSPMenuProfileCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/12/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPMenuProfileCell: UITableViewCell {

    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var userPhotoImageView: AvatarImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var constraintTopUserImageView: NSLayoutConstraint!
    
    var onSettingsClick: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    public func config(topConstraintConstant constant: CGFloat) {
        constraintTopUserImageView.constant = constant
        if let user = CSPSessionManager.currentUser {
            userNameLabel.text = user.fullName
            if user.photoURL != nil {
                userInitialsLabel.isHidden = true
                userPhotoImageView.set(user: user)
            } else {
                userPhotoImageView.image = R.image.ic_menu_profile_bg()
                userInitialsLabel.isHidden = false
                userInitialsLabel.text = user.initials
            }
        }
    }

    @IBAction func clickedSettingsButton(_ sender: UIButton) {
        onSettingsClick?()
    }
    
}

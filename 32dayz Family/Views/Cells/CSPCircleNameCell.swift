//
//  CSPCircleNameCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/16/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCircleNameCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    
    var onTextChange: ((String?) -> ())?

    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            backgroundColor = .white
        } else {
            backgroundColor = UIColor(0xfcfcfc)
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = .white
        } else {
            backgroundColor = UIColor(0xfcfcfc)
        }
    }
    
    public func config(withName name: String?, isEditable: Bool) {
        textField.text = name
        textField.isEnabled = isEditable
    }

    // MARK: - Actions
    
    @IBAction func textFieldTextDidChange(_ sender: UITextField) {
        onTextChange?(sender.text)
    }
    
}

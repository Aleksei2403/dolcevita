//
//  CSPPopUpCalendarCell.swift
//  32dayz Family
//
//  Created by mac on 18.06.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPPopUpCalendarCell: UICollectionViewCell {

    @IBOutlet weak var labelDay: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        labelDay.font = R.font.latoRegular(size: 12)
    }

}

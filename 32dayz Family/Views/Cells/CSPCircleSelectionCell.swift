//
//  CSPCircleSelectionCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/18/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCircleSelectionCell: UITableViewCell {

    @IBOutlet weak var circleNameLabel: UILabel!
    @IBOutlet weak var selectedCircleImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func config(forCircle circle: CSPCircleModel, isCurrent: Bool) {
        circleNameLabel.text = circle.name
        selectedCircleImageView.isHidden = !isCurrent
    }

}

//
//  CSPCircleMembersCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/16/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit


class CSPCircleMembersCell: UITableViewCell,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    var members: [CSPUserModel] = []
    var invitedMembers: [CSPUserModel] = []
    var onCellSelection: (() -> ())?

 
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var arrowImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(R.nib.cspTaskMemberCell)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            backgroundColor = .white
        } else {
            backgroundColor = UIColor(0xfcfcfc)
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            backgroundColor = .white
        } else {
            backgroundColor = UIColor(0xfcfcfc)
        }
    }
    
    public func config(withMembers members: [CSPUserModel], isEditable: Bool) {
        self.members = members
        arrowImageView.isHidden = !isEditable
        collectionView.reloadData()
    }

    // MARK: - Actions
    
    @IBAction func highlight(_ sender: UIButton) {
        setHighlighted(true, animated: true)
    }
    
    @IBAction func unhighlight(_ sender: UIButton) {
        setHighlighted(false, animated: true)
    }
    
    @IBAction func didClickMembersButton(_ sender: UIButton) {
        onCellSelection?()
    }
    private var maxItems: Int {
        get {
            return Int((collectionView.frame.width - 50) / (50)) //pm cell width (45) + spacing (5)
        }
    }
    
    fileprivate var circleMembersCount: Int {
        get {
            return members.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return circleMembersCount > maxItems ? maxItems : circleMembersCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.cspTaskMemberCell_id.identifier, for: indexPath) as! CSPTaskMemberCell
        if indexPath.item == maxItems - 1 && circleMembersCount > maxItems {
            cell.config(forUsers: circleMembersCount - maxItems + 1)
        } else {
            cell.config(forUser: members[indexPath.item])
        }
        return cell
    }
    
}

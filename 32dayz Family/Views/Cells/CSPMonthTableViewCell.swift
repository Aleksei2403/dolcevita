//
//  CSPMonthTableViewCell.swift
//  32dayz Family
//
//  Created by mac on 9.07.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPMonthTableViewCell: UITableViewCell {

    @IBOutlet weak var nameTaskLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTaskLabel.font = R.font.latoRegular(size: 10)
        nameTaskLabel.backgroundColor = .clear
    }

    override func prepareForReuse() {
    
       }

}

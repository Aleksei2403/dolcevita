//
//  CSPManageMemberCell.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/16/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import Nuke

class CSPManageMemberCell: UITableViewCell {

    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
    var user: CSPUserModel?
    
    var onRemoveUser: ((CSPUserModel) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func config(forUser user: CSPUserModel, isHidden: Bool) {
        self.user = user
        if let url = user.photoURL {
            userPhotoImageView.isHidden = false
            userInitialsView.isHidden = true
            Nuke.loadImage(with: URL(string: url)!, into: userPhotoImageView)
        } else {
            userPhotoImageView.isHidden = true
            userInitialsView.isHidden = false
            userInitialsLabel.text = user.initials
        }
        userNameLabel.text = user.fullName
        removeButton.isHidden = isHidden
    }
    
    @IBAction func clickedRemoveButton(_ sender: UIButton) {
        guard let user = user else { return }
        onRemoveUser?(user)
    }

}

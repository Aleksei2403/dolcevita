//
//  CSPReminderTableViewCell.swift
//  32dayz Family
//
//  Created by mac on 26.08.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPReminderTableViewCell: UITableViewCell {

    @IBOutlet weak var goldLabel: UILabel!
    @IBOutlet weak var imageReminder: UIImageView!
    @IBOutlet weak var textReminder: UILabel!
    @IBOutlet weak var reminderLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        reminderLabel.font = R.font.latoRegular(size: 17)
        reminderLabel.textColor = .black
        textReminder.text = "none"
        
    }

    
    
}

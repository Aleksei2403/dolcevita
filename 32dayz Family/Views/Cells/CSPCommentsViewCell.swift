//
//  CSPCommentsViewCell.swift
//  32dayz Family
//
//  Created by mac on 1.06.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPCommentsViewCell: UITableViewCell {

    @IBOutlet weak var dateCommentLabel: UILabel!
    @IBOutlet weak var authorCommentLabel: UILabel!
    
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userInitialsView: UIView!
    @IBOutlet weak var userInitialsLabel: UILabel!
    @IBOutlet weak var textCommentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userPhotoImageView.layer.cornerRadius = 23
        userInitialsView.layer.cornerRadius = 23
         
    }
}

//
//  CSPAddListTableViewCell.swift
//  32dayz Family
//
//  Created by mac on 29.08.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

class CSPAddListTableViewCell: UITableViewCell {

    @IBOutlet weak var addListLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

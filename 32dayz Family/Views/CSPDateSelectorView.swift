//
//  CSPDateSelectorView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/9/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPDateSelectorView: UIView {

    @IBOutlet var buttons: [UIButton]!
    
    var onDateSelection: ((NSDate) -> ())?
    var onDatePick: (() -> ())?
    var onDateRemove: (() -> ())?
    
    class func instanceFromNib() -> CSPDateSelectorView {
        return R.nib.cspDateSelectorView().instantiate(withOwner: nil, options: nil).first as! CSPDateSelectorView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = UIScreen.main.bounds
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        buttons.forEach { button in
            //pm 53x48 - image size
            let title = button.titleLabel!.text! as NSString
            let size = title.size(withAttributes: [NSAttributedString.Key.font: R.font.muliSemiBold(size: 14)!])
            button.imageEdgeInsets = UIEdgeInsets(top: -size.height / 2 - 15, left: (button.frame.width - 53) / 2, bottom: 0, right: 0)
            button.titleEdgeInsets = UIEdgeInsets(top: 39 + size.height / 2, left: (button.frame.width - size.width) / 2 - 53, bottom: 0, right: 0)
        }
    }
    
    public func show() {
        UIApplication.shared.keyWindow?.addSubview(self)
    }
    
    // MARK: - Actions
    
    @IBAction func clickedButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            onDateSelection?(NSDate.today())
            
            // MARK: - Analytics
            AnalyticsManager.logEvent(type: .TaskCreated, parameters: ["task_menu_date_updated" : Parameter(.type, .today)])
            // MARK: -
            
            break
        case 1:
            onDateSelection?(NSDate.tomorrow())
            
            // MARK: - Analytics
            
            AnalyticsManager.logEvent(type: .TaskMenuDateUpdated, parameters: ["task_menu_date_updated" :Parameter(.type, .tomorrow)])
            
            // MARK: -
            
            break
        case 2:
            onDateSelection?(NSDate.someday())
            
            // MARK: - Analytics
            
            AnalyticsManager.logEvent(type: .TaskMenuDateUpdated, parameters: ["task_menu_date_updated" :Parameter(.type, .someday)])
            
            // MARK: -
            
            break
        case 3:
            onDatePick?()
            
            // MARK: - Analytics
            
            AnalyticsManager.logEvent(type: .TaskMenuDateUpdated, parameters: ["task_menu_date_updated" :Parameter(.type, .customDate)])
            
            // MARK: -
            
            break
        case 4:
            onDateRemove?()
            
            // MARK: - Analytics
          
            AnalyticsManager.logEvent(type: .TaskMenuDateUpdated, parameters: ["task_menu_date_updated" :Parameter(.type, .removed)])
            
            // MARK: -
            
            break
        default:
            break
        }
        clickedBackgroundButton(sender)
    }

    @IBAction func clickedBackgroundButton(_ sender: UIButton) {
        removeFromSuperview()
    }
    
}

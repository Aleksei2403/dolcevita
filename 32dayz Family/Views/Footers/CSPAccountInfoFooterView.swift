//
//  CSPAccountInfoFooterView.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 8/21/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit

class CSPAccountInfoFooterView: UIView {

    var onLogOutClick: (() -> ())?
    
    class func instanceFromNib() -> CSPAccountInfoFooterView {
        return UIKit.UINib(resource: R.nib.cspAccountInfoFooterView).instantiate(withOwner: nil, options: nil  ).first as! CSPAccountInfoFooterView
    }
    
    // MARK: - Actions
    
    @IBAction func clickedLogOutButton(_ sender: UIButton) {
        onLogOutClick?()
    }

}

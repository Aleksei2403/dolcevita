//
//  Collection+Safe.swift
//  32dayz Family
//
//  Created by Valik Kuchinsky on 3/15/20.
//  Copyright © 2020 Nesus UAB. All rights reserved.
//

import Foundation

extension Collection where Index == Int {
    subscript(safe index: Index) -> Element? {
        return index >= 0 && index < count ? self[index] : nil
    }
}

//
//  NSDate+Compare.swift
//  32dayz Family
//
//  Created by Valik Kuchinsky on 3/12/20.
//  Copyright © 2020 Nesus UAB. All rights reserved.
//

import Foundation

func > (lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSinceReferenceDate > rhs.timeIntervalSinceReferenceDate
}

func < (lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.timeIntervalSinceReferenceDate < rhs.timeIntervalSinceReferenceDate
}

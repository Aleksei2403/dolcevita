//
//  Array+Sugar.swift
//  32dayz Family
//
//  Created by Valik Kuchinsky on 3/12/20.
//  Copyright © 2020 Nesus UAB. All rights reserved.
//

import Foundation

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}

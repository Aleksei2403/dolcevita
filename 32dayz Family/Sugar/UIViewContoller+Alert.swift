//
//  UIViewContoller+Alert.swift
//  32dayz Family
//
//  Created by Valik Kuchinsky on 3/10/20.
//  Copyright © 2020 Nesus UAB. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentAlertController(title: String = "Warning", text: String) {
        let alertController = UIAlertController(title: "Warning", message: text, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

//
//  UIScrollView+Postitioning.swift
//  32dayz Family
//
//  Created by Valik Kuchinsky on 3/8/20.
//  Copyright © 2020 Nesus UAB. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollToPage(_ page: Int, animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.contentOffset.x = (self?.frame.width ?? 0) * CGFloat(page)
            }
        } else {
            contentOffset.x = frame.width * CGFloat(page)
        }
    }

    func scrollToTop(animated: Bool = true) {
        guard self.contentOffset.y > 20 else {
            return
        }
        scrollRectToVisible(CGRect(x: -(contentInset.left), y: -(contentInset.top), width: 1, height: 1), animated: animated)
        scheduleOnMainQueueAfter(delay: animated ? 0.2 : 0.01) { // needs to refactor
            self.scrollToTop(animated: false)
            if self.contentOffset.y < 0 {
                self.contentOffset = CGPoint(x: -(self.contentInset.left), y: -(self.contentInset.top))
            }
        }
    }
}

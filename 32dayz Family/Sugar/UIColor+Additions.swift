//
//  UIColor+Additions.swift
//  32dayz Family
//
//  Created by mac on 16.06.21.
//  Copyright © 2021 Nesus UAB. All rights reserved.
//

import UIKit

extension UIColor {


    @nonobjc class var blackk: UIColor {
        return UIColor(red: 0, green: 0 , blue: 0 , alpha: 0.12)
    }
    
    @nonobjc class var turquoiseBlue: UIColor {
        return UIColor(red: 0.3843137255 , green: 0.737254902, blue: 0.9019607843, alpha: 1)
    }
    
    @nonobjc class var polar: UIColor {
        return UIColor(red:0.9490196078, green: 0.9725490196, blue: 0.9882352941, alpha: 1)
    }
}

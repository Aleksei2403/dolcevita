//
//  GCD+Sugar.swift
//  32dayz Family
//
//  Created by Valik Kuchinsky on 3/8/20.
//  Copyright © 2020 Nesus UAB. All rights reserved.
//

import Foundation

let defaultDelay: Double = 0.3
let noDelay: Double = 0.0

func scheduleOnMainQueueAfter(delay: Double, closure: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}


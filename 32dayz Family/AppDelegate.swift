//
//  AppDelegate.swift
//  FamilyOrganizer
//
//  Created by Vitalik on 8/3/16.
//  Copyright © 2016 Nesus UAB. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FBAudienceNetwork
import AppTrackingTransparency
import CoreData
import GoogleMobileAds
import Amplitude
import FBSDKCoreKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
static let shared = AppDelegate()
let apiKeyAmplitude = "269c54432ed99aa4bbfafb8467a0119e"
let secretCode = "1fda5117375e450db20f68c57f6826b0"
let center = UNUserNotificationCenter.current()
    
    enum AdIds : String {
        // FIXME:- Follow the link to know how to add AppId in Info.plist
        // https://developers.google.com/admob/ios/quick-start#update_your_infoplist
        
        // FIXME:-  REPLACE THE VALUES BY YOUR APP AND AD IDS
        case bannerJournal     = "ca-app-pub-2106792415220263/7082031750" // test id
        case banner = "ca-app-pub-3940256099942544/2934735716"
        case interestial = "ca-app-pub-3940256099942544/4411468910" // test id
        case rewarded    = "ca-app-pub-2106792415220263/6036435599" // id ca-app-pub-6594510677641115~1436482084
    }

    let testDevices:[String] = [
    //    "6e429ada4c3d0bacdd26e0704df1cfff",   //iPhone 5s
    //    "7e511268fe0e0942360666821f6d5b92", // iPhone 6
    //    "ba46a538145922976119616abaac7a2e" // iPad Air 2
    ]
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Amplitude.instance().trackingSessionEvents = true
          // Initialize SDK
        Amplitude.instance().initializeApiKey(apiKeyAmplitude)
          // Set userId
        Amplitude.instance().setUserId(CSPSessionManager.currentUser?.uid)
          // Log an event
        Amplitude.instance().logEvent("app_start")
        //Amplitude.instance().setServerUrl("https://api.eu.amplitude.com")
        CSPIAPManager.shared.fetchProducts { _ in }
        CSPAPIManager.shared.validateSubscription { }
        requestTrackingPermission()
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ kGADSimulatorID ]
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }

        return true
    }

    fileprivate func requestTrackingPermission() {
            if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization { (status) in
                    switch status {
                    case .authorized:
                        self.center.requestAuthorization(options: [.alert,.sound,.badge]) { (granted, error) in
                            
                            guard granted else { return }
                            self.center.getNotificationSettings { (settings) in
                                print(settings)
                                guard settings.authorizationStatus == .authorized else { return }
                            }
                           
                        }
                        FBAdSettings.setAdvertiserTrackingEnabled(true)
                    default:
                        self.center.requestAuthorization(options: [.alert,.sound,.badge]) { (granted, error) in
                            
                            guard granted else { return }
                            self.center.getNotificationSettings { (settings) in
                                print(settings)
                                guard settings.authorizationStatus == .authorized else { return }
                            }
                           
                        }
                        FBAdSettings.setAdvertiserTrackingEnabled(false)
                    }
                }
            }
        }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        #if DEBUG
        Auth.auth().setAPNSToken(deviceToken, type: .sandbox)
        #else
        Auth.auth().setAPNSToken(deviceToken, type: .prod)
        #endif
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification notification: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
    }

    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) { }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) {
        CSPDBManager.shared.saveContext()
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "32dayzFamilyDatabase")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    

}


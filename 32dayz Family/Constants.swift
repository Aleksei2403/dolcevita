//
//  Constants.swift
//  FamilyOrganizer
//
//  Created by Pavel Maksimov on 7/14/17.
//  Copyright © 2017 Nesus UAB. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Amplitude

let APPID = 1271378658
let APPSTORE_LINK = "itms-apps://itunes.apple.com/app/id\(APPID)"

// MARK: - Analytics

struct Event {
    static let OnboardingPhoneSubmitted = "OnboardingPhoneSubmitted"
    static let OnboardingAccountCreated = "OnboardingAccountCreated"
    static let OnboardingCircleCreated = "OnboardingCircleCreated"
    static let PostCreated = "PostCreated"
    static let PostDeleted = "PostDeleted"
    static let ListCreated = "ListCreated"
    static let ListDeleted = "ListDeleted"
    static let TaskCreated = "TaskCreated"
    static let TaskDeleted = "TaskDeleted"
    static let TaskCompleted = "TaskCompleted"
    static let TaskMenuDateUpdated = "TaskMenuDateUpdated"
    static let TaskMemberAdded = "TaskMemberAdded"
    static let TaskListAssigned = "TaskListAssigned"
    static let TaskDueDateAdded = "TaskDueDateAdded"
    static let TaskPositionChanged = "TaskPositionChanged"
    static let ApplicationShared = "ApplicationShared"
    static let CircleNameChanged = "CircleNameChanged"
    static let MemberInvited = "MemberInvited"
    static let CircleCreated = "CircleCreated"
    static let CircleDeleted = "CircleDeleted"
    static let PhoneNumberUpdated = "PhoneNumberUpdated"
}

enum Parameters: String {
    //tarif
    case tarifOpen = "page_tarif_open"
    case tarifOpenError = "tarif_open_error"
    case tarifCloseButton = "tarifCloseButton"
    case tarifSend = "send_tarif"
    case tarifSendError = "send_tarif_error"
    case tarifSendError_cancel = "send_tarif_error_cancel"
    case tarifSendSuccess = "send_tarif_success"
    
    case OnboardingPhoneSubmitted = "OnboardingPhoneSubmitted"
    case OnboardingAccountCreated = "OnboardingAccountCreated"
    case OnboardingCircleCreated = "OnboardingCircleCreated"
    case PostCreated = "PostCreated"
    case PostDeleted = "PostDeleted"
    case ListCreated = "ListCreated"
    case ListDeleted = "ListDeleted"
    case TaskCreated = "TaskCreated"
    case TaskDeleted = "TaskDeleted"
    case TaskCompleted = "TaskCompleted"
    case TaskMenuDateUpdated = "TaskMenuDateUpdated"
    case TaskMemberAdded = "TaskMemberAdded"
    case TaskListAssigned = "TaskListAssigned"
    case TaskDueDateAdded = "TaskDueDateAdded"
    case TaskPositionChanged = "TaskPositionChanged"
    case EventCreated = "EventCreated"
    case EventDeleted = "EventDeleted"
    case EventCompleted = "EventCompleted"
    case EventMenuDateUpdated = "EventMenuDateUpdated"
    case EventMemberAdded = "EventMemberAdded"
    case EventListAssigned = "EventListAssigned"
    case EventDueDateAdded = "EventDueDateAdded"
    case EventPositionChanged = "EventPositionChanged"
    case ApplicationShared = "ApplicationShared"
    case CircleNameChanged = "CircleNameChanged"
    case MemberInvited = "MemberInvited"
    case CircleCreated = "CircleCreated"
    case CircleDeleted = "CircleDeleted"
    case PhoneNumberUpdated = "PhoneNumberUpdated"
    case photoAdded = "photoAdded"
    case source = "source"
    case content = "content"
    case gesture = "gesture"
    case type = "type"
    
    
    
    case yes = "true"
    case no = "false"
    case button = "button"
    case header = "header"
    case textAndPhoto = "text and photo"
    case text = "text"
    case photo = "photo"
    case journal = "journal"
    case originalList = "original list"
    case todayList = "today list"
    case next7DaysList = "next 7 days list"
    case swipe = "swipe"
    case taskMenu = "task menu"
    case today = "today"
    case tomorrow = "tomorrow"
    case someday = "someday"
    case customDate = "custom date"
    case removed = "removed"
    case settings = "settings"
    case onboarding = "onboarding"
}

struct Parameter {
    
    enum ParameterName: String {
        case photoAdded = "photoAdded"
        case source = "source"
        case content = "content"
        case gesture = "gesture"
        case type = "type"
    }
    
    enum ParameterValue: String {
        case yes = "true"
        case no = "false"
        case button = "button"
        case header = "header"
        case textAndPhoto = "text and photo"
        case text = "text"
        case photo = "photo"
        case journal = "journal"
        case originalList = "original list"
        case todayList = "today list"
        case next7DaysList = "next 7 days list"
        case swipe = "swipe"
        case taskMenu = "task menu"
        case today = "today"
        case tomorrow = "tomorrow"
        case someday = "someday"
        case customDate = "custom date"
        case removed = "removed"
        case settings = "settings"
        case onboarding = "onboarding"
    }
    
    var name: ParameterName
    var value: ParameterValue
    
    init(_ name: ParameterName, _ value: ParameterValue) {
        self.name = name
        self.value = value
    }
}

class AnalyticsManager {
    class func logEvent(type: Parameters, parameters: [String: Any]? = nil) {
        let params: [String: Any] = parameters ?? [:]
        Analytics.logEvent(type.rawValue, parameters: params)
        Amplitude.instance().logEvent(type.rawValue, withEventProperties: params)
    }
//    class func logEvent(type: Parameters, parametrs: [Parameter]? = nil) {
//        let properties: [String: Any] = parametrs ?? [:]
//        Analytics.logEvent(type.rawValue, parameters: properties)
//        Amplitude.instance().logEvent(type.rawValue, withEventProperties: properties)
//
//    }
}

//pm - Notification Names :

struct NN {
    static let Update       = Notification.Name("LocalStorageUpdatedNotification")
    static let DrawerMove   = Notification.Name("DrawerControllerDidMove")
    static let DrawerOpen   = Notification.Name("DrawerControllerOpen")
    static let DrawerClose  = Notification.Name("DrawerControllerClose")
}

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct Device {
    static let iPhone4          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let iPhone5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let iPhone6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let iPhone6Plus      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let iPad             = UIDevice.current.userInterfaceIdiom == .pad   && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

//pm - Device Dependent Value :

@inline(__always) func DDV<T>(_ four: T, _ five: T, _ six: T, _ sixPlus: T) -> T {
    if Device.iPhone4 {
        return four
    } else if Device.iPhone5 {
        return five
    } else if Device.iPhone6 {
        return six
    } else {
        return sixPlus
    }
}
